/*!\file All_config.h
*  \date Dec 3, 2015
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef ALL_CONFIG_H_
#define ALL_CONFIG_H_


#ifdef __cplusplus
	extern "C" {
#endif

#define FUNC_DMA_NM			YES

#ifdef __cplusplus
	}
#endif
#endif /* ALL_CONFIG_H_ */
