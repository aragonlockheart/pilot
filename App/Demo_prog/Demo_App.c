#include "Demo_App.h"
#include "Global_Var.h"
#include "Gpt.h"

UART_DataUnit UARTDataTx;
UART_DataUnit UARTDataRx;
UART_DataUnit UARTIdleCheckRx;

uint8 UART_Ready[] = "Ready ";
uint8 UART_Receive[] = " Received ";
uint8 UART_IdleCheck[]=" ";

uint8 UART_RxMessage[2];
uint16 Debug_UART_RxNo;
uint16 Debug_UART_RxResult;
UART_StateType UART_State = UartApp_Ready_State;


void DEMO_APPUART(void)
{

	UARTDataTx.Msg_Name = UART_Msg_2;

	UARTDataRx.Msg_Attribute.Data_Size = sizeof(UART_RxMessage);
	UARTDataRx.Msg_Data.Data_Address = (void *)&UART_RxMessage;
	UARTDataRx.Msg_Name = UART_Msg_1;
	UARTDataRx.Msg_Timeout = 800u;


	switch(UART_State)
	{
	case UartApp_Ready_State:
		UARTDataTx.Msg_Attribute.Data_Size = sizeof(UART_Ready);
		UARTDataTx.Msg_Data.Data_Address = (void *)&UART_Ready;

		if(UART_SER_TriggerReceiveString(&UARTDataRx) EQUAL_CON UART_SER_Successful)
		{ /* If triggering for Rx is OK, change to Waiting state */
			UART_State = UartApp_Wait_State;

			UART_SER_SendString(&UARTDataTx);
		}
		else {/* Do nothing sinnce triggering is not successful */}

		break;
	case UartApp_Wait_State:
		Debug_UART_RxResult = UART_SER_GetReceiveString(&UARTDataRx,&Debug_UART_RxNo);

		if(Debug_UART_RxResult EQUAL_CON UART_SER_Successful)
		{
			if(Debug_UART_RxNo > ZERO_16)
			{
				UART_State =  UartApp_Response_State;
			}
			else
			{
				UART_State = UartApp_Ready_State;
			}
		}
		else{;}
		break;
	case UartApp_Response_State:
		UARTDataTx.Msg_Attribute.Data_Size = sizeof(UART_Receive);
		UARTDataTx.Msg_Data.Data_Address = (void *)&UART_Receive;

		if(UART_SER_SendString(&UARTDataTx) EQUAL_CON UART_SER_Successful)
		{
			UART_State = UartApp_Reflect_State;
		}
		break;
	case UartApp_Reflect_State:
		UARTDataTx.Msg_Attribute.Data_Size = Debug_UART_RxNo;
		UARTDataTx.Msg_Data.Data_Address = (void *)&UART_RxMessage;

		if(UART_SER_SendString(&UARTDataTx) EQUAL_CON UART_SER_Successful)
		{
			UART_State = UartApp_CheckIdleStart_State;
		}
		break;
	case UartApp_CheckIdleStart_State:
		UARTIdleCheckRx.Msg_Attribute.Data_Size = sizeof(UART_IdleCheck);
		UARTIdleCheckRx.Msg_Data.Data_Address = (void *)&UART_IdleCheck;
		UARTIdleCheckRx.Msg_Name = UART_Msg_1;
		UARTIdleCheckRx.Msg_Timeout = 20u;

		if(UART_SER_TriggerReceiveString(&UARTIdleCheckRx) EQUAL_CON UART_SER_Successful)
		{ /* If triggering for Rx is OK, change to Waiting state */
			UART_State = UartApp_CheckIdleWait_State;
		}

		break;
	case UartApp_CheckIdleWait_State:
		Debug_UART_RxResult = UART_SER_GetReceiveString(&UARTIdleCheckRx,&Debug_UART_RxNo);

		if(Debug_UART_RxResult EQUAL_CON UART_SER_Successful)
		{
			UART_State = UartApp_CheckIdleStart_State;
		}
		else
		{
			UART_State = UartApp_Ready_State;
		}
		break;
	default:
		UART_State = UartApp_CheckIdleStart_State;
		break;
	}
}

//I2c test section ----------------------------------------------------------------
volatile uint8_t	receive_data[7];
volatile uint8_t transmit_data[7];
uint8_t sec, min, hour, day, date, mon, year;
volatile uint8_t initialized = 0;
volatile uint32_t	systick_counter;

volatile uint8_t	delayflag = 0;
volatile uint32_t delaycounter = 0;


uint8_t	BCD2DEC(uint8_t data)
{
	return ((data >> 4)*10 +(data & 0x0F));
}

uint8_t DEC2BCD(uint8_t data)
{
	return (((data / 10) << 4) | (data % 10));
}

void DS3231_UpdateData(void)
{
	if (I2C_User_IsReceiveCompleted_Chnl1() == I2C_OK)
	{
		sec = BCD2DEC(receive_data[0]);
		min = BCD2DEC(receive_data[1]);
		hour = BCD2DEC(receive_data[2]);
		day = BCD2DEC(receive_data[3]);
		date = BCD2DEC(receive_data[4]);
		mon = BCD2DEC(receive_data[5]);
		year = BCD2DEC(receive_data[6]);

		LED_GREEN_BLINK();
	}

	if (I2C_User_IsReceiveCompleted_Chnl2() == I2C_OK)
	{
		sec = BCD2DEC(receive_data[0]);
		min = BCD2DEC(receive_data[1]);
		hour = BCD2DEC(receive_data[2]);
		day = BCD2DEC(receive_data[3]);
		date = BCD2DEC(receive_data[4]);
		mon = BCD2DEC(receive_data[5]);
		year = BCD2DEC(receive_data[6]);

		LED_ORANGE_BLINK();
	}

	if (I2C_User_IsReceiveCompleted_Chnl3() == I2C_OK)
	{
		sec = BCD2DEC(receive_data[0]);
		min = BCD2DEC(receive_data[1]);
		hour = BCD2DEC(receive_data[2]);
		day = BCD2DEC(receive_data[3]);
		date = BCD2DEC(receive_data[4]);
		mon = BCD2DEC(receive_data[5]);
		year = BCD2DEC(receive_data[6]);

		LED_BLUE_BLINK();
	}
}


void delayms(uint32_t ms)
{
	delaycounter = 0;
	delayflag = 1;
	while(delaycounter < ms);
	delayflag = 0;
	delaycounter = 0;
}

void DS3231_TimeSetUp(void)
{
	transmit_data[0] = DEC2BCD(Isec);
	transmit_data[1] = DEC2BCD(Imin);
	transmit_data[2] = DEC2BCD(Ihour);
	transmit_data[3] = DEC2BCD(Iday);
	transmit_data[4] = DEC2BCD(Idate);
	transmit_data[5] = DEC2BCD(Imon);
	transmit_data[6] = DEC2BCD(Iyear);


//	I2C_UserTransmit(&I2C_Handler_3, (DS33231_ADD << 1), 0x0000, transmit_data, 7);
//	I2C_UserTransmit_MemRequest_Chnl1((DS33231_ADD << 1), 0x0000, transmit_data, 7);
//	I2C_UserTransmit_MemRequest_Chnl2((DS33231_ADD << 1), 0x0000, transmit_data, 7);
//	I2C_UserTransmit_MemRequest_Chnl3((DS33231_ADD << 1), 0x0000, transmit_data, 7);
	initialized = 1;

}
//---------------------------------------------------------------------------------------------
