#ifndef DEMO_APP_H
#define DEMO_APP_H

#ifdef __cplusplus
	extern "C" {
#endif

#include "stm32f4xx.h"
#include "common_para.h"
#include "Std_Types.h"
#include "SER_UART.h"
#include "OS_Service.h"
#include "I2C_core.h"

typedef enum
{
	UartApp_Ready_State, 		 /* 0 */
	UartApp_Wait_State, 		 /* 1 */
	UartApp_Response_State,		 /* 2 */
	UartApp_Reflect_State,		 /* 3 */
	UartApp_CheckIdleStart_State,/* 4 */
	UartApp_CheckIdleWait_State  /* 5 */
}UART_StateType;


void DEMO_APPUART(void);
#define DS33231_ADD			0x68

#define Isec						00
#define Imin						38
#define Ihour						23
#define Iday						6
#define Idate						23
#define Imon						12
#define Iyear						16


uint8_t	BCD2DEC(uint8_t data);
uint8_t DEC2BCD(uint8_t data);
void DS3231_UpdateData(void);

extern volatile uint32_t	systick_counter;

extern volatile uint8_t	receive_data[7];
extern volatile uint8_t transmit_data[7];
extern uint8_t sec, min, hour, day, date, mon, year;
#ifdef __cplusplus
	}
#endif

#endif
