#ifdef __cplusplus   // avoid C++ mangling
    extern "C" {
#endif

// begin of the functions declaration
#include "stm32f4xx.h"
#include "InitMain.h"
#include "Task_Center.h"
#include "DMA_Services.h" /* Test purpose */
#include "Platform_Types.h"
#include "Std_Types.h"

#include "Port.h"
#include "Port_Cfg.h"

#include "I2C_core.h"
#include "I2C_Cfg.h"

int main(void);



#ifdef __cplusplus
    }
#endif
