#ifndef INITMAIN_H
#define INITMAIN_H

#ifdef __cplusplus
	extern "C" {
#endif
#include "stm32f4xx.h"
#include "stm32f4xx_Types.h"
#include "GPIO_drv.h"
#include "NVIC_drv.h"
#include "Timer_drv.h"
#include "RCC_drv.h"
#include "SER_UART.h"
#include "port.h"


void StartUpSequence(void);
#ifdef __cplusplus
	}
#endif

#endif
