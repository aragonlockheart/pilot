#include "InitMain.h"


inline static void StartUp_Native(void)
{
	RCC_drv_Init();
	GPIO_drv_Init();
	Port_Init(User_PortConfig);
	NVIC_drv_Init();
	Timer_Init();
	Gpt_Init();
	DMA_SER_Init(VOID);
	UART_SER_Init();
	return;
}


inline static void StartUp_Devices(void)
{

	return;
}


inline void StartUpSequence(void)
{
	/* Init the clock */
	SystemInit(VOID);
	StartUp_Native();
	StartUp_Devices();
	return;
}


