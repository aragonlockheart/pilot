/*
 * Dio.h
 *
 *  Created on: Aug 9, 2016
 *      Author: Mr.A
 */

#ifndef DIO_H_
#define DIO_H_

#include	"stm32f4xx.h"
#include  	"Std_types.h"
#include	"Port.h"
#include	"Port_Cfg.h"





#define Dio_ReadPin					Port_ReadPin
#define Dio_ReadPort				Port_ReadPort

#define Dio_WritePin				Port_WritePin
#define Dio_WritePort				Port_WritePort


#endif /* DIO_H_ */
