/*!
 *\file HAL_PWM.c
 *\date Jun 13, 2015
 *\author Nguyen Trong Viet
 *\brief this file contain parameters for HAL PWM
 */
#include "HAL_PWM.h"

/*! @brief Function to set the Duty Cycle of PWM
 *	@param ENR_PWMChannel: name of the PWM channel
 *	@param Duty_uint32: the desired duty cycle of PWM
 *	@return The result of setting duty cycle
 */
inline BOOL HAL_PWM_SetDuty(ENU_PWMChannels ENR_PWMChannel, uint32 Duty_uint32)
{
	return EM_ENABLE;
}




/*! @brief Function to set the frequency of PWM
 *	@param ENR_PWMChannel: name of the PWM channel
 *	@param Duty_uint32: the desired frequency of PWM
 *	@return The result of setting frequency
 */
inline BOOL HAL_PWM_SetFreq(ENU_PWMChannels ENR_PWMChannel, uint32 Duty_uint32)
{
	return EM_ENABLE;
}



/*! @brief Function to start PWM
 *	@param ENR_PWMChannel: name of the PWM channel
 *	@return The result of starting UART
 */
inline BOOL HAL_PWM_Start(ENU_PWMChannels ENR_PWMChannel)
{
	return EM_ENABLE;
}



/*! @brief Function to stop PWM
 *	@param ENR_PWMChannel: name of the PWM channel
 *	@return The result of stopping UART
 */
inline BOOL HAL_PWM_Stop(ENU_PWMChannels ENR_PWMChannel)
{
	return EM_ENABLE;
}
