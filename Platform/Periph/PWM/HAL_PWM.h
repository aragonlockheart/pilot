/*!
 *\file HAL_PWM.h
 *\date 13 June 2015
 *\author Nguyen Trong Viet
 *\brief this file contains the HAL for PWM
 */



#ifndef HAL_PWM_H
#define HAL_PWM_H

#include "stm32f4xx.h"
#include "common_para.h"
#include "stm32f4xx_Types.h"
#include "HAL_PWM_para.h"


#ifdef __cplusplus
	extern "C" {
#endif


inline BOOL HAL_PWM_SetFreq(ENU_PWMChannels ENR_PWMChannel, uint32 Duty_uint32);
inline BOOL HAL_PWM_SetDuty(ENU_PWMChannels ENR_PWMChannel,uint32 Duty_uint32);
inline BOOL HAL_PWM_Start(ENU_PWMChannels ENR_PWMChannel);
inline BOOL HAL_PWM_Stop(ENU_PWMChannels ENR_PWMChannel);

#ifdef __cplusplus
	}
#endif


#endif

