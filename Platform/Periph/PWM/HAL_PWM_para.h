/*!
 *\file HAL_PWM_para.h
 *\date Jun 13, 2015
 *\author Nguyen Trong Viet
 *\brief this file contain parameters for HAL PWM
 */

#ifndef HAL_PWM_PARA_H
#define HAL_PWM_PARA_H

#ifdef __cplusplus
	extern "C" {
#endif
#include "HAL_PWM.h"

/*!ENUM ENU_PWChannels contains number of PWM channels offered by MCU */
typedef enum ENU_PWMChannels
{
	EM_PWMChannel1,         //!< 1st PWM channel: TIM2_CH2
	EM_PWMMaxChannel		//!< Maximum number of PWM channels
}ENU_PWMChannels;


/*! \var HAL_PWMChann_ENUMS[EM_PWMMaxChannel]
 * Mapping array for PWM channels*/
TIM_TypeDef * const HAL_PWMChann_ENUMS[EM_PWMMaxChannel] = {TIM2};








#ifdef __cplusplus
	}
#endif

#endif /* HAL_PWM_PARA_H_ */
