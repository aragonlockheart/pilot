/*!\file DVR_UART.h
*  \date Jul 15, 2015
*  \author Nguyen Trong Viet
*  \brief Enter description here
*/

#ifndef DVR_UART_H_
#define DVR_UART_H_

#ifdef __cplusplus
		extern "C" {
#endif

#include "stm32f4xx.h"
#include "stm32f4xx_Types.h"
#include "HAL_UART.h"
#include "UART_CFG.h"
#include "stm32f4xx_RegTypes.h"

/*!DRV_UART_RetType: return type for UART Driver function*/
typedef enum
{
	EM_UART_DRV_Ret_Fail,	     		/*0*/
	EM_UART_DRV_Ret_Successful,		/*1*/
	EM_UART_DRV_Ret_Busy	    		/*2*/
}DRV_UART_RetType;

extern uint32 * Arr_Function_Pointer[UART_CntrMax];

DRV_UART_RetType DRV_UART_Init(void);

DRV_UART_RetType DRV_UART_Start(UART_DataUnit * Str_DRV_UART_SDU);

DRV_UART_RetType DRV_UART_Stop(UART_DataUnit * Str_DRV_UART_SDU);

DRV_UART_RetType DRV_UART_GetStat(UART_DataUnit * Str_DRV_UART_SDU);

DRV_UART_RetType DRV_UART_GetData(UART_DataUnit * Str_DRV_UART_SDU);

DRV_UART_RetType DRV_UART_SendSingleData(UART_DataUnit * Str_DRV_UART_SDU);

DRV_UART_RetType DRV_UART_GetSingleData(UART_DataUnit * Str_DRV_UART_SDU);

DRV_UART_RetType DRV_UART_SetBaud(UART_DataUnit * Str_DRV_UART_SDU);

DRV_UART_RetType DRV_UART_GetBaud(UART_DataUnit * Str_DRV_UART_DSU);

DRV_UART_RetType DRV_UART_GetHWAddress(UART_DataUnit * Str_DRV_UART_SDU);
#ifdef __cplusplus
		}
#endif

#endif
