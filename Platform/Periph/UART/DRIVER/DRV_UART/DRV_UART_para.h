/*!\file DRV_UART_para.h
*  \date Jul 15, 2015
*  \author Nguyen Trong Viet
*  \brief Enter description here
*/

#ifndef DRV_UART_PARA_H_
#define DRV_UART_PARA_H_

#ifdef __cplusplus
		extern "C" {
#endif

#include "DRV_UART.h"

typedef BooleanLstType (*UART_Cntr_Func)(UART_DataUnit * SDU);

extern uint32 HAL_UART_Ctr1FuncList[UART_Func_Max];


#ifdef __cplusplus
		}
#endif

#endif
