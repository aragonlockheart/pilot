/*!\file DR_UART.c
*  \date Jul 16, 2015
*  \author Nguyen Trong Viet
*  \brief Enter description here
*/
#include "DRV_UART.h"
#include "DRV_UART_para.h"

/*!< Arr_Function_Pointer: pointer to function list of controllers*/
uint32 * Arr_Function_Pointer[UART_CntrMax] = {HAL_UART_Ctr1FuncList};



STATIC BOOL DRV_UART_TxInterface_local(UART_DataUnit * Str_DRV_UART_SDU)
{
	/*! local_retval: return value of this function */
	BOOL local_retval;

	/*! local function pointer to get the desired function */
	uint32 * local_FunctionName = ZERO_32;

	UART_Cntr_Func Func_name = ZERO_32;

	/*! Controller index */
	UART_CtrlLst ControllerIndex;

	local_retval = EM_DISABLE;

	/* Get the defined controller index of the specific input message */
	ControllerIndex = CST_ARR_UARTBSWTable[UART_LAYER_Cntr][Str_DRV_UART_SDU->Msg_Name];

	/* Get the physical port index of the specific input message */
	Str_DRV_UART_SDU->Msg_Attribute.PhyPort = CST_ARR_UARTBSWTable[UART_LAYER_PhyPo][Str_DRV_UART_SDU->Msg_Name];

	/* Point the local function pointer to the wanted function of obtained controller */
	local_FunctionName = (Arr_Function_Pointer[ControllerIndex]) + Str_DRV_UART_SDU->Msg_Function;

	/* Get the function pointer to the obtained function address */
	Func_name = (UART_Cntr_Func)(*local_FunctionName);

	/* Call the function */
	local_retval = Func_name(Str_DRV_UART_SDU);


	return local_retval;
}








DRV_UART_RetType DRV_UART_Init(void)
{
	/*!UART_DataUnit: Dummy Data Unit */
	UART_DataUnit      DummyDataUnit_local;

	/*!DummyMsgName_local: Dummy message */
	UART_Msg_Name_Type DummyMsgName_local;

	/* Set function as initialization */
	DummyDataUnit_local.Msg_Function = UART_Func_Init;

	/* For all the messages, call initialization functions */
	for(DummyMsgName_local = UART_Msg_1; DummyMsgName_local<UART_Msg_Max; DummyMsgName_local++)
	{
		DummyDataUnit_local.Msg_Name = DummyMsgName_local;

		DRV_UART_TxInterface_local(&DummyDataUnit_local);
	}

	return EM_ENABLE;
}


DRV_UART_RetType DRV_UART_SendSingleData(UART_DataUnit * Str_DRV_UART_SDU)
{
	BOOL SendRet_local = EM_DISABLE;
	DRV_UART_RetType ActionRet_local = EM_UART_DRV_Ret_Fail;

	/* Set the desired function to SendData */
	Str_DRV_UART_SDU->Msg_Function = UART_Func_SendData;

	/* Call the TxInterface to handle the transmitting */
	SendRet_local = DRV_UART_TxInterface_local(Str_DRV_UART_SDU);

	if(SendRet_local EQUAL_CON EM_ENABLE)
	{
		ActionRet_local = EM_UART_DRV_Ret_Successful;
	}
	else
	{
		ActionRet_local = EM_UART_DRV_Ret_Fail;
	}
	return ActionRet_local;
}

DRV_UART_RetType DRV_UART_GetSingleData(UART_DataUnit * Str_DRV_UART_SDU)
{
	BOOL SendRet_local = EM_DISABLE;
	DRV_UART_RetType ActionRet_local = EM_UART_DRV_Ret_Fail;

	/* Set the desired function to SendData */
	Str_DRV_UART_SDU->Msg_Function = UART_Func_GetData;

	/* Call the TxInterface to handle the transmitting */
	SendRet_local = DRV_UART_TxInterface_local(Str_DRV_UART_SDU);

	if(SendRet_local EQUAL_CON EM_ENABLE)
	{
		ActionRet_local = EM_UART_DRV_Ret_Successful;
	}
	else
	{
		ActionRet_local = EM_UART_DRV_Ret_Fail;
	}
	return ActionRet_local;
}


DRV_UART_RetType DRV_UART_GetHWAddress(UART_DataUnit * Str_DRV_UART_SDU)
{
	BOOL SendRet_local = EM_DISABLE;
	DRV_UART_RetType ActionRet_local = EM_UART_DRV_Ret_Fail;

	/* Set the desired function to SendData */
	Str_DRV_UART_SDU->Msg_Function = UART_Func_GetAddress;

	/* Call the TxInterface to handle the transmitting */
	SendRet_local = DRV_UART_TxInterface_local(Str_DRV_UART_SDU);

	if(SendRet_local EQUAL_CON EM_ENABLE)
	{
		ActionRet_local = EM_UART_DRV_Ret_Successful;
	}
	else
	{
		ActionRet_local = EM_UART_DRV_Ret_Fail;
	}
	return ActionRet_local;
}

DRV_UART_RetType DRV_UART_SendMultiData(UART_DataUnit * Str_DRV_UART_SDU)
{


	return EM_UART_DRV_Ret_Successful;
}
