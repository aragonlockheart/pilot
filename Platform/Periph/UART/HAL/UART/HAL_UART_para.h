/*!\file HAL_UART_para.h
*  \date Jun 14, 2015
*  \author Nguyen Trong Viet
*  \brief This file contains the parameters for UARTs
*/

#ifndef HAL_UART_PARA_H_
#define HAL_UART_PARA_H_

#ifdef __cplusplus
		extern "C" {
#endif

#include "HAL_UART.h"




/*************************** Macro definition area *********************
 ***********************************************************************
	Below this comment is the space for defining all the UART
	macros. Please maintain this template
************************************************************************
************************************************************************/
#define UART_PhyPoNum_MAX	((uint8)	 2u)

#define MC_USARTRx			((uint16) 2u)			/*!< Enable Rx function */

#define MC_USARTTx			((uint16) 3u)			/*!< Enable TX function */

#define MC_USARTSBK			(uint16)	(0u)			/*!< Enable sending BREAK character  */

#define MC_USARTEn			(uint16) (13u)			/*!< Enable USART channel  */

#define MC_USART_TXE		(uint16)	(7u)			/*!< Transmit data register empty */

#define MC_USART_RXNE		(uint16) (5u)			/*!< Receive data register not empty */

#define CST_PhyPo0Default	(EM_UART_115200)

#define CST_PhyPo1Default	(EM_UART_115200)

/*************************** End of Macro definition area *************
 **********************************************************************/



typedef enum
{
	EM_UART_1200,			/*!< 0 */
	EM_UART_2400,			/*!< 1 */
	EM_UART_9600,			/*!< 2 */
	EM_UART_19200,			/*!< 3 */
	EM_UART_38400,			/*!< 4 */
	EM_UART_57600,			/*!< 5 */
	EM_UART_115200,			/*!< 6 */
	EM_UART_MaxNumofBaud	/*!< 7 */

}ENU_UARTBauds;

typedef enum
{
	EM_42MHz,			/*!< 0 */
	EM_84MHz,			/*!< 1 */
	EM_MaxNumOfSpeed	/*!< 2 */
}ENU_APBCLK;
typedef enum
{
	EM_APB1,
	EM_APB2,
	EM_MaxNumOfBank
}ENU_APBBank;

/*! EN_PhyPoLst: this is the list of controller's private physical ports */
typedef enum
{
	EM_HALPhyPo_0,	/*!< 0 Physical Port 0 */
	EM_HALPhyPo_1,	/*!< 1 Physical Port 1 */
	EM_HALPhyPo_Max	/*!< 2 Physical Port Max */
}EN_HALPhyPoLst;

/*! Strt_PhyPortPa: attributes of the controller's physical port */
typedef struct
{
	USART_TypeDef * PeriphAddress;		/* Pointer to Periph register address */
	ENU_APBCLK		PeriphClock;		/* Periph clock value */
	ENU_APBBank		PeriphBank;			/* MCU Bank of the periph */
}Strt_HALPhyPortPa;


Strt_HALPhyPortPa UART_HALCntrOne[EM_HALPhyPo_Max]	=
{
						/* Reg */	/*PerClk*/	/*Bank*/
		/*PhyPo0*/		{ USART6,	EM_84MHz,	EM_APB2},
		/*PhyPo1*/		{ USART6,	EM_84MHz,	EM_APB2}
};


/*! \var const BaudRegValue_uint16[EM_MaxNumOfSpeed][EM_UART_MaxNumofBaud]
 * Mapping table for baud rate value*/
uint16 const BaudRegValue_uint16[EM_MaxNumOfSpeed][EM_UART_MaxNumofBaud] =
{
        		/*!<1.2 KBps  ||   2.4KBps  ||  9.6KBps ||  19.2KBps ||  38.4KBps ||  57.6KBps ||  115.2KBps	*/
	/*42MHz*/	    0x88B8u	   ,   0x445Cu   ,  0x1117u	 ,  0x088Cu   ,  0x0446u   ,  0x02D9u   ,  0x016Du       ,
	/*84MHz*/		0x1170u	   ,   0x88B8u   ,  0x222Eu  ,  0x1117u   ,  0x088Cu   ,  0x05B2u   ,  0x02D9u
};

/*! ARR_BSWtoCntr1Baud: Map between the supported baud rate of BSW and Physical Port */
ENU_UARTBauds ARR_BSWtoCntr1Baud[UART_BSW_BaudLst_Max] =
{
		[UART_BSW_BaudLst_9600]   = EM_UART_9600,
		[UART_BSW_BaudLst_115200] = EM_UART_115200

};

ENU_UARTBauds ARR_DefaultBaud[EM_HALPhyPo_Max] =
{
		[EM_HALPhyPo_0] = EM_UART_115200,
		[EM_HALPhyPo_1]	= EM_UART_115200
};





#ifdef __cplusplus
		}
#endif

#endif
