/*!\file HAL_UART.h
*  \date Jun 14, 2015
*  \author Nguyen Trong Viet
*  \brief This file contains prototypes for UARTs
*/

#ifndef HAL_UART_H_
#define HAL_UART_H_

#include "stm32f4xx.h"
#include "stm32f4xx_Types.h"
#include "stm32f4xx_rcc.h"
#include "UART_CFG.h"
#include "stm32f4xx_RegTypes.h"
#include "MCU_Reg_USART.h"



#ifdef __cplusplus
		extern "C" {
#endif



/*! function void HAL_UART_Init(void) */
BooleanLstType HAL_UART_Init(UART_DataUnit * SDU);

/*! function HAL_UART_START(void) */
BooleanLstType HAL_UART_START(UART_DataUnit * SDU);

/*! function void HAL_UART_STOP(void) */
BooleanLstType HAL_UART_STOP(UART_DataUnit * SDU);

/*! function void HAL_UART_SETBAUD(void) */
BooleanLstType HAL_UART_GETBAUD(UART_DataUnit * SDU);

/*! function void HAL_UART_SETBAUD(void) */
BooleanLstType HAL_UART_SETBAUD(UART_DataUnit * SDU);

/*! function void HAL_UART_GETDATA(void) */
BooleanLstType HAL_UART_GETDATA(UART_DataUnit * SDU);

/*! function void HAL_UART_SENDDATA(void) */
BooleanLstType HAL_UART_SENDDATA(UART_DataUnit * SDU);

BooleanLstType HAL_UART_GetAddress(UART_DataUnit * SDU);

extern uint32 HAL_UART_Ctr1FuncList[UART_Func_Max];


#ifdef __cplusplus
		}
#endif

#endif
