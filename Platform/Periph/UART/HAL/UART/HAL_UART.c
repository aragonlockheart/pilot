/*!\file HAL_UART.c
*  \date Jun 14, 2015
*  \author Nguyen Trong Viet
*  \brief This file contains UART function declaration
*/

#include "HAL_UART.h"
#include "HAL_UART_para.h"


RegType HAL_UART_Ctr1FuncList[UART_Func_Max] =
{
	/*0*/	[UART_Func_Init]	= (RegType) &HAL_UART_Init,
	/*1*/	[UART_Func_Start]	= (RegType) &HAL_UART_START,
	/*2*/	[UART_Func_Stop]	= (RegType) &HAL_UART_STOP,
	/*3*/	[UART_Func_GetBaud]	= (RegType) &HAL_UART_GETBAUD,
	/*4*/	[UART_Func_SetBaud]	= (RegType) &HAL_UART_SETBAUD,
	/*5*/	[UART_Func_GetData]	= (RegType) &HAL_UART_GETDATA,
	/*6*/	[UART_Func_SendData]= (RegType) &HAL_UART_SENDDATA,
	/*7*/	[UART_Func_GetAddress] = (RegType) &HAL_UART_GetAddress
};


STATIC void Init_HalUART_local(void)
{
	EN_HALPhyPoLst Temp_PhyPoIndex;

	Strt_HALPhyPortPa Temp_HalPhyPort_local;
	for(Temp_PhyPoIndex = EM_HALPhyPo_0;Temp_PhyPoIndex < EM_HALPhyPo_Max;Temp_PhyPoIndex++)
	{
		Temp_HalPhyPort_local= UART_HALCntrOne[Temp_PhyPoIndex];

		Temp_HalPhyPort_local.PeriphAddress->BRR = BaudRegValue_uint16[Temp_HalPhyPort_local.PeriphClock][ARR_DefaultBaud[EM_HALPhyPo_0]];
		Temp_HalPhyPort_local.PeriphAddress->CR2 = ZERO_16;
		Temp_HalPhyPort_local.PeriphAddress->CR3 = 0x00C0;
		Temp_HalPhyPort_local.PeriphAddress->CR1 = (uint16)(0x200C);
	}

	return Void_Return;
}


STATIC BOOL IsSendPossible_local(void)
{
	BOOL ret_Status = EM_DISABLE;
	return ret_Status;
}


/*! @brief Function to start the USART communication
 * 	@param None
 * 	@return BOOL type
 *
 */
BooleanLstType HAL_UART_START(UART_DataUnit * SDU)
{
	BooleanLstType ret_val = Boolean_NO;
	return ret_val;
}



/*! @brief Function to stop the USART communication
 *	@param ENU_UARTChannels ENR_Channel
 *	@return BOOL type
 */
BooleanLstType HAL_UART_STOP(UART_DataUnit * SDU)
{
	BooleanLstType ret_val = Boolean_NO;
	return ret_val;
}




/*! @brief Function get the baudrate of USART communication
 *	@param None
 *	@return BOOL type
 */
BooleanLstType HAL_UART_GETBAUD(UART_DataUnit * SDU)
{
	BooleanLstType ret_val = Boolean_NO;
	return ret_val;
}




/*! @brief Function to set the baudrate of the USART communication
 *	@param None
 *	@return BOOL type
 */
BooleanLstType HAL_UART_SETBAUD(UART_DataUnit * SDU)
{
	BooleanLstType ret_val = Boolean_NO;
	return ret_val;
}




/*! @brief Function to get data using the USART communication
 *	@param ENU_UARTChannels ENR_Channel : Desired channel
 *	@param uint16 * UART_DATA_Puint32 : address where received data will be put in
 *	@return BOOL type
 */
BooleanLstType HAL_UART_GETDATA(UART_DataUnit * SDU)
{
	Strt_HALPhyPortPa Temp_HalPhyPort_local;
	USART_AllRegType * USART_AllRegPtr;
	uint16 Dummy_Rx;
	USART_SR_Type Status_local;
	BooleanLstType ret_val = Boolean_NO;

	Temp_HalPhyPort_local= UART_HALCntrOne[SDU->Msg_Attribute.PhyPort];
	USART_AllRegPtr = (USART_AllRegType *) Temp_HalPhyPort_local.PeriphAddress;

	Status_local = USART_AllRegPtr->SR;
	Dummy_Rx = (uint16) USART_AllRegPtr->DR.REG_DR_WHOLE;

	if(Status_local.REG_SR_DETAIL.RXNE EQUAL_CON ((uint32) 1u))
	{
		(*((uint16 *)SDU->Msg_Data.Data_Address)) = Dummy_Rx;
		ret_val = Boolean_YES;
	}
	else{; /* Do nothing */}

	return ret_val;
}

/*! @brief Function to send the data using USART communication
 *	@param ENU_UARTChannels ENR_Channel : name of the desired channel
 *	@param uint16 UART_DATA_uint16 : Data need to be transfer
 *	@return BOOL type
 */
BooleanLstType HAL_UART_SENDDATA(UART_DataUnit * SDU)
{
	Strt_HALPhyPortPa Temp_HalPhyPort_local;
	uint16 STATUS;
	uint16 Status_local;
	BooleanLstType ret_val = Boolean_NO;

	Temp_HalPhyPort_local= UART_HALCntrOne[SDU->Msg_Attribute.PhyPort];
	Status_local = Temp_HalPhyPort_local.PeriphAddress->SR;

	if(BitIsSet(Status_local,MC_USART_TXE))
	{
		Temp_HalPhyPort_local.PeriphAddress->DR = (*((uint16 *)SDU->Msg_Data.Data_Address));
	}

	return ret_val;
}

/*! @brief Function to init UART channels
 * 	@param void
 * 	@return void
 * 	@warn This function only configure parameters for UART port
 * 	@warn Pins must be enabled before hand in GPIO
 */
BooleanLstType HAL_UART_Init(UART_DataUnit * SDU)
{
	Init_HalUART_local();
	BooleanLstType ret_val = Boolean_YES;
	return ret_val;
}

BooleanLstType HAL_UART_GetAddress(UART_DataUnit * SDU)
{
	BooleanLstType ret_val = Boolean_NO;
	Strt_HALPhyPortPa Temp_HalPhyPort_local = UART_HALCntrOne[SDU->Msg_Attribute.PhyPort];
	SDU->Msg_Data.HW_Address = (void *) &(Temp_HalPhyPort_local.PeriphAddress->DR);
	return ret_val;
}
