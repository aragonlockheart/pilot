/*!\file UART_CFG.h
*  \date Aug 9, 2015
*  \author Nguyen Trong Viet
*  \brief Enter description here
*/

#ifndef UART_CFG_H_
#define UART_CFG_H_

#include "stm32f4xx_Types.h"
#include "stm32f4xx.h"
#include "DMA_Services.h"

#ifdef __cplusplus
		extern "C" {
#endif


#define UART_CtrlFuncList(Controller_Name)			void ##(Controller_Name)##_FnctTable

#define UART_DrvtoCtrlFuncList(Controller_Name)				(Controller_Name)##_FnctTable

#define Void_Return											/* void macro */

#define UART_Msg1Interrupt							void USART6_IRQn(void)
#define UART_Msg2Interrupt							void USART6_IRQn(void)

/*! UART_Msg_Name_Type contains list of UART message */
typedef enum
{
	UART_Msg_1,		/*!< 0*/
	UART_Msg_2,		/*!< 1*/
	UART_Msg_Max	/*!< 2*/
}UART_Msg_Name_Type;

typedef enum
{
	UART_Dir_Rx,		/*!<0 */
	UART_Dir_Tx,		/*!<1 */
	UART_Dir_Max		/*!<2 */
}UART_Msg_Dir;

/*! UART_Bsw_Layer_Type contains list of UART Bsw layers */
typedef enum
{
	UART_LAYER_USER,	/*!< 0*/
	UART_LAYER_LoPo,	/*!< 1*/
	UART_LAYER_Cntr,	/*!< 2*/
	UART_LAYER_PhyPo,	/*!< 3*/
	UART_LAYER_Max		/*!< 4*/
}UART_Bsw_Layer_Type;

/*! UART_FunctListType: List of functions supported */
typedef enum
{
	UART_Func_Init,			/*!< 0*/
	UART_Func_Start,		/*!< 1*/
	UART_Func_Stop,			/*!< 2*/
	UART_Func_GetBaud,		/*!< 3*/
	UART_Func_SetBaud,		/*!< 4*/
	UART_Func_GetData,		/*!< 5*/
	UART_Func_SendData,		/*!< 6*/
	UART_Func_GetAddress,	/*!< 7*/
	UART_Func_Max			/*!< 8*/
}UART_FunctListType;

typedef uint16 UART_Msg_TimeOut_Type;

/*! UART_BSW_BaudLst: List of supported baud rate by UART BSW */
typedef enum
{
	UART_BSW_BaudLst_9600,		/*!< 0*/
	UART_BSW_BaudLst_115200,	/*!< 1*/
	UART_BSW_BaudLst_Max		/*!< 2*/
}UART_BSW_BaudLst;

/*! UART_CtrlLst: List of UART controllers */
typedef enum
{
	UART_Cntr0,		/*!< 0 */
	UART_CntrMax	/*!< 1 */
}UART_CtrlLst;

/*! UART_PhoPoLst: List of UART Physical Ports */
typedef enum
{
	UART_EM_PhyPo0,	/*!< 0 */
	UART_EM_PhyPo1,	/*!< 1 */
	UART_EM_PhyPoMax	/*!< 2 */
}UART_PhoPoLst;

typedef enum
{
	EM_UARTLoPo0,	/*!< 0 */
	EM_UARTLoPo1,	/*!< 1 */
	EM_UARTLoPoMax	/*!< 2 */
}UART_LoPoLst;

/*! UART_Msg_Data_Type contains the pointer to data of UART message */
typedef struct
{
	void * Data_Address;
	void * HW_Address;
}UART_Msg_Data_Type;

/*! UART_Msg_Attribute_Type: contains properties of UART message */
typedef struct
{
	uint16 			Data_Size;
	UART_Msg_Dir  	Direction;
	UART_PhoPoLst	PhyPort;
}UART_Msg_Attribute_Type;

/*! UART_Msg_Status_Type: status flags of message transferring */
typedef struct
{
	uint8 UART_Stat_TxComplete: 	1;
	uint8 UART_Stat_RxComplete: 	1;
	uint8 UART_Stat_Idle:			1;
}UART_Msg_Status_Type;

/*! UART_DataUnit: SDU of UART BSW */
typedef struct
{
	UART_Msg_Data_Type 			Msg_Data;
	UART_Msg_Attribute_Type 	Msg_Attribute;
	UART_Msg_Name_Type			Msg_Name;
	UART_Msg_Status_Type 		Msg_Status;
	UART_Msg_TimeOut_Type		Msg_Timeout;
	UART_FunctListType			Msg_Function;
}UART_DataUnit;


/* CST_ARR_UARTBSWTable declared in UART_CFG.c */
extern const uint8 CST_ARR_UARTBSWTable[UART_LAYER_Max][UART_Msg_Max];

extern const DMA_Path_Lst CST_ARR_UART2DMA[UART_Msg_Max];

#ifdef __cplusplus
		}
#endif

#endif
