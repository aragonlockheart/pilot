/*!\file UART_CFG.c
*  \date Aug 15, 2015
*  \author Nguyen Trong Viet
*  \brief Enter description here
*/

#include "UART_CFG.h"
#include "UART_CFG_Para.h"

/*! UART_MsgStatus: contains status bits of all Transmitt and receive message */
uint32 UART_MsgStatus = ZERO_32;

/*! CST_ARR_UARTBSWTable: contains parameters for UART Bsw */
const uint8 CST_ARR_UARTBSWTable[UART_LAYER_Max][UART_Msg_Max]=
{
					/*Msg1*/			/*Msg2*/
	/*USER*/		UART_USER_COM,		UART_USER_COM,
	/*Logical*/		EM_UARTLoPo0,		EM_UARTLoPo1,
	/*Controller*/	UART_Cntr0,			UART_Cntr0,
	/*Physical*/	UART_EM_PhyPo0,		UART_EM_PhyPo1
};


const DMA_Path_Lst CST_ARR_UART2DMA[UART_Msg_Max] =
{
	[UART_Msg_1] = DMA_Path_01,
	[UART_Msg_2] = DMA_Path_02
};
