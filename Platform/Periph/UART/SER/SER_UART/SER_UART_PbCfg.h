/*!\file UART_ROUT_para.h
*  \date Sep 22, 2015
*  \author Nguyen Trong Viet
*  \brief Enter description here
*/

#ifndef UART_ROUT_PARA_H_
#define UART_ROUT_PARA_H_

#ifdef __cplusplus
		extern "C" {
#endif

#include "DRV_UART.h"

RegType * UART_HW_Desc[UART_Msg_Max];

uint16 UART_TimeOutThres[UART_Msg_Max];

#ifdef __cplusplus
		}
#endif

#endif
