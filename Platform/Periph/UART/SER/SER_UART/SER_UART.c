/*!\file ROUT_UART.c
*  \date Sep 22, 2015
*  \author Nguyen Trong Viet
*  \brief This module is the router for UART COM Stack
*/
#include "SER_UART.h"
#include "SER_UART_LCfg.h"
#include "SER_UART_PbCfg.h"


UART_SER_Rettype UART_SER_Init(void)
{
	UART_DataUnit UART_DummyUnit_local;
	UART_Msg_Name_Type UART_Path_ID_local = UART_Msg_1;

	for(UART_Path_ID_local = UART_Msg_1; UART_Path_ID_local < DMA_Path_Max; UART_Path_ID_local++)
	{
		UART_DummyUnit_local.Msg_Name = UART_Path_ID_local;
		DRV_UART_GetHWAddress(&UART_DummyUnit_local);
		UART_HW_Desc[UART_Path_ID_local] = UART_DummyUnit_local.Msg_Data.HW_Address;
	}
	DRV_UART_Init();

	return UART_SER_Successful;


}

UART_SER_Rettype UART_SER_SendSingleData(UART_DataUnit * UART_Unit)
{
	DRV_UART_RetType temp_ret = EM_UART_DRV_Ret_Fail;
	UART_SER_Rettype ret_value = UART_SER_Fail;

	temp_ret = DRV_UART_SendSingleData(UART_Unit);

	switch (temp_ret)
	{
		case EM_UART_DRV_Ret_Successful:
			ret_value = UART_SER_Successful;
			break;
		case EM_UART_DRV_Ret_Fail:
			ret_value = UART_SER_Fail;
			break;
		default:
			ret_value = UART_SER_Busy;
			break;
	}

	return temp_ret;
}

UART_SER_Rettype UART_SER_GetSingleData(UART_DataUnit * UART_Unit)
{
	DRV_UART_RetType temp_ret = EM_UART_DRV_Ret_Fail;
	UART_SER_Rettype ret_value = UART_SER_Fail;

	temp_ret = DRV_UART_GetSingleData(UART_Unit);

	switch (temp_ret)
	{
		case EM_UART_DRV_Ret_Successful:
			ret_value = UART_SER_Successful;
			break;
		case EM_UART_DRV_Ret_Fail:
			ret_value = UART_SER_Fail;
			break;
		default:
			ret_value = UART_SER_Busy;
			break;
	}

	return ret_value;
}

UART_SER_Rettype UART_SER_SendString(UART_DataUnit * UART_Unit)
{

	DMA_UnitStruct DMA_Struct_local;
	UART_SER_Rettype ret_value;
	DMA_Struct_local.SE_DMA_NumOfTransfer = (UART_Unit->Msg_Attribute).Data_Size;
	DMA_Struct_local.PathId 			  = CST_ARR_UART2DMA[UART_Unit->Msg_Name];
	DMA_Struct_local.Dest	   		      = (RegType) UART_HW_Desc[UART_Unit->Msg_Name];
	DMA_Struct_local.Src	              = (RegType) UART_Unit->Msg_Data.Data_Address;
	DMA_Struct_local.SE_DMA_Status		  = ZERO_32;



	switch(DMA_SER_Start(&DMA_Struct_local))
	{
	case DMA_SER_Succcessful:
		ret_value = UART_SER_Successful;
		break;
	case DMA_SER_Busy:
		ret_value = UART_SER_Busy;
		break;
	case DMA_SER_Fail:
		ret_value = UART_SER_Fail;
		break;
	default:
		ret_value = UART_SER_Fail;
		break;
	}
	return ret_value;
}

UART_SER_Rettype UART_SER_TriggerReceiveString(UART_DataUnit * UART_Unit)
{

	DMA_UnitStruct DMA_Struct_local;
	UART_SER_Rettype ret_value;
	DMA_Struct_local.SE_DMA_NumOfTransfer = (UART_Unit->Msg_Attribute).Data_Size;
	DMA_Struct_local.PathId 			  = CST_ARR_UART2DMA[UART_Unit->Msg_Name];
	DMA_Struct_local.Dest	   		      = (RegType) UART_Unit->Msg_Data.Data_Address;
	DMA_Struct_local.Src	              = (RegType) UART_HW_Desc[UART_Unit->Msg_Name];
	DMA_Struct_local.SE_DMA_Status		  = ZERO_32;
	DMA_Struct_local.TimeOutThreshold	  = UART_Unit->Msg_Timeout;

	/* Read data from UART registers to clean the flags of redudant data */
	UART_SER_GetSingleData(UART_Unit);

	switch(DMA_SER_Start(&DMA_Struct_local))
	{
		case DMA_SER_Succcessful:
			ret_value = UART_SER_Successful;
			break;
		case DMA_SER_Busy:
			ret_value = UART_SER_Busy;
			break;
		case DMA_SER_Fail:
			ret_value = UART_SER_Fail;
			break;
		default:
			ret_value = UART_SER_Fail;
			break;

	}

	return ret_value;
}


UART_SER_Rettype UART_SER_GetReceiveString(UART_DataUnit * UART_Unit, uint16 * NoOfTransferred)
{
	/* Initialize return value */
	UART_SER_Rettype Ret_val = UART_SER_Fail;

	/* Get the DMA Index of the UART message */
	DMA_Path_Lst DMA_Index = CST_ARR_UART2DMA[UART_Unit->Msg_Name];

	/* Check if the message has been received */
	if(DMA_SER_WhetherComplete(DMA_Index) EQUAL_CON Boolean_YES)
	{
		(*NoOfTransferred) = DMA_DRV_GetTransferredAmount(DMA_Index);
		Ret_val = UART_SER_Successful;
	}
	else
	{;}

	return Ret_val;
}
