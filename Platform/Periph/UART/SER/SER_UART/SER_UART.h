/*!\file UART_ROUT.h
*  \date Sep 22, 2015
*  \author Nguyen Trong Viet
*  \brief Enter description here
*/

#ifndef UART_SER_H_
#define UART_SER_H_

#include "DRV_UART.h"

typedef enum
{
	UART_SER_Successful,	/*!0*/
	UART_SER_Fail,			/*!1*/
	UART_SER_Busy			/*!2*/
}UART_SER_Rettype;



#ifdef __cplusplus
		extern "C" {
#endif

UART_SER_Rettype UART_SER_Init(void);

UART_SER_Rettype UART_SER_SendSingleData(UART_DataUnit * UART_Unit);

UART_SER_Rettype UART_SER_SendString(UART_DataUnit * UART_Unit);

UART_SER_Rettype UART_SER_GetSingleData(UART_DataUnit * UART_Unit);

UART_SER_Rettype UART_SER_TriggerReceiveString(UART_DataUnit * UART_Unit);

UART_SER_Rettype UART_SER_GetReceiveString(UART_DataUnit * UART_Unit, uint16 * NoOfTransferred);

#ifdef __cplusplus
		}
#endif

#endif
