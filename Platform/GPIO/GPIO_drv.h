/**
  ******************************************************************************
  * @file    GPIO_drv.h
  * @author  Viet
  * @brief   This file is the driver for GPIO
  ******************************************************************************
  * @attention
  ******************************************************************************
  */
#ifndef GPIO_DRV_H
#define GPIO_DRV_H

#ifdef __cplusplus
	extern "C" {
#endif

#include "stm32f4xx.h"
#include "stm32f4xx_Types.h"
#include "common_para.h"
#include "GPIO_drv_para.h"

void GPIO_drv_Init(void);


#ifdef __cplusplus
	}
#endif
#endif
