#ifndef GPIO_MIDDLE_H
#define GPIO_MIDDLE_H

#ifdef __cplusplus
	extern "C" {
#endif

#include "stm32f4xx.h"
#include "stm32f4xx_Types.h"
#include "Common_funcs.h"
#include "common_para.h"

/*function to toggle bits */
inline void GPIO_Toggle(uint16 PINMASK, GPIO_TypeDef * PORT);

#ifdef __cplusplus
	}
#endif

#endif
