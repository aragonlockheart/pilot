#include "stm32f4xx.h"
#include "common_para.h"
#include "stm32f4xx_Types.h"

#define	GPIOA_MODER			(uint32) (0xA8000008)
#define	GPIOA_AFL			(uint32) (0x00000010)
#define GPIOA_AFH			(uint32) (0x00000000)
#define GPIOA_PP			(uint32) (ZERO_32)
#define GPIOA_PULL			(uint32) (0x64000000)
#define GPIOA_SPEED			(uint32) (0x0000000C)
#define GPIOA_OTYPER		(uint32) (ZERO_32)

#define	GPIOB_MODER			(uint32) (0x00000288)
#define	GPIOB_AFL			(uint32) (0x00000020)
#define GPIOB_AFH			(uint32) (0x00000000)
#define GPIOB_PP			(uint32) (ZERO_32)
#define GPIOB_PULL			(uint32) (0x00000108)
#define GPIOB_SPEED			(uint32) (0x000000CC)
#define GPIOB_OTYPER		(uint32) (ZERO_32)

#define	GPIOC_MODER			(uint32) (0x0000A000)
#define	GPIOC_AFL			(uint32) (0x88000000)
#define GPIOC_AFH			(uint32) (ZERO_32)
#define GPIOC_PP			(uint32) (ZERO_32)
#define GPIOC_PULL			(uint32) (0x00005000)
#define GPIOC_SPEED			(uint32) (0x0000F000)
#define GPIOC_OTYPER		(uint32) (ZERO_32)

#define	GPIOD_MODER			(uint32) (0xA1AAAA0A)
#define	GPIOD_AFL			(uint32) (0xCCCC00CC)
#define GPIOD_AFH			(uint32) (0xCC00CCCC)
#define GPIOD_PP			(uint32) (ZERO_32)
#define GPIOD_PULL			(uint32) (ZERO_32)
#define GPIOD_SPEED			(uint32) (0xF0FFFF0F)
#define GPIOD_OTYPER		(uint32) (ZERO_32)

#define	GPIOE_MODER			(uint32) (0xAAAA8000)
#define	GPIOE_AFL			(uint32) (0xC0000000)
#define GPIOE_AFH			(uint32) (0xCCCCCCCC)
#define GPIOE_PP			(uint32) (ZERO_32)
#define GPIOE_PULL			(uint32) (ZERO_32)
#define GPIOE_SPEED			(uint32) (0xFFFFC000)
#define GPIOE_OTYPER		(uint32) (ZERO_32)
