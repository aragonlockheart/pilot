#include "GPIO_Middle.h"


inline void GPIO_Toggle(uint16 PINMASK, GPIO_TypeDef * PORT)
{
	/* local variable for getting affected pins' values */
	uint32 affected_GPIO_out;

	/* local variable for non affected pins value */
	uint32 non_affected_GPIO_out;

	/* Get only the concerned pins' value */
	affected_GPIO_out = (PORT->ODR) & ((uint32) PINMASK);

	/* Get only the non-concerned pins' value */
	non_affected_GPIO_out = (PORT->ODR) & (~((uint32) PINMASK));

	/*Toggle the data of concerned pins */
	affected_GPIO_out ^= PINMASK;

	/*Output the processed pins' data */
	(PORT->ODR) = non_affected_GPIO_out | affected_GPIO_out;

}
