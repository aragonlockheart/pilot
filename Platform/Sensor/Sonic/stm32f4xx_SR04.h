
/*!
 * \brief This file contains the prototypes for SR04 middleware
 * \file stm32f4xx_SR04.h
 * \author Nguyen Trong Viet
 * \date 01-06-2015
 * \pre The PWM and input capture must be ready
 */
#ifndef STM32F4XX_SR04_H

#include "stm32f4xx.h"
#include "Common_funcs.h"
#include "common_para.h"

#ifdef __cplusplus
	extern "C" {
#endif

/*!
 * \brief This function is used to get raw time from SR04 sensor
 * @param None
 * \return unsigned integer 16 bit
 */
uint16 SR04_DataRaw(void);



/*!
 * \brief This function is used to get distance from SR04 sensor in centimeters
 * @param None
 * \return unsigned integer 16 bit
 */
uint16 SR04_DataInCm(void);


/*!
 * \brief This function is used to get distance from SR04 sensor in millimeters
 * @param None
 * \return unsigned integer 16 bit
 */
uint16 SR04_DataInMm(void);

#ifdef __cplusplus
	}
#endif

#endif
