/*! \file stm32f4xx_SR04.c
 * \brief
 * This file contains functions for getting distance from SR04 sensor
 * \author Nguyen Trong Viet
 * \date 04/06/2015
 * \addtogroup USER_DRIVER
 * \addtogroup SENSOR
 * \ingroup USER_DRIVER
 * \addtogroup ULTRASONIC
 * \addtogroup SR04
 */




#include "stm32f4xx_SR04.h"

/*! \define CM_STEP time for sound to travel 1cm in microseconds */
#define CM_STEP 		((uint16) (58u))

/*! \define DEC_10_uint16 decimal value of TEN */
#define DEC_10_uint16	((uint16) (10u))




uint16 SR04_DataRaw(void)
{
	/* Get value of TIM2 capture register */
	return ((uint16) (TIM2->CCR1));
}



uint16 SR04_DataInCm(void)
{
	uint16 Temp_Var;

	Temp_Var = ((uint16) (TIM2->CCR1)) DEVIDE_BY CM_STEP;

	return Temp_Var;
}



uint16 SR04_DataInMm(void)
{
	uint16 Temp_Var;

	Temp_Var = (((uint16) (TIM2->CCR1)) MULTIPLY DEC_10_uint16) DEVIDE_BY CM_STEP;

	return Temp_Var;
}
