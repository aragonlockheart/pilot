#ifndef RCC_DRV_H
#define RCC_DRV_H

#ifdef __cplusplus
	extern "C" {
#endif

#include "stm32f4xx.h"
#include "stm32f4xx_Types.h"
#include "common_para.h"
#include "MCU_Reg_RCC.h"
#include "Std_Types.h"
#include "Compiler.h"

void RCC_drv_Init(void);



#ifdef __cplusplus
	}
#endif
#endif
