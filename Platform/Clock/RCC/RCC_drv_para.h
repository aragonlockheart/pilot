#ifndef RCC_DRV_PARA_H
#define RCC_DRV_PARA_H

#ifdef __cplusplus
	extern "C" {
#endif
#include "stm32f4xx.h"
#include "stm32f4xx_Types.h"
#include "I2C_Parameter.h"

#define AHB1_VALUE (uint32) (0x0000009F)
#define AHB2_VALUE (uint32) (0x00000000)
#define AHB3_VALUE (uint32) (0x00000001)
#define APB1_VALUE ((uint32) (0x0020001F)| I2C_CLOCK_ENABLE_FINAL)
#define APB2_VALUE (uint32) (0x00000030)

typedef enum
{
	RCC_OFF = 0,
	RCC_ON	= 1
}RCC_BOOL;
#ifdef __cplusplus
	}
#endif

#endif
