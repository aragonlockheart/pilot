#include "RCC_drv.h"
#include "RCC_drv_para.h"



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                     Private variable			                                                          |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*                                                                                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/






/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                     Local functions declaration                                                          |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*                                                                                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

STRICT_INLINE static void AHB_ENR_local(void);
STRICT_INLINE static void APB_ENR_local(void);

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                     Global API definition                                                                |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*                                                                                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/



/*+------------------------------------------[   RCC_drv_Init    ]-------------------------------------------+
  |                                                                                                          |
  | brief 	    Initialize RCC registers 								                                     |
  |                                                                                                          |
  | parameter 	 None													                                     |
  | return       None										                                                 |
  |                                                                                                        	 |
  +----------------------------------------------------------------------------------------------------------+*/
void RCC_drv_Init(void)
{
	AHB_ENR_local();
	APB_ENR_local();
	return;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                     Local functions definition                                                           |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*                                                                                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/*+-----------------------------------------[   AHB_ENR_local    ]-------------------------------------------+
  |                                                                                                          |
  | brief 	    Initialize RCC AHB registers 							                                     |
  |                                                                                                          |
  | parameter 	 None													                                     |
  | return       None										                                                 |
  |                                                                                                        	 |
  +----------------------------------------------------------------------------------------------------------+*/
STRICT_INLINE static void AHB_ENR_local(void)
{
	/* Temporary variables represent AHB registers */
	volatile Reg_RCC_AHB1ENR_Type Temp_AHB1ENR;
	volatile Reg_RCC_AHB2ENR_Type Temp_AHB2ENR;
	volatile Reg_RCC_AHB3ENR_Type Temp_AHB3ENR;

	/* Initialize to 0 */
	Temp_AHB1ENR.Whole = ZERO_UNSIGNED_32;
	Temp_AHB2ENR.Whole = ZERO_UNSIGNED_32;
	Temp_AHB3ENR.Whole = ZERO_UNSIGNED_32;

	/* Initialize AHB1 temporary variable */
	Temp_AHB1ENR.BitField.GPIOAEN 	= RCC_ON;           /* GPIOA ON */
	Temp_AHB1ENR.BitField.GPIOBEN 	= RCC_ON;           /* GPIOB ON */
	Temp_AHB1ENR.BitField.GPIOCEN 	= RCC_ON;           /* GPIOC ON */
	Temp_AHB1ENR.BitField.GPIODEN 	= RCC_ON;           /* GPIOD ON */
	Temp_AHB1ENR.BitField.GPIOEEN 	= RCC_ON;           /* GPIOE ON */
	Temp_AHB1ENR.BitField.GPIOFEN 	= RCC_ON;           /* GPIOF ON */
	Temp_AHB1ENR.BitField.GPIOGEN 	= RCC_ON;           /* GPIOG ON */
	Temp_AHB1ENR.BitField.GPIOHEN 	= RCC_ON;           /* GPIOH ON */
	Temp_AHB1ENR.BitField.GPIOIEN 	= RCC_ON;           /* GPIOI ON */
	Temp_AHB1ENR.BitField.GPIOJEN 	= RCC_ON;           /* GPIOJ ON */
	Temp_AHB1ENR.BitField.GPIOKEN 	= RCC_ON;           /* GPIOK ON */
	Temp_AHB1ENR.BitField.CRCEN	  	= RCC_OFF;          /* CRC OFF  */
	Temp_AHB1ENR.BitField.BKPSRAMEN = RCC_OFF;          /* Backup RAM OFF */
	Temp_AHB1ENR.BitField.CCMDATARAMEN = RCC_OFF;       /* CCM Data RAM OFF */
	Temp_AHB1ENR.BitField.DMA1EN  	= RCC_ON;           /* DMA1 ON */
	Temp_AHB1ENR.BitField.DMA2EN 	= RCC_ON;           /* DMA2 ON */
	Temp_AHB1ENR.BitField.DMA2DEN	= RCC_ON;           /* DMA2D ON */
	Temp_AHB1ENR.BitField.ETHMACEN	= RCC_OFF;          /* Ethernet MAC OFF */
	Temp_AHB1ENR.BitField.ETHMACTXEN= RCC_OFF;          /* Ethernet Transmission OFF */
	Temp_AHB1ENR.BitField.ETHMACRXEN= RCC_OFF;          /* Ethernet Reception OFF */
	Temp_AHB1ENR.BitField.ETHMACPTPEN  = RCC_OFF;       /* Ethernet PTP OFF */
	Temp_AHB1ENR.BitField.OTGHSEN	= RCC_OFF;          /* USB OTG HS OFFF */
	Temp_AHB1ENR.BitField.OTGHSULPIEN  = RCC_OFF;       /* USB OTG HSULPI OFFF */

	/* Initialize AHB2 temporary variable */
	Temp_AHB2ENR.BitField.DCMIEN = RCC_OFF;			    /* DCMI OFF */
	Temp_AHB2ENR.BitField.CRYPEN = RCC_OFF;             /* CRYPTO OFF */
	Temp_AHB2ENR.BitField.HASHEN = RCC_OFF;             /* HASH OFF */
	Temp_AHB2ENR.BitField.RNGEN	 = RCC_OFF;             /* Random Number Generator OFF */
	Temp_AHB2ENR.BitField.OTGFSEN= RCC_OFF;             /* USB OTG FS OFF */

	/* Initialize AHB3 temporary variable */
	Temp_AHB3ENR.BitField.FMCEN  = RCC_ON;              /* FSMC ON */

	/* Copy values from temporary variables to HW regiters */
	Reg_RCC_AHB1ENR->Whole = Temp_AHB1ENR.Whole;
	Reg_RCC_AHB2ENR->Whole = Temp_AHB2ENR.Whole;
	Reg_RCC_AHB3ENR->Whole = Temp_AHB3ENR.Whole;
	return;
}


/*+-----------------------------------------[   APB_ENR_local    ]-------------------------------------------+
  |                                                                                                          |
  | brief 	    Initialize RCC APB registers 							                                     |
  |                                                                                                          |
  | parameter 	 None													                                     |
  | return       None										                                                 |
  |                                                                                                        	 |
  +----------------------------------------------------------------------------------------------------------+*/
STRICT_INLINE static void APB_ENR_local(void)
{
	/* Temporary variables represent AHB registers */
	volatile Reg_RCC_APB1ENR_Type Temp_APB1ENR;
	volatile Reg_RCC_APB2ENR_Type Temp_APB2ENR;


	/* Initialize to 0 */
	Temp_APB1ENR.Whole = ZERO_UNSIGNED_32;
	Temp_APB2ENR.Whole = ZERO_UNSIGNED_32;


	Temp_APB1ENR.BitField.CAN1EN = RCC_OFF; 	/* CAN 1 OFF */
	Temp_APB1ENR.BitField.CAN2EN = RCC_OFF; 	/* CAN 2 OFF */
	Temp_APB1ENR.BitField.DACEN	 = RCC_OFF; 	/* DAC OFF */
	Temp_APB1ENR.BitField.I2C1EN = RCC_ON;  	/* I2C 1 ON */
	Temp_APB1ENR.BitField.I2C2EN = RCC_ON;		/* I2C 2 ON */
	Temp_APB1ENR.BitField.I2C3EN = RCC_ON;  	/* I2C 3 ON */
	Temp_APB1ENR.BitField.PWREN  = RCC_OFF; 	/* Power Interface OFF */
	Temp_APB1ENR.BitField.SPI2EN = RCC_ON;  	/* SPI 2 ON */
	Temp_APB1ENR.BitField.SPI3EN = RCC_ON;  	/* SPI 3 ON */
	Temp_APB1ENR.BitField.TIM2EN = RCC_OFF; 	/* TIM 2 OFF */
	Temp_APB1ENR.BitField.TIM3EN = RCC_OFF; 	/* TIM 3 OFF */
	Temp_APB1ENR.BitField.TIM4EN = RCC_ON;	 	/* TIM 4 ON */
	Temp_APB1ENR.BitField.TIM5EN = RCC_ON; 		/* TIM 5 ON */
	Temp_APB1ENR.BitField.TIM6EN = RCC_OFF; 	/* TIM 6 OFF */
	Temp_APB1ENR.BitField.TIM7EN = RCC_OFF; 	/* TIM 7 OFF */
	Temp_APB1ENR.BitField.TIM12EN= RCC_OFF; 	/* TIM 12 OFF */
	Temp_APB1ENR.BitField.TIM13EN= RCC_OFF; 	/* TIM 13 OFF */
	Temp_APB1ENR.BitField.TIM14EN= RCC_OFF; 	/* TIM 14 OFF */
	Temp_APB1ENR.BitField.USART2EN=RCC_OFF; 	/* USART 2 OFF */
	Temp_APB1ENR.BitField.USART3EN=RCC_OFF; 	/* USART 3 OFF */
	Temp_APB1ENR.BitField.UART4EN =RCC_OFF; 	/* UART 4 OFF */
	Temp_APB1ENR.BitField.UART5EN =RCC_OFF; 	/* UART 5 OFF */
	Temp_APB1ENR.BitField.UART7EN =RCC_OFF; 	/* UART 7 OFF */
	Temp_APB1ENR.BitField.UART8EN =RCC_OFF; 	/* UART 8 OFF */
	Temp_APB1ENR.BitField.WWDGEN  =RCC_OFF; 	/* Watch dog timer OFF */

	Temp_APB2ENR.BitField.USART1EN = RCC_OFF;	/* USART 1 OFF */
	Temp_APB2ENR.BitField.USART6EN = RCC_ON;	/* USART 6 OFF */
	Temp_APB2ENR.BitField.TIM1EN   = RCC_OFF;   /* TIM 1 OFF */
	Temp_APB2ENR.BitField.TIM8EN   = RCC_OFF;   /* TIM 8 OFF */
	Temp_APB2ENR.BitField.TIM9EN   = RCC_OFF;	/* TIM 9 OFF */
	Temp_APB2ENR.BitField.TIM10EN  = RCC_OFF;   /* TIM 10 OFF */
	Temp_APB2ENR.BitField.TIM11EN  = RCC_OFF;   /* TIM 11 OFF */
	Temp_APB2ENR.BitField.SPI1EN   = RCC_OFF;   /* SPI 1 OFF */
	Temp_APB2ENR.BitField.SPI4EN   = RCC_OFF;	/* SPI 4 OFF */
	Temp_APB2ENR.BitField.SPI5EN   = RCC_OFF;   /* SPI 5 OFF */
	Temp_APB2ENR.BitField.SPI6EN   = RCC_OFF;   /* SPI 6 OFF */
	Temp_APB2ENR.BitField.ADC1EN   = RCC_OFF;	/* ADC 1 OFF */
	Temp_APB2ENR.BitField.ADC2EN   = RCC_OFF;	/* ADC 2 OFF */
	Temp_APB2ENR.BitField.ADC3EN   = RCC_OFF;	/* ADC 3 OFF */
	Temp_APB2ENR.BitField.LTDCEN   = RCC_OFF;	/* LCD-TFT control OFF */
	Temp_APB2ENR.BitField.SAI1EN   = RCC_OFF;   /* SAI 1 OFF */
	Temp_APB2ENR.BitField.SDIOEN   = RCC_OFF;   /* SDIO OFF */

	/* Assign value to the HW register */
	Reg_RCC_APB1ENR->Whole = Temp_APB1ENR.Whole;
	Reg_RCC_APB2ENR->Whole = Temp_APB2ENR.Whole;

	return;
}

