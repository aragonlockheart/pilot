/*
 * I2C_core.h
 *
 *  Created on: Nov 14, 2016
 *      Author: Mr.A
 */

#ifndef I2C_CORE_H_
#define I2C_CORE_H_

#include "stm32f4xx.h"
#include "Platform_Types.h"
#include "Remapping.h"

/* TRUE - FALSE values - Internal using for I2C core*/
#ifndef	FALSE
#define FALSE		0
#endif

#ifndef TRUE
#define TRUE		!(FALSE)
#endif
/****************************************************/

/* Vaules for I2C function Switch*/
#define I2C_FS_ENABLED					1
#define I2C_FS_DISABLED					0
/**********************************/


/* Macro derived from std_stm32f4 lib */
#define I2C_SPEED_STANDARD(__PCLK__, __SPEED__)            (((((__PCLK__)/((__SPEED__) << 1U)) & I2C_CCR_CCR) < 4U)? 4U:((__PCLK__) / ((__SPEED__) << 1U)))

#define I2C_SPEED_FAST(__PCLK__, __SPEED__, __DUTYCYCLE__) (((__DUTYCYCLE__) == I2C_DUTYCYCLE_2)? ((__PCLK__) / ((__SPEED__) * 3U)) : (((__PCLK__) / ((__SPEED__) * 25U)) | I2C_DUTYCYCLE_16_9))

#define I2C_SPEED(__PCLK__, __SPEED__, __DUTYCYCLE__)      (((__SPEED__) <= 100000U)? (I2C_SPEED_STANDARD((__PCLK__), (__SPEED__))) : \
                                                                  ((I2C_SPEED_FAST((__PCLK__), (__SPEED__), (__DUTYCYCLE__)) & I2C_CCR_CCR) == 0U)? 1U : \
                                                                  ((I2C_SPEED_FAST((__PCLK__), (__SPEED__), (__DUTYCYCLE__))) | I2C_CCR_FS))

#define I2C_RISE_TIME(__FREQRANGE__, __SPEED__)            (((__SPEED__) <= 100000U) ? ((__FREQRANGE__) + 1U) : ((((__FREQRANGE__) * 300U) / 1000U) + 1U))

#define I2C_DISABLE(__HANDLE__)                            ((__HANDLE__)->CR1 &=  ~I2C_CR1_PE)

#define I2C_ENABLE(__HANDLE__)                             ((__HANDLE__)->CR1 |=  I2C_CR1_PE)

#define I2C_STARTGENERATE(__HANDLE__)                      ((__HANDLE__)->CR1 |=  I2C_CR1_START)
#define I2C_STOPGENERATE(__HANDLE__)                       ((__HANDLE__)->CR1 |=  I2C_CR1_STOP)

#define I2C_ENABLE_ACK(__HANDLE__)                         ((__HANDLE__)->CR1 |=  I2C_CR1_ACK)
#define I2C_DISABLE_ACK(__HANDLE__)                        ((__HANDLE__)->CR1 &=  ~I2C_CR1_ACK)

#define I2C_ENABLE_POS(__HANDLE__)						   ((__HANDLE__)->CR1 |=  I2C_CR1_POS)
#define I2C_DISABLE_POS(__HANDLE__)						   ((__HANDLE__)->CR1 &=  ~I2C_CR1_POS)


#define I2C_FLAG_SMBALERT               ((uint32_t)0x00018000U)
#define I2C_FLAG_TIMEOUT                ((uint32_t)0x00014000U)
#define I2C_FLAG_PECERR                 ((uint32_t)0x00011000U)
#define I2C_FLAG_OVR                    ((uint32_t)0x00010800U)
#define I2C_FLAG_AF                     ((uint32_t)0x00010400U)
#define I2C_FLAG_ARLO                   ((uint32_t)0x00010200U)
#define I2C_FLAG_BERR                   ((uint32_t)0x00010100U)
#define I2C_FLAG_TXE                    ((uint32_t)0x00010080U)
#define I2C_FLAG_RXNE                   ((uint32_t)0x00010040U)
#define I2C_FLAG_STOPF                  ((uint32_t)0x00010010U)
#define I2C_FLAG_ADD10                  ((uint32_t)0x00010008U)
#define I2C_FLAG_BTF                    ((uint32_t)0x00010004U)
#define I2C_FLAG_ADDR                   ((uint32_t)0x00010002U)
#define I2C_FLAG_SB                     ((uint32_t)0x00010001U)
#define I2C_FLAG_DUALF                  ((uint32_t)0x00100080U)
#define I2C_FLAG_SMBHOST                ((uint32_t)0x00100040U)
#define I2C_FLAG_SMBDEFAULT             ((uint32_t)0x00100020U)
#define I2C_FLAG_GENCALL                ((uint32_t)0x00100010U)
#define I2C_FLAG_TRA                    ((uint32_t)0x00100004U)
#define I2C_FLAG_BUSY                   ((uint32_t)0x00100002U)
#define I2C_FLAG_MSL                    ((uint32_t)0x00100001U)
#define I2C_FLAG_MASK  					((uint32_t)0x0000FFFFU)

/** @brief  Checks whether the specified I2C flag is set or not.
  * @param  __HANDLE__: specifies the I2C Handle.
  *         This parameter can be I2C where x: 1, 2, or 3 to select the I2C peripheral.
  * @param  __FLAG__: specifies the flag to check.
  *         This parameter can be one of the following values:
  *            @arg I2C_FLAG_SMBALERT: SMBus Alert flag
  *            @arg I2C_FLAG_TIMEOUT: Timeout or Tlow error flag
  *            @arg I2C_FLAG_PECERR: PEC error in reception flag
  *            @arg I2C_FLAG_OVR: Overrun/Underrun flag
  *            @arg I2C_FLAG_AF: Acknowledge failure flag
  *            @arg I2C_FLAG_ARLO: Arbitration lost flag
  *            @arg I2C_FLAG_BERR: Bus error flag
  *            @arg I2C_FLAG_TXE: Data register empty flag
  *            @arg I2C_FLAG_RXNE: Data register not empty flag
  *            @arg I2C_FLAG_STOPF: Stop detection flag
  *            @arg I2C_FLAG_ADD10: 10-bit header sent flag
  *            @arg I2C_FLAG_BTF: Byte transfer finished flag
  *            @arg I2C_FLAG_ADDR: Address sent flag
  *                                Address matched flag
  *            @arg I2C_FLAG_SB: Start bit flag
  *            @arg I2C_FLAG_DUALF: Dual flag
  *            @arg I2C_FLAG_SMBHOST: SMBus host header
  *            @arg I2C_FLAG_SMBDEFAULT: SMBus default header
  *            @arg I2C_FLAG_GENCALL: General call header flag
  *            @arg I2C_FLAG_TRA: Transmitter/Receiver flag
  *            @arg I2C_FLAG_BUSY: Bus busy flag
  *            @arg I2C_FLAG_MSL: Master/Slave flag
  * @retval The new state of __FLAG__ (TRUE or FALSE).
  */
#define I2C_GET_FLAG(__HANDLE__, __FLAG__) ((((uint8_t)((__FLAG__) >> 16U)) == 0x01U)?((((__HANDLE__)->SR1) & ((__FLAG__) & I2C_FLAG_MASK)) == ((__FLAG__) & I2C_FLAG_MASK)): \
																					  ((((__HANDLE__)->SR2) & ((__FLAG__) & I2C_FLAG_MASK)) == ((__FLAG__) & I2C_FLAG_MASK)))
#define UNUSED(x) ((void)(x))
	
#define I2C_CLEAR_ADDRFLAG(__HANDLE__)\
  do{                                 \
    __IO uint32_t tmpreg = 0x00U;     \
    tmpreg = (__HANDLE__)->SR1;       \
    tmpreg = (__HANDLE__)->SR2;       \
    UNUSED(tmpreg);                   \
  } while(0)

#define HAL_RCC_I2C1_CLK_ENABLE()     do { \
                                        __IO uint32_t tmpreg = 0x00U; \
                                        SET_BIT(RCC->APB1ENR, RCC_APB1ENR_I2C1EN);\
                                        /* Delay after an RCC peripheral clock enabling */ \
                                        tmpreg = READ_BIT(RCC->APB1ENR, RCC_APB1ENR_I2C1EN);\
                                        UNUSED(tmpreg); \
                                          } while(0)

/*****************************************************************************/

 /*I2C_Duty option*/
#define I2C_DUTYCYCLE_2                 ((uint32_t)0x00000000U)
#define I2C_DUTYCYCLE_16_9              I2C_CCR_DUTY

/*I2C_AddressMode option*/
#define I2C_ADDRESSINGMODE_7BIT         ((uint32_t)0x00004000U)
#define I2C_ADDRESSINGMODE_10BIT        (I2C_OAR1_ADDMODE | ((uint32_t)0x00004000U))

typedef enum {
	I2C_NOTINITIALIZED,
	I2C_READY,
	I2C_RECEIVING,
	I2C_TRANSMITING,
	I2C_MEMADRREQTX,
	I2C_MEMADRREQRX,
	I2C_RXDATAREADY,
	I2C_RX_ERROR,
	I2C_TX_ERROR
} I2C_StatesDefinition;

typedef enum {
	I2C_RX_STARTING,
	I2C_RX_ADDRESSING,
	I2C_RX_SETUP,
	I2C_RX_1BYTERECEIVING,
	I2C_RX_2BYTERECEIVING,
	I2C_RX_NORRECEIVING,
	I2C_RX_COMPLETED,
	I2C_RX_INVALID
} I2C_RxStepsDefinition;

typedef enum {
	I2C_TX_STARTING,
	I2C_TX_ADDRESSING,
	I2C_TX_SETUP,
	I2C_TX_TRANSMITING,
	I2C_TX_COMPLETED,
	I2C_TX_RESTART,
	I2C_TX_INVALID
} I2C_TxStepsDefinition;

typedef enum {
	I2C_OK,
	I2C_NOT_OK
}I2C_StatusType;

#define I2C1_HANDLER					1
#define I2C2_HANDLER					2
#define I2C3_HANDLER					3

#define I2C_GETBASE(__I2C_HANDLER_ID__)		((__I2C_HANDLER_ID__ == I2C1_HANDLER)? I2C1 : ((__I2C_HANDLER_ID__ == I2C2_HANDLER)? I2C2 : I2C3))

typedef struct {
	 uint8_t 		I2C_HandlerID;
	 uint32_t 		I2C_Speed;
	 uint32_t		I2C_Duty;
	 uint8_t		I2C_GeneralCallSupport;				//TRUE or FALSE
	 uint8_t		I2C_StretchClockSupport; 			//TRUE or FALSE
	 uint32_t		I2C_AddressMode;
	 uint32_t		I2C_Address0;
	 uint8_t		I2C_DualAddressSupport;				//TRUE or FALSE
	 uint32_t		I2C_Address1;

} I2C_ConfigType;

typedef struct {
	I2C_TypeDef*									I2C_Base;
	__IO I2C_StatesDefinition						I2C_States;
	uint16_t										I2C_SlaveAddress;
	uint16_t										I2C_MemAdrReq;
	
	__IO I2C_RxStepsDefinition						I2C_RxSteps;
	__IO uint8_t* 									I2C_pRxBuffer;
	__IO uint8_t									I2C_RxLength;

	__IO I2C_TxStepsDefinition						I2C_TxSteps;
	__IO uint8_t* 									I2C_pTxBuffer;
	__IO uint8_t									I2C_TxLength;

	__IO uint8_t 									I2C_TOMonitoring;
} I2C_HandlerType;

extern volatile uint8_t initialized;

void I2C_Init(void);
void I2C_ReceiveProcess(I2C_HandlerType* Handler);
void I2C_TransmitProcess(I2C_HandlerType* Handler);

I2C_StatusType I2C_UserReceive_MemRequest(I2C_HandlerType* Handler, uint16_t SlaveAddress, uint16_t MemAdrReq, uint8_t* pRxData, uint8_t Length);
I2C_StatusType I2C_UserReceive(I2C_HandlerType* Handler, uint16_t SlaveAddress, uint8_t* pRxData, uint8_t Length);

I2C_StatusType I2C_UserTransmit_MemRequest(I2C_HandlerType* Handler, uint16_t SlaveAddress, uint16_t MemAdrReq, uint8_t* pTxData, uint8_t Length);
I2C_StatusType I2C_UserTransmit(I2C_HandlerType* Handler, uint16_t SlaveAddress, uint8_t* pTxData, uint8_t Length);

I2C_StatusType I2C_User_IsReceiveCompleted(I2C_HandlerType* Handler);
#endif /* I2C_CORE_H_ */
