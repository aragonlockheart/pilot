/*
 * I2C_Core.c
 *
 *  Created on: Nov 14, 2016
 *      Author: Mr.A
 */
#include "I2C_core.h"
#include "I2C_Cfg.h"
#include "I2C_Parameter.h"

/*	PRIVATE VARIABLE DECLARATION BEGIN*/

/*	PRIVATE VARIABLE DECLARATION END*/

/*	PRIVATE FUNCTION DECLARATION BEGIN*/

uint32_t I2C_Get_PCLK1_Clock(void);
void I2C_SubInit(const I2C_ConfigType* ConfigPtr);
void I2C_Mon_CounterReset(I2C_HandlerType* Handler);
I2C_StatusType I2C_MOn_TOChecking_IsOK(I2C_HandlerType* Handler);

/*	PRIVATE FUNCTION DECLARATION END*/



void I2C_Init(void)
{

	initialized = 1;
#if (FS_I2C_CHANNEL_1_ENABLED == I2C_FS_ENABLED)
	I2C_SubInit(&UserCfg_I2C_Channel_1);

	// initialize I2C_Handler
	I2C_Handler_1.I2C_Base = I2C_GETBASE(UserCfg_I2C_Channel_1.I2C_HandlerID);
	I2C_Handler_1.I2C_States = I2C_READY;
	I2C_Handler_1.I2C_RxSteps = I2C_RX_INVALID;
	I2C_Handler_1.I2C_TxSteps = I2C_TX_INVALID;
	I2C_Handler_1.I2C_TOMonitoring = 0;
	// ... to be updated.

#endif

#if (FS_I2C_CHANNEL_2_ENABLED == I2C_FS_ENABLED)
	I2C_SubInit(&UserCfg_I2C_Channel_2);

	// initialize I2C_Handler
	I2C_Handler_2.I2C_Base = I2C_GETBASE(UserCfg_I2C_Channel_2.I2C_HandlerID);
	I2C_Handler_2.I2C_States = I2C_READY;
	I2C_Handler_2.I2C_RxSteps = I2C_RX_INVALID;
	I2C_Handler_2.I2C_TxSteps = I2C_TX_INVALID;
	I2C_Handler_2.I2C_TOMonitoring = 0;
	// ... to be updated.

#endif

#if (FS_I2C_CHANNEL_3_ENABLED == I2C_FS_ENABLED)
	I2C_SubInit(&UserCfg_I2C_Channel_3);

	// initialize I2C_Handler
	I2C_Handler_3.I2C_Base = I2C_GETBASE(UserCfg_I2C_Channel_3.I2C_HandlerID);
	I2C_Handler_3.I2C_States = I2C_READY;
	I2C_Handler_3.I2C_RxSteps = I2C_RX_INVALID;
	I2C_Handler_3.I2C_TxSteps = I2C_TX_INVALID;
	I2C_Handler_3.I2C_TOMonitoring = 0;
	// ... to be updated.

#endif

}

void I2C_ReceiveProcess(I2C_HandlerType* Handler)
{
	if (I2C_RECEIVING == Handler->I2C_States)
	{
		switch (Handler->I2C_RxSteps)
		{
		case I2C_RX_STARTING:
		{

			I2C_DISABLE_POS(Handler->I2C_Base);
			I2C_STARTGENERATE(Handler->I2C_Base);
			Handler->I2C_RxSteps = I2C_RX_ADDRESSING;

			I2C_Mon_CounterReset(Handler);
			break;
		}
		case I2C_RX_ADDRESSING:
		{
			if (I2C_GET_FLAG(Handler->I2C_Base, I2C_FLAG_SB))
			{
				// SB flag alread set --> ADDRESS send
				// 7 bit mode only
				Handler->I2C_Base->DR = (uint8_t)( Handler->I2C_SlaveAddress | 0x0001);
				Handler->I2C_RxSteps = I2C_RX_SETUP;
				I2C_Mon_CounterReset(Handler);
			}
			else
			{
				if (I2C_NOT_OK == I2C_MOn_TOChecking_IsOK(Handler))
				{
					Handler->I2C_States = I2C_RX_ERROR;
					LED_RED_ON();
				}
			}
			break;
		}
		case I2C_RX_SETUP:
		{
			if (I2C_GET_FLAG(Handler->I2C_Base, I2C_FLAG_ADDR))
			{
				switch(Handler->I2C_RxLength)
				{
				case 0:
				{
					I2C_DISABLE_ACK(Handler->I2C_Base);
					I2C_CLEAR_ADDRFLAG(Handler->I2C_Base);
					I2C_STOPGENERATE(Handler->I2C_Base);
					Handler->I2C_RxSteps = I2C_RX_INVALID;		// taken care
					Handler->I2C_States = I2C_RX_ERROR;

					break;
				}
				case 1:
				{
					I2C_DISABLE_ACK(Handler->I2C_Base);
					I2C_CLEAR_ADDRFLAG(Handler->I2C_Base);
					I2C_STOPGENERATE(Handler->I2C_Base);
					Handler->I2C_RxSteps = I2C_RX_1BYTERECEIVING;
					break;
				}
				case 2:
				{
					I2C_DISABLE_ACK(Handler->I2C_Base);
					I2C_ENABLE_POS(Handler->I2C_Base);
					I2C_CLEAR_ADDRFLAG(Handler->I2C_Base);
					Handler->I2C_RxSteps = I2C_RX_2BYTERECEIVING;
					break;
				}
				default:
				{
					I2C_ENABLE_ACK(Handler->I2C_Base);
					I2C_CLEAR_ADDRFLAG(Handler->I2C_Base);
					Handler->I2C_RxSteps = I2C_RX_NORRECEIVING;

					break;
				}
				}

				I2C_Mon_CounterReset(Handler);
			}
			else
			{
				if (I2C_NOT_OK == I2C_MOn_TOChecking_IsOK(Handler))
				{
					Handler->I2C_States = I2C_RX_ERROR;
					LED_RED_ON();
				}
			}
			break;
		}
		case I2C_RX_1BYTERECEIVING:
		{
			if (I2C_GET_FLAG(Handler->I2C_Base, I2C_FLAG_RXNE))
			{
				*(Handler->I2C_pRxBuffer++) = Handler->I2C_Base->DR;
				Handler->I2C_RxLength--;
				Handler->I2C_RxSteps = I2C_RX_COMPLETED;
				Handler->I2C_States = I2C_RXDATAREADY;

				I2C_Mon_CounterReset(Handler);
			}
			else
			{
				if (I2C_NOT_OK == I2C_MOn_TOChecking_IsOK(Handler))
				{
					Handler->I2C_States = I2C_RX_ERROR;
					LED_RED_ON();
				}
			}
			break;
		}
		case I2C_RX_2BYTERECEIVING:
		{
			if (I2C_GET_FLAG(Handler->I2C_Base, I2C_FLAG_BTF))
			{
				I2C_STOPGENERATE(Handler->I2C_Base);
				*(Handler->I2C_pRxBuffer++) = Handler->I2C_Base->DR;
				Handler->I2C_RxLength--;

				*(Handler->I2C_pRxBuffer++) = Handler->I2C_Base->DR;
				Handler->I2C_RxLength--;

				Handler->I2C_RxSteps = I2C_RX_COMPLETED;
				Handler->I2C_States = I2C_RXDATAREADY;

				I2C_Mon_CounterReset(Handler);
			}
			else
			{
				if (I2C_NOT_OK == I2C_MOn_TOChecking_IsOK(Handler))
				{
					Handler->I2C_States = I2C_RX_ERROR;
					LED_RED_ON();
				}
			}
			break;
		}
		case I2C_RX_NORRECEIVING:
		{
			if (I2C_GET_FLAG(Handler->I2C_Base, I2C_FLAG_BTF))
			{
				if (Handler->I2C_RxLength > 3)
				{
					*(Handler->I2C_pRxBuffer++) = Handler->I2C_Base->DR;
					Handler->I2C_RxLength--;
				}
				else
				{
					I2C_DISABLE_ACK(Handler->I2C_Base);
					*(Handler->I2C_pRxBuffer++) = Handler->I2C_Base->DR;
					Handler->I2C_RxLength--;
					Handler->I2C_RxSteps = I2C_RX_2BYTERECEIVING;
				}

				I2C_Mon_CounterReset(Handler);
			}
			else
			{
				if (I2C_NOT_OK == I2C_MOn_TOChecking_IsOK(Handler))
				{
					Handler->I2C_States = I2C_RX_ERROR;
					LED_RED_ON();
				}
			}
			break;
		}
		default:
		{
			// not defined yet
		}
		}
	}
	else if (I2C_RX_ERROR == Handler->I2C_States)
	{
		/* Reset system*/
		Handler->I2C_States = I2C_READY;
		Handler->I2C_RxSteps = I2C_RX_INVALID;
		Handler->I2C_TxSteps = I2C_TX_INVALID;

		/* Clear BUSY bit*/
		I2C_STOPGENERATE(Handler->I2C_Base);
		LED_RED_OFF();
	}
}



void I2C_TransmitProcess(I2C_HandlerType* Handler)
{
	if ((I2C_TRANSMITING == Handler->I2C_States) || (I2C_MEMADRREQTX == Handler->I2C_States) || (I2C_MEMADRREQRX == Handler->I2C_States))
	{
		switch (Handler->I2C_TxSteps)
		{
		case I2C_TX_STARTING:
		{
			I2C_DISABLE_POS(Handler->I2C_Base);
			I2C_STARTGENERATE(Handler->I2C_Base);
			Handler->I2C_TxSteps = I2C_TX_ADDRESSING;

			I2C_Mon_CounterReset(Handler);
			break;
		}
		case I2C_TX_ADDRESSING:
		{
			if (I2C_GET_FLAG(Handler->I2C_Base, I2C_FLAG_SB))
			{
				// SB flag already set --> ADDRESS send
				// 7 bit mode only
				Handler->I2C_Base->DR = (uint8_t)( Handler->I2C_SlaveAddress & 0xFFFE);
				Handler->I2C_TxSteps = I2C_TX_SETUP;

				I2C_Mon_CounterReset(Handler);
			}
			else
			{
				if (I2C_NOT_OK == I2C_MOn_TOChecking_IsOK(Handler))
				{
					Handler->I2C_States = I2C_TX_ERROR;
					LED_RED_ON();
				}
			}
			break;
		}
		case I2C_TX_SETUP:
		{

			if (I2C_GET_FLAG(Handler->I2C_Base, I2C_FLAG_ADDR))
			{
				I2C_CLEAR_ADDRFLAG(Handler->I2C_Base);
				Handler->I2C_TxSteps = I2C_TX_TRANSMITING;

				I2C_Mon_CounterReset(Handler);
			}
			else
			{
				if (I2C_NOT_OK == I2C_MOn_TOChecking_IsOK(Handler))
				{
					Handler->I2C_States = I2C_TX_ERROR;
					LED_RED_ON();
				}
			}
			break;
		}
		case I2C_TX_RESTART:
		{
			if (I2C_GET_FLAG(Handler->I2C_Base, I2C_FLAG_TXE))
			{
				Handler->I2C_RxSteps = I2C_RX_STARTING;
				Handler->I2C_States = I2C_RECEIVING;
				Handler->I2C_TxSteps = I2C_TX_COMPLETED;
				// jump to ReceiveProcess

				I2C_Mon_CounterReset(Handler);
			}
			else
			{
				if (I2C_NOT_OK == I2C_MOn_TOChecking_IsOK(Handler))
				{
					Handler->I2C_States = I2C_TX_ERROR;
					LED_RED_ON();
				}
			}
			break;
		}
		case I2C_TX_TRANSMITING:
		{
			if (I2C_GET_FLAG(Handler->I2C_Base, I2C_FLAG_TXE))
			{
				switch (Handler->I2C_States)
				{
				case I2C_TRANSMITING:
				{
					if (Handler->I2C_TxLength > 0)
					{
						Handler->I2C_Base->DR = *(Handler->I2C_pTxBuffer++);
						Handler->I2C_TxLength--;
					}
					else
					{
						if (I2C_GET_FLAG(Handler->I2C_Base, I2C_FLAG_BTF))
						{
							I2C_STOPGENERATE(Handler->I2C_Base);
							Handler->I2C_TxSteps = I2C_TX_COMPLETED;
							Handler->I2C_States = I2C_READY;

							I2C_Mon_CounterReset(Handler);
						}
						else
						{
							if (I2C_NOT_OK == I2C_MOn_TOChecking_IsOK(Handler))
							{
								Handler->I2C_States = I2C_TX_ERROR;
								LED_RED_ON();
							}
						}
					}
					break;
				}
				case I2C_MEMADRREQTX:
				{
					Handler->I2C_Base->DR = (uint8_t)(Handler->I2C_MemAdrReq);
					Handler->I2C_States = I2C_TRANSMITING;
					Handler->I2C_TxSteps = I2C_TX_TRANSMITING;
					break;
				}
				case I2C_MEMADRREQRX:
				{
					Handler->I2C_Base->DR = (uint8_t)(Handler->I2C_MemAdrReq);
					Handler->I2C_TxSteps = I2C_TX_RESTART;

					break;
				}
				default:
				{
					break;
				}
				}

				I2C_Mon_CounterReset(Handler);
			}
			else
			{
				if (I2C_NOT_OK == I2C_MOn_TOChecking_IsOK(Handler))
				{
					Handler->I2C_States = I2C_TX_ERROR;
					LED_RED_ON();
				}
			}
			break;
		}
		default:
		{
			// not defined yet
		}
		}
	}
	else if (I2C_TX_ERROR == Handler->I2C_States)
	{
		/* Reset system*/
		Handler->I2C_States = I2C_READY;
		Handler->I2C_RxSteps = I2C_RX_INVALID;
		Handler->I2C_TxSteps = I2C_TX_INVALID;

		/* Clear BUSY bit*/
		I2C_STOPGENERATE(Handler->I2C_Base);
		LED_RED_OFF();
	}
}

/*	PRIVATE FUNCTION BEGIN*/

void I2C_Mon_CounterReset(I2C_HandlerType* Handler)
{
	Handler->I2C_TOMonitoring = 0;
}

uint8_t I2C_MOn_TOChecking_IsOK(I2C_HandlerType* Handler)
{
	Handler->I2C_TOMonitoring++;
	if (Handler->I2C_TOMonitoring >= I2C_TO_THRESHOLD)
	{
		Handler->I2C_TOMonitoring = 0;
		return I2C_NOT_OK;
	}
	return I2C_OK;
}

I2C_StatusType I2C_UserReceive_MemRequest(I2C_HandlerType* Handler, uint16_t SlaveAddress, uint16_t MemAdrReq, uint8_t* pRxData, uint8_t Length)
{
	if ((I2C_READY == Handler->I2C_States) && (!(I2C_GET_FLAG(Handler->I2C_Base, I2C_FLAG_BUSY))))
	{
		Handler->I2C_TxSteps = I2C_TX_STARTING;
		Handler->I2C_MemAdrReq = MemAdrReq;
		Handler->I2C_SlaveAddress = SlaveAddress;
		Handler->I2C_RxLength = Length;
		Handler->I2C_RxSteps = I2C_RX_INVALID;
		Handler->I2C_pRxBuffer = pRxData;

		Handler->I2C_States = I2C_MEMADRREQRX;
		return I2C_OK;
	}
	return I2C_NOT_OK;
}

I2C_StatusType I2C_UserReceive(I2C_HandlerType* Handler, uint16_t SlaveAddress, uint8_t* pRxData, uint8_t Length)
{
	if ((I2C_READY == Handler->I2C_States) && (!(I2C_GET_FLAG(Handler->I2C_Base, I2C_FLAG_BUSY))))
	{
		Handler->I2C_SlaveAddress = SlaveAddress;
		Handler->I2C_RxLength = Length;
		Handler->I2C_RxSteps = I2C_RX_STARTING;
		Handler->I2C_pRxBuffer = pRxData;

		Handler->I2C_States = I2C_RECEIVING;
		return I2C_OK;
	}
	return I2C_NOT_OK;
}

I2C_StatusType I2C_UserTransmit_MemRequest(I2C_HandlerType* Handler, uint16_t SlaveAddress, uint16_t MemAdrReq, uint8_t* pTxData, uint8_t Length)
{
	if ((I2C_READY == Handler->I2C_States) && (!(I2C_GET_FLAG(Handler->I2C_Base, I2C_FLAG_BUSY))))
	{
		Handler->I2C_TxSteps = I2C_TX_STARTING;
		Handler->I2C_MemAdrReq = MemAdrReq;
		Handler->I2C_SlaveAddress = SlaveAddress;
		Handler->I2C_TxLength = Length;
		Handler->I2C_pTxBuffer = pTxData;

		Handler->I2C_States = I2C_MEMADRREQTX;
		return I2C_OK;
	}
	return I2C_NOT_OK;
}

I2C_StatusType I2C_UserTransmit(I2C_HandlerType* Handler, uint16_t SlaveAddress, uint8_t* pTxData, uint8_t Length)
{
	if ((I2C_READY == Handler->I2C_States) && (!(I2C_GET_FLAG(Handler->I2C_Base, I2C_FLAG_BUSY))))
	{
		Handler->I2C_TxSteps = I2C_TX_STARTING;
		Handler->I2C_SlaveAddress = SlaveAddress;
		Handler->I2C_TxLength = Length;
		Handler->I2C_pTxBuffer = pTxData;

		Handler->I2C_States = I2C_TRANSMITING;
		return I2C_OK;
	}
	return I2C_NOT_OK;
}

I2C_StatusType I2C_User_IsReceiveCompleted(I2C_HandlerType* Handler)
{
	if (Handler->I2C_States == I2C_RXDATAREADY)
	{
		/* Turn system to READY state*/
		Handler->I2C_States = I2C_READY;
		return I2C_OK;
	}
	return I2C_NOT_OK;
}
/* Get PCLK1 clock value which provided for I2C operation
 * NOTE: NOT IMPLEMENTED YET, RETURN DEFAULT 42 000 000 Hz
 */
uint32_t I2C_Get_PCLK1_Clock(void)
{
	// to be added code here
	return 42000000;
}

void I2C_SubInit(const I2C_ConfigType* ConfigPtr)
{
	uint32_t l_pclk1 = 0, l_pclk1_temp = 0;
	uint32_t l_trise = 0;
	I2C_TypeDef* l_I2C_Base = I2C_GETBASE(ConfigPtr->I2C_HandlerID);

	// disable I2C before configuration
	I2C_DISABLE(l_I2C_Base);

	// get pclk1 clock value.
	l_pclk1 = I2C_Get_PCLK1_Clock();
	// format value to FREQ[5:0] of CR2
	l_pclk1_temp = l_pclk1 / 1000000;
	l_pclk1_temp &= 0x0000003F;
	// prepare CR2 register
	l_I2C_Base->CR2 &= 0xFFFFFFC0;
	// put FREQ[5:0] value
	l_I2C_Base->CR2 |= l_pclk1_temp;

	// Calculate TRISE
	l_trise = I2C_RISE_TIME(l_pclk1_temp, ConfigPtr->I2C_Speed);
	l_trise &= 0x0000003F;
	l_I2C_Base->TRISE &= 0xFFFFFFC0;
	// put TRISE[5:0] value
	l_I2C_Base->TRISE |= l_trise;

	// CCR configuration; temporary copy from st lib, should be considered again
	l_I2C_Base->CCR = I2C_SPEED(l_pclk1, ConfigPtr->I2C_Speed, ConfigPtr->I2C_Duty);

	if (ConfigPtr->I2C_GeneralCallSupport)
	{
		l_I2C_Base->CR1 |= I2C_CR1_ENGC;
	}
	else
	{
		l_I2C_Base->CR1 &= (~I2C_CR1_ENGC);
	}

	if (ConfigPtr->I2C_StretchClockSupport)
	{
		l_I2C_Base->CR1 &= (~I2C_CR1_NOSTRETCH);
	}
	else
	{
		l_I2C_Base->CR1 |= I2C_CR1_NOSTRETCH;
	}

	l_I2C_Base->OAR1 = (ConfigPtr->I2C_AddressMode | (ConfigPtr->I2C_Address0 & 0x3FF));

	// be noted when DualAddress is used, AddressMode must be 7 bit for both or what?
	if (ConfigPtr->I2C_DualAddressSupport)
	{
		l_I2C_Base->OAR2 = ((ConfigPtr->I2C_Address1 & 0x000000FF) | 0x00000001);
	}
	else
	{
		l_I2C_Base->OAR2 = 0x00000000;
	}

	// will updated for DMA feature...
	I2C_ENABLE(l_I2C_Base);
}
/*	PRIVATE FUNCTION END*/
