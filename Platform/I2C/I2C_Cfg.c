/*
 * I2C_Cfg.c
 *
 *  Created on: Nov 22, 2016
 *      Author: Mr.A
 */

#include "I2C_Cfg.h"
#include "I2C_Parameter.h"


	/*------THE BEGIN OF USER EDIT SECTION---------*/
#if (FS_I2C_CHANNEL_1_ENABLED == I2C_FS_ENABLED)
const I2C_ConfigType UserCfg_I2C_Channel_1 =
	{
		I2C1_HANDLER,
		400000,
		I2C_DUTYCYCLE_16_9,
		FALSE,
		TRUE,
		I2C_ADDRESSINGMODE_7BIT,
		0x01,
		FALSE,
		0xFF,
	};
#endif

#if (FS_I2C_CHANNEL_2_ENABLED == I2C_FS_ENABLED)
const I2C_ConfigType UserCfg_I2C_Channel_2 =
	{
		I2C2_HANDLER,
		400000,
		I2C_DUTYCYCLE_16_9,
		FALSE,
		TRUE,
		I2C_ADDRESSINGMODE_7BIT,
		0x01,
		FALSE,
		0xFF,
	};
#endif

#if (FS_I2C_CHANNEL_3_ENABLED == I2C_FS_ENABLED)
const I2C_ConfigType UserCfg_I2C_Channel_3 =
	{
		I2C3_HANDLER,
		400000,
		I2C_DUTYCYCLE_16_9,
		FALSE,
		TRUE,
		I2C_ADDRESSINGMODE_7BIT,
		0x01,
		FALSE,
		0xFF,
	};
#endif
		/*------THE END OF USER EDIT SECTION---------*/
		/*
		I2C_TypeDef*	I2C_Base;
		uint32_t 		I2C_Speed;
		uint32_t		I2C_Duty;
		uint8_t			I2C_GeneralCallSupport;				//TRUE or FALSE
		uint8_t			I2C_StretchClockSupport; 			//TRUE or FALSE
		uint32_t		I2C_AddressMode;
		uint32_t		I2C_Address0;
		uint8_t			I2C_DualAddressSupport;				//TRUE or FALSE
		uint32_t		I2C_Address1;
		*/


#if (FS_I2C_CHANNEL_1_ENABLED == I2C_FS_ENABLED)
	I2C_HandlerType I2C_Handler_1;
#endif

#if (FS_I2C_CHANNEL_2_ENABLED == I2C_FS_ENABLED)
	I2C_HandlerType I2C_Handler_2;
#endif

#if (FS_I2C_CHANNEL_3_ENABLED == I2C_FS_ENABLED)
	I2C_HandlerType I2C_Handler_3;
#endif
