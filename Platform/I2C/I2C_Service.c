/*
 * I2C_Service.c
 *
 *  Created on: May 27, 2017
 *      Author: Mr.A
 */
#include "I2C_Service.h"


I2C_StatusType I2C_UserReceive_MemRequest_Chnl1(uint16_t SlaveAddress, uint16_t MemAdrReq, uint8_t* pRxData, uint8_t Length)
{
#if (FS_I2C_CHANNEL_1_ENABLED == I2C_FS_ENABLED)
	I2C_StatusType l_ReVal;
	l_ReVal = I2C_UserReceive_MemRequest(&I2C_Handler_1, SlaveAddress, MemAdrReq, pRxData, Length);
	return l_ReVal;
#else
	return I2C_NOT_OK;
#endif
}

I2C_StatusType I2C_UserReceive_MemRequest_Chnl2(uint16_t SlaveAddress, uint16_t MemAdrReq, uint8_t* pRxData, uint8_t Length)
{
#if (FS_I2C_CHANNEL_2_ENABLED == I2C_FS_ENABLED)
	I2C_StatusType l_ReVal;
	l_ReVal = I2C_UserReceive_MemRequest(&I2C_Handler_2, SlaveAddress, MemAdrReq, pRxData, Length);
	return l_ReVal;
#else
	return I2C_NOT_OK;
#endif
}

I2C_StatusType I2C_UserReceive_MemRequest_Chnl3(uint16_t SlaveAddress, uint16_t MemAdrReq, uint8_t* pRxData, uint8_t Length)
{
#if (FS_I2C_CHANNEL_3_ENABLED == I2C_FS_ENABLED)
	I2C_StatusType l_ReVal;
	l_ReVal = I2C_UserReceive_MemRequest(&I2C_Handler_3, SlaveAddress, MemAdrReq, pRxData, Length);
	return l_ReVal;
#else
	return I2C_NOT_OK;
#endif
}

I2C_StatusType I2C_UserReceive_Chnl1(uint16_t SlaveAddress, uint8_t* pRxData, uint8_t Length)
{
#if (FS_I2C_CHANNEL_1_ENABLED == I2C_FS_ENABLED)
	I2C_StatusType l_ReVal;
	l_ReVal = I2C_UserReceive(&I2C_Handler_1, SlaveAddress, pRxData, Length);
	return l_ReVal;
#else
	return I2C_NOT_OK;
#endif
}

I2C_StatusType I2C_UserReceive_Chnl2(uint16_t SlaveAddress, uint8_t* pRxData, uint8_t Length)
{
#if (FS_I2C_CHANNEL_2_ENABLED == I2C_FS_ENABLED)
	I2C_StatusType l_ReVal;
	l_ReVal = I2C_UserReceive(&I2C_Handler_2, SlaveAddress, pRxData, Length);
	return l_ReVal;
#else
	return I2C_NOT_OK;
#endif
}

I2C_StatusType I2C_UserReceive_Chnl3(uint16_t SlaveAddress, uint8_t* pRxData, uint8_t Length)
{
#if (FS_I2C_CHANNEL_3_ENABLED == I2C_FS_ENABLED)
	I2C_StatusType l_ReVal;
	l_ReVal = I2C_UserReceive(&I2C_Handler_3, SlaveAddress, pRxData, Length);
	return l_ReVal;
#else
	return I2C_NOT_OK;
#endif
}


I2C_StatusType I2C_UserTransmit_MemRequest_Chnl1(uint16_t SlaveAddress, uint16_t MemAdrReq, uint8_t* pTxData, uint8_t Length)
{
#if (FS_I2C_CHANNEL_1_ENABLED == I2C_FS_ENABLED)
	I2C_StatusType l_ReVal;
	l_ReVal = I2C_UserTransmit_MemRequest(&I2C_Handler_1, SlaveAddress, MemAdrReq, pTxData, Length);
	return l_ReVal;
#else
	return I2C_NOT_OK;
#endif
}

I2C_StatusType I2C_UserTransmit_MemRequest_Chnl2(uint16_t SlaveAddress, uint16_t MemAdrReq, uint8_t* pTxData, uint8_t Length)
{
#if (FS_I2C_CHANNEL_2_ENABLED == I2C_FS_ENABLED)
	I2C_StatusType l_ReVal;
	l_ReVal = I2C_UserTransmit_MemRequest(&I2C_Handler_2, SlaveAddress, MemAdrReq, pTxData, Length);
	return l_ReVal;
#else
	return I2C_NOT_OK;
#endif
}

I2C_StatusType I2C_UserTransmit_MemRequest_Chnl3(uint16_t SlaveAddress, uint16_t MemAdrReq, uint8_t* pTxData, uint8_t Length)
{
#if (FS_I2C_CHANNEL_3_ENABLED == I2C_FS_ENABLED)
	I2C_StatusType l_ReVal;
	l_ReVal = I2C_UserTransmit_MemRequest(&I2C_Handler_3, SlaveAddress, MemAdrReq, pTxData, Length);
	return l_ReVal;
#else
	return I2C_NOT_OK;
#endif
}


I2C_StatusType I2C_UserTransmit_Chnl1(uint16_t SlaveAddress, uint8_t* pTxData, uint8_t Length)
{
#if (FS_I2C_CHANNEL_1_ENABLED == I2C_FS_ENABLED)
	I2C_StatusType l_ReVal;
	l_ReVal = I2C_UserTransmit(&I2C_Handler_1, SlaveAddress, pTxData, Length);
	return l_ReVal;
#else
	return I2C_NOT_OK;
#endif
}

I2C_StatusType I2C_UserTransmit_Chnl2(uint16_t SlaveAddress, uint8_t* pTxData, uint8_t Length)
{
#if (FS_I2C_CHANNEL_2_ENABLED == I2C_FS_ENABLED)
	I2C_StatusType l_ReVal;
	l_ReVal = I2C_UserTransmit(&I2C_Handler_2, SlaveAddress, pTxData, Length);
	return l_ReVal;
#else
	return I2C_NOT_OK;
#endif
}

I2C_StatusType I2C_UserTransmit_Chnl3(uint16_t SlaveAddress, uint8_t* pTxData, uint8_t Length)
{
#if (FS_I2C_CHANNEL_3_ENABLED == I2C_FS_ENABLED)
	I2C_StatusType l_ReVal;
	l_ReVal = I2C_UserTransmit(&I2C_Handler_3, SlaveAddress, pTxData, Length);
	return l_ReVal;
#else
	return I2C_NOT_OK;
#endif
}

I2C_StatusType I2C_User_IsReceiveCompleted_Chnl1(void)
{
#if (FS_I2C_CHANNEL_1_ENABLED == I2C_FS_ENABLED)
	I2C_StatusType l_ReVal;
	l_ReVal = I2C_User_IsReceiveCompleted(&I2C_Handler_1);
	return l_ReVal;
#else
	return I2C_NOT_OK;
#endif
}

I2C_StatusType I2C_User_IsReceiveCompleted_Chnl2(void)
{
#if (FS_I2C_CHANNEL_2_ENABLED == I2C_FS_ENABLED)
	I2C_StatusType l_ReVal;
	l_ReVal = I2C_User_IsReceiveCompleted(&I2C_Handler_2);
	return l_ReVal;
#else
	return I2C_NOT_OK;
#endif
}

I2C_StatusType I2C_User_IsReceiveCompleted_Chnl3(void)
{
#if (FS_I2C_CHANNEL_3_ENABLED == I2C_FS_ENABLED)
	I2C_StatusType l_ReVal;
	l_ReVal = I2C_User_IsReceiveCompleted(&I2C_Handler_3);
	return l_ReVal;
#else
	return I2C_NOT_OK;
#endif
}
