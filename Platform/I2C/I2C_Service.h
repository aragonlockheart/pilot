/*
 * I2C_Service.h
 *
 *  Created on: May 27, 2017
 *      Author: Mr.A
 */

#ifndef PLATFORM_I2C_I2C_SERVICE_H_
#define PLATFORM_I2C_I2C_SERVICE_H_

#include "I2C_Parameter.h"


I2C_StatusType I2C_UserReceive_MemRequest_Chnl1(uint16_t SlaveAddress, uint16_t MemAdrReq, uint8_t* pRxData, uint8_t Length);
I2C_StatusType I2C_UserReceive_MemRequest_Chnl2(uint16_t SlaveAddress, uint16_t MemAdrReq, uint8_t* pRxData, uint8_t Length);
I2C_StatusType I2C_UserReceive_MemRequest_Chnl3(uint16_t SlaveAddress, uint16_t MemAdrReq, uint8_t* pRxData, uint8_t Length);

I2C_StatusType I2C_UserReceive_Chnl1(uint16_t SlaveAddress, uint8_t* pRxData, uint8_t Length);
I2C_StatusType I2C_UserReceive_Chnl2(uint16_t SlaveAddress, uint8_t* pRxData, uint8_t Length);
I2C_StatusType I2C_UserReceive_Chnl3(uint16_t SlaveAddress, uint8_t* pRxData, uint8_t Length);

I2C_StatusType I2C_UserTransmit_MemRequest_Chnl1(uint16_t SlaveAddress, uint16_t MemAdrReq, uint8_t* pTxData, uint8_t Length);
I2C_StatusType I2C_UserTransmit_MemRequest_Chnl2(uint16_t SlaveAddress, uint16_t MemAdrReq, uint8_t* pTxData, uint8_t Length);
I2C_StatusType I2C_UserTransmit_MemRequest_Chnl3(uint16_t SlaveAddress, uint16_t MemAdrReq, uint8_t* pTxData, uint8_t Length);

I2C_StatusType I2C_UserTransmit_Chnl1(uint16_t SlaveAddress, uint8_t* pTxData, uint8_t Length);
I2C_StatusType I2C_UserTransmit_Chnl2(uint16_t SlaveAddress, uint8_t* pTxData, uint8_t Length);
I2C_StatusType I2C_UserTransmit_Chnl3(uint16_t SlaveAddress, uint8_t* pTxData, uint8_t Length);

I2C_StatusType I2C_User_IsReceiveCompleted_Chnl1(void);
I2C_StatusType I2C_User_IsReceiveCompleted_Chnl2(void);
I2C_StatusType I2C_User_IsReceiveCompleted_Chnl3(void);

#endif /* PLATFORM_I2C_I2C_SERVICE_H_ */
