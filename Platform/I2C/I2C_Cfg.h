/*
 * I2C_Cfg.h
 *
 *  Created on: Nov 22, 2016
 *      Author: Mr.A
 *  Interface to upper layer
 */

#ifndef I2C_CFG_H_
#define I2C_CFG_H_

#include "I2C_core.h"


/* Function Switch for I2C channel used*/

#define FS_I2C_CHANNEL_1_ENABLED							I2C_FS_ENABLED
#define I2C_SCL_PIN_CHNL_1									PORT_B_PIN_8
#define I2C_SDA_PIN_CHNL_1									PORT_B_PIN_9

#define FS_I2C_CHANNEL_2_ENABLED							I2C_FS_ENABLED
#define I2C_SCL_PIN_CHNL_2									PORT_B_PIN_10
#define I2C_SDA_PIN_CHNL_2									PORT_B_PIN_11

#define FS_I2C_CHANNEL_3_ENABLED							I2C_FS_ENABLED
#define I2C_SCL_PIN_CHNL_3									PORT_A_PIN_8
#define I2C_SDA_PIN_CHNL_3									PORT_C_PIN_9

/***************************************/

/* Threshold for I2C time out checking [cycle: 50 ms]*/
#define I2C_TO_THRESHOLD									5
/***************************************/

#if (FS_I2C_CHANNEL_1_ENABLED == I2C_FS_ENABLED)
extern	const I2C_ConfigType UserCfg_I2C_Channel_1;
#endif

#if (FS_I2C_CHANNEL_2_ENABLED == I2C_FS_ENABLED)
extern	const I2C_ConfigType UserCfg_I2C_Channel_2;
#endif

#if (FS_I2C_CHANNEL_3_ENABLED == I2C_FS_ENABLED)
extern	const I2C_ConfigType UserCfg_I2C_Channel_3;
#endif

#if (FS_I2C_CHANNEL_1_ENABLED == I2C_FS_ENABLED)
extern	I2C_HandlerType I2C_Handler_1;
#endif

#if (FS_I2C_CHANNEL_2_ENABLED == I2C_FS_ENABLED)
extern	I2C_HandlerType I2C_Handler_2;
#endif

#if (FS_I2C_CHANNEL_3_ENABLED == I2C_FS_ENABLED)
extern	I2C_HandlerType I2C_Handler_3;
#endif

#endif /* I2C_CFG_H_ */
