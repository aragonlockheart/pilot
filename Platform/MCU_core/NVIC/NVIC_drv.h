/**
  ******************************************************************************
  * @file    NVIC_drv.h
  * @author  Viet
  * @brief   This file is used for the NVIC setting and definition
  ******************************************************************************
  * @attention The function in this driver will affect the interrupts heavily
  ******************************************************************************
  */
#ifndef NVIC_DRV_H
#define NVIC_DRV_H

#include "stm32f4xx.h"
#include "MISC.h"
#include "stm32f4xx_Types.h"
#include "NVIC_para.h"

#ifdef __cplusplus
	extern "C" {
#endif

/**
 * @brief This function initializes the NVIC
 *
 * @param None
 *
 * @reval None
 */
void NVIC_drv_Init(void);



#ifdef __cplusplus
	}
#endif

#endif
