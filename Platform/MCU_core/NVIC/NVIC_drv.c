#include "NVIC_drv.h"


inline static void NVIC_Tim1(void)
{
	NVIC_InitTypeDef NVIC_p;
	NVIC_p.NVIC_IRQChannel = TIM1_UP_TIM10_IRQn;
	NVIC_p.NVIC_IRQChannelPreemptionPriority = TIM_1_Preemp;
	NVIC_p.NVIC_IRQChannelSubPriority = TIM_1_SubPri;
	NVIC_p.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_p);
	return;
}


inline static void NVIC_Tim8(void)
{
	NVIC_InitTypeDef NVIC_p;
	NVIC_p.NVIC_IRQChannel = TIM8_UP_TIM13_IRQn;
	NVIC_p.NVIC_IRQChannelPreemptionPriority = TIM_8_Preemp;
	NVIC_p.NVIC_IRQChannelSubPriority = TIM_8_SubPri;
	NVIC_p.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_p);
	return;
}

inline static void NVIC_Tim2(void)
{
	NVIC_InitTypeDef NVIC_p;
	NVIC_p.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_p.NVIC_IRQChannelPreemptionPriority = TIM_2_Preemp;
	NVIC_p.NVIC_IRQChannelSubPriority = TIM_2_SubPri;
	NVIC_p.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_p);
	return;
}

inline static void NVIC_Tim5(void)
{
	NVIC_InitTypeDef NVIC_p;
	NVIC_p.NVIC_IRQChannel = TIM5_IRQn;
	NVIC_p.NVIC_IRQChannelPreemptionPriority = TIM_5_Preemp;
	NVIC_p.NVIC_IRQChannelSubPriority = TIM_5_SubPri;
	NVIC_p.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_p);
	return;
}

inline static void NVIC_Tim3(void)
{
	NVIC_InitTypeDef NVIC_p;
	NVIC_p.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_p.NVIC_IRQChannelPreemptionPriority = TIM_3_Preemp;
	NVIC_p.NVIC_IRQChannelSubPriority = TIM_3_SubPri;
	NVIC_p.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_p);
	return;
}

inline static void NVIC_Tim4(void)
{
	NVIC_InitTypeDef NVIC_p;
	NVIC_p.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_p.NVIC_IRQChannelPreemptionPriority = TIM_4_Preemp;
	NVIC_p.NVIC_IRQChannelSubPriority = TIM_4_SubPri;
	NVIC_p.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_p);
	return;
}

void NVIC_drv_Init(void)
{
	NVIC_Tim1();
	NVIC_Tim8();
	NVIC_Tim2();
	NVIC_Tim5();
	NVIC_Tim3();
	NVIC_Tim4();
	return;
}
