#include "IRQs.h"
/**
  ******************************************************************************
  * @file    IRQs.c
  * @author  Viet
  * @brief   This C file contains the declarations of IRQs
  ******************************************************************************
  * @attention Please remember to configure NVIC before using
  ******************************************************************************
  */

void  TIM1_UP_TIM10_IRQHandler(void)
{
	if(BitIsSet(TIM1->SR,ZERO_16))
	{
		//Test_Timer++;
		TIM1->SR &= ~(ONE_16);
	}
		return;
}


void TIM8_UP_TIM13_IRQHandler(void)
{
	if(BitIsSet(TIM8->SR,ZERO_16))
	{
		//Test_Timer++;
		TIM8->SR &= ~(ONE_16);
	}
	return;
}

void TIM2_IRQHandler(void)
{
	if(BitIsSet(TIM2->SR,ZERO_16))
	{
		//Test_Timer++;
		TIM2->SR &= ~(ONE_16);
	}
	return;
}


void TIM5_IRQHandler(void)
{
	if(BitIsSet(TIM5->SR,ZERO_16))
	{
		//Test_Timer++;
		TIM5->SR &= ~(ONE_16);
	}
	return;
}

void TIM3_IRQHandler(void)
{
	if(BitIsSet(TIM3->SR,ZERO_16))
	{
		//Test_Timer++;
		TIM3->SR &= ~(ONE_16);
	}
	return;
}

void TIM4_IRQHandler(void)
{
	if(BitIsSet(TIM4->SR,ZERO_16))
	{
		Test_Timer++;
		TIM4->SR &= ~(ONE_16);
	}
	return;
}
