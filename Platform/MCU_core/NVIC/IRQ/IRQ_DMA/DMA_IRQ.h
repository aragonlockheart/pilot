/*!\file DMA_IRQ.h
*  \date Dec 3, 2015
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef DMA_IRQ_H_
#define DMA_IRQ_H_

#include "MCU_Cfg.h"
#include "MCU_Reg.h"
#include "MCU_Types.h"

typedef enum
{
	EN_DMA1_Stream0, /*!00*/
	EN_DMA1_Stream1, /*!01*/
	EN_DMA1_Stream2, /*!02*/
	EN_DMA1_Stream3, /*!03*/
	EN_DMA1_Stream4, /*!04*/
	EN_DMA1_Stream5, /*!05*/
	EN_DMA1_Stream6, /*!06*/
	EN_DMA1_Stream7, /*!07*/
	EN_DMA2_Stream0, /*!08*/
	EN_DMA2_Stream1, /*!09*/
	EN_DMA2_Stream2, /*!10*/
	EN_DMA2_Stream3, /*!11*/
	EN_DMA2_Stream4, /*!12*/
	EN_DMA2_Stream5, /*!13*/
	EN_DMA2_Stream6, /*!14*/
	EN_DMA2_Stream7, /*!15*/
	EN_DMA_StreamMax /*!16*/
}DMA_IRQ_Type;

extern RegType	Var_DMA_GlobalFlag;

void DMA1_Stream0_IRQHandler(void);
void DMA1_Stream1_IRQHandler(void);
void DMA1_Stream2_IRQHandler(void);
void DMA1_Stream3_IRQHandler(void);
void DMA1_Stream4_IRQHandler(void);
void DMA1_Stream5_IRQHandler(void);
void DMA1_Stream6_IRQHandler(void);
void DMA1_Stream7_IRQHandler(void);

void DMA2_Stream0_IRQHandler(void);
void DMA2_Stream1_IRQHandler(void);
void DMA2_Stream2_IRQHandler(void);
void DMA2_Stream3_IRQHandler(void);
void DMA2_Stream4_IRQHandler(void);
void DMA2_Stream5_IRQHandler(void);
void DMA2_Stream6_IRQHandler(void);
void DMA2_Stream7_IRQHandler(void);

BOOL DMA_SetGlobalIRQFlag(DMA_IRQ_Type par_Position);
#ifdef __cplusplus
	extern "C" {
#endif
BOOL DMA_ResetGlobalIRQFlag(DMA_IRQ_Type par_Position);
BOOL DMA_IsGlobalIRQFlagSet(DMA_IRQ_Type par_Position);

#ifdef __cplusplus
	}
#endif
#endif /* DMA_IRQ_H_ */
