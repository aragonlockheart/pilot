/*!
 * @file DMA_IRQ.c
 * @date Apr 9, 2016
 * @author Constantine
 */
#include "DMA_IRQ.h"
#include "Cfg_Core_DMA.h"

RegType Var_DMA_GlobalFlag;

void DMA1_Stream0_IRQHandler(void)
{
	DMA_SetGlobalIRQFlag(EN_DMA1_Stream0);
}

void DMA1_Stream1_IRQHandler(void)
{
	DMA_SetGlobalIRQFlag(EN_DMA1_Stream1);
}
void DMA1_Stream2_IRQHandler(void)
{
	DMA_SetGlobalIRQFlag(EN_DMA1_Stream2);
}
void DMA1_Stream3_IRQHandler(void)
{
	DMA_SetGlobalIRQFlag(EN_DMA1_Stream3);
}
void DMA1_Stream4_IRQHandler(void)
{
	DMA_SetGlobalIRQFlag(EN_DMA1_Stream4);
}

void DMA1_Stream5_IRQHandler(void)
{
	DMA_SetGlobalIRQFlag(EN_DMA1_Stream5);
}

void DMA1_Stream6_IRQHandler(void)
{
	DMA_SetGlobalIRQFlag(EN_DMA1_Stream6);
}

void DMA1_Stream7_IRQHandler(void)
{
	DMA_SetGlobalIRQFlag(EN_DMA1_Stream7);
}

void DMA2_Stream0_IRQHandler(void)
{
	DMA_SetGlobalIRQFlag(EN_DMA2_Stream0);
}

void DMA2_Stream1_IRQHandler(void)
{
	DMA_SetGlobalIRQFlag(EN_DMA2_Stream1);
}

void DMA2_Stream2_IRQHandler(void)
{
	DMA_SetGlobalIRQFlag(EN_DMA2_Stream2);
}

void DMA2_Stream3_IRQHandler(void)
{
	DMA_SetGlobalIRQFlag(EN_DMA2_Stream3);
}

void DMA2_Stream4_IRQHandler(void)
{
	DMA_SetGlobalIRQFlag(EN_DMA2_Stream4);
}

void DMA2_Stream5_IRQHandler(void)
{
	DMA_SetGlobalIRQFlag(EN_DMA2_Stream5);
}

void DMA2_Stream6_IRQHandler(void)
{
	DMA_SetGlobalIRQFlag(EN_DMA2_Stream6);
}

void DMA2_Stream7_IRQHandler(void)
{
	DMA_SetGlobalIRQFlag(EN_DMA2_Stream7);
}


BOOL DMA_SetGlobalIRQFlag(DMA_IRQ_Type par_Position)
{
	/* Set the flag bit at requested position */
	Var_DMA_GlobalFlag |= (ONE_32 << par_Position);

	/* Return ENABLE as the above operation is always successful */
	return EM_ENABLE;
}

BOOL DMA_ResetGlobalIRQFlag(DMA_IRQ_Type par_Position)
{
	/* Reset the flag bit at requested position */
	Var_DMA_GlobalFlag &= ~(ONE_32 << par_Position);

	/* Return ENABLE as the above operation is always successful */
	return EM_ENABLE;
}

BOOL DMA_IsGlobalIRQFlagSet(DMA_IRQ_Type par_Position)
{
	/* Initialize return value as disable */
	BOOL ret_val = EM_DISABLE;

	/* If the flag bit at requested position is set? */
	if(Var_DMA_GlobalFlag & (ONE_32 << par_Position))
	{
		/* Set return value to ENABLE */
		ret_val = EM_ENABLE;
	}
	else
	{/* Flag bit is not set, do nothing */}

	return ret_val;
}
