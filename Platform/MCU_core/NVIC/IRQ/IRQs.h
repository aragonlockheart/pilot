/**
  ******************************************************************************
  * @file    IRQs.h
  * @author  Viet
  * @brief   This file contains IRQs, name of defined IRQs can be found in startup_stm32f4xx.c
  ******************************************************************************
  * @attention
  ******************************************************************************
  */

#ifndef IRQ_H
#define IRQ_H


#ifdef __cplusplus
 extern "C" {
#endif

#include "stm32f4xx.h"
#include "stm32f4xx_Types.h"
#include "common_para.h"


extern uint32 Test_Timer;


/**
 * @brief This is the handler for timer1 and timer 10 update
 *
 * @param None
 *
 * @reval None
 */
void TIM1_UP_TIM10_IRQHandler(void);

void TIM8_UP_TIM13_IRQHandler(void);

void TIM2_IRQHandler(void);


#ifdef __cplusplus
 }
#endif


#endif
