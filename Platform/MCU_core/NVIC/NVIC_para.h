#ifndef NVIC_PARA_H
#define NVIC_PARA_H

#include "stm32f4xx.h"
#include "stm32f4xx_Types.h"

#ifdef __cplusplus
	extern "C" {
#endif

/* Timers 1&8 */
#define TIM_1_Preemp	(uint8) (0u)
#define TIM_8_Preemp	(uint8) (0u)

/* Timers 2&5 */
#define TIM_2_Preemp	(uint8) (1u)
#define TIM_5_Preemp	(uint8) (1u)

/* Timers 3&4*/
#define TIM_3_Preemp	(uint8) (2u)
#define TIM_4_Preemp	(uint8) (2u)

/* Timers 6&7 */
#define TIM_6_Preemp	(uint8) (3u)
#define TIM_7_Preemp	(uint8) (3u)

/* Timer 9 to 14 */
#define TIM_9_Preemp	(uint8) (4u)
#define TIM_10_Preemp	(uint8) (4u)
#define TIM_11_Preemp	(uint8) (4u)
#define TIM_12_Preemp	(uint8) (4u)
#define TIM_13_Preemp	(uint8) (4u)
#define TIM_14_Preemp	(uint8) (4u)




/* Timers 1&8 */
#define TIM_1_SubPri	(uint8) (0u)
#define TIM_8_SubPri	(uint8) (1u)

/* Timers 2&5 */
#define TIM_2_SubPri	(uint8) (0u)
#define TIM_5_SubPri	(uint8) (1u)

/* Timers 3&4*/
#define TIM_3_SubPri	(uint8) (0u)
#define TIM_4_SubPri	(uint8) (1u)

/* Timers 6&7 */
#define TIM_6_SubPri	(uint8) (0u)
#define TIM_7_SubPri	(uint8) (1u)

/* Timer 9 to 14 */
#define TIM_9_SubPri	(uint8) (0u)
#define TIM_10_SubPri	(uint8) (1u)
#define TIM_11_SubPri	(uint8) (2u)
#define TIM_12_SubPri	(uint8) (3u)
#define TIM_13_SubPri	(uint8) (4u)
#define TIM_14_SubPri	(uint8) (5u)

#ifdef __cplusplus
	}
#endif

#endif
