/*
 * MCU_Reg_Timer2_5.h
 *
 *  Created on: Apr 23, 2017
 *      Author: USER
 */

#ifndef PLATFORM_MCU_CORE_MCU_REG_MCU_REG_TIMER2_5_H_
#define PLATFORM_MCU_CORE_MCU_REG_MCU_REG_TIMER2_5_H_

#ifdef __cplusplus
	extern "C" {
#endif





/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* _______________________________________________________________________________________________________________________________ */
/*/                                                                                                                               \*/
/*|                                                      BIT FIELD DEFINITION                                                     |*/
/*\_______________________________________________________________________________________________________________________________/*/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/




	/* Define the bit field types for timer's register */
typedef struct
{
	volatile uint32 CEN			:1;	 /* Bit 0 */
	volatile uint32 UDIS		:1;  /* Bit 1 */
	volatile uint32 URS			:1;  /* Bit 2 */
	volatile uint32 OPM			:1;  /* Bit 3 */
	volatile uint32 DIR 		:1;  /* Bit 4 */
	volatile uint32 CMS			:2;  /* Bit 5-6 */
	volatile uint32 ARPE		:1;  /* Bit 7 */
	volatile uint32 CKD			:2;  /* Bit 8-9 */
	volatile uint32 Rsvd2		:22; /* Bit 10-31 */
}MCU_Reg_Time2_5_CR1_Bitfield_Type;

typedef struct
{
	volatile uint32 Rsvd1		:3; /* Bit 0 to 2 */
	volatile uint32 CCDS		:1; /* Bit 3 */
	volatile uint32 MMS			:3; /* Bit 4 to 6 */
	volatile uint32 TI1S		:1; /* Bit 7 */
	volatile uint32 Rsvd2		:24;/* Bit 8 to 31 */
}MCU_Reg_Time2_5_CR2_Bitfield_Type;


typedef struct
{
	volatile uint32 SMS			:3; /*Bit 0-2 */
	volatile uint32 Rsvd1		:1; /*Bit 3 */
	volatile uint32 TS			:3; /*Bit 4-6 */
	volatile uint32 MSM			:1; /*Bit 7 */
	volatile uint32 ETF			:4; /*Bit 8-11 */
	volatile uint32 ETPS		:2; /*Bit 12-13 */
	volatile uint32 ECE			:1; /*Bit 14 */
	volatile uint32 ETP			:1; /*Bit 15 */
	volatile uint32 Rsvd2		:16;/*Bit 16 to 31*/
}MCU_Reg_Time2_5_SMCR_Bitfield_Type;

typedef struct
{
	volatile uint32 UIE			:1; /*Bit 0 */
	volatile uint32 CC1IE		:1; /*Bit 1 */
	volatile uint32 CC2IE		:1; /*Bit 2 */
	volatile uint32 CC3IE		:1; /*Bit 3 */
	volatile uint32 CC4IE		:1; /*Bit 4 */
	volatile uint32 Rsvd1		:1; /*Bit 5 */
	volatile uint32 TIE			:1; /*Bit 6 */
	volatile uint32 Rsvd2		:1; /*Bit 7 */
	volatile uint32 UDE			:1; /*Bit 8 */
	volatile uint32 CC1DE		:1; /*Bit 9 */
	volatile uint32 CC2DE		:1; /*Bit 10 */
	volatile uint32 CC3DE		:1; /*Bit 11 */
	volatile uint32 CC4DE		:1; /*Bit 12 */
	volatile uint32 COMDE		:1; /*Bit 13 */
	volatile uint32 TDE			:1; /*Bit 14 */
	volatile uint32 Rsvd3		:17;/*Bit 15 to 31 */
}MCU_Reg_Time2_5_DIER_Bitfield_Type;

typedef struct
{
	volatile uint32 UIF			:1; /*Bit 0 */
	volatile uint32 CC1IF		:1; /*Bit 1 */
	volatile uint32 CC2IF		:1; /*Bit 2 */
	volatile uint32 CC3IF		:1; /*Bit 3 */
	volatile uint32 CC4IF		:1; /*Bit 4 */
	volatile uint32 Rsvd1		:1; /*Bit 5 */
	volatile uint32 TIF			:1; /*Bit 6 */
	volatile uint32 Rsvd2		:2; /*Bit 7-8 */
	volatile uint32 CC1OF		:1; /*Bit 9 */
	volatile uint32 CC2OF		:1; /*Bit 10 */
	volatile uint32 CC3OF		:1; /*Bit 11 */
	volatile uint32 CC4OF		:1; /*Bit 12 */
	volatile uint32 Rsvd3		:19;/*Bit 13 to 31*/
}MCU_Reg_Time2_5_SR_Bitfield_Type;


typedef struct
{
	volatile uint32 UG			:1; /*Bit 0 */
	volatile uint32 CC1G		:1; /*Bit 1 */
	volatile uint32 CC2G		:1; /*Bit 2 */
	volatile uint32 CC3G		:1; /*Bit 3 */
	volatile uint32 CC4G		:1; /*Bit 4 */
	volatile uint32 Rsvd1		:1; /*Bit 5 */
	volatile uint32 TG			:1; /*Bit 6 */
	volatile uint32 Rsvd2		:25;/*Bit 7 to 31*/
}MCU_Reg_Time2_5_EGR_Bitfield_Type;

typedef struct
{
	volatile uint32 CNT			:32; /*Bit 0 to 31 */
}MCU_Reg_Time2_5_CNT_Bitfield_Type;

typedef struct
{
	volatile uint32 PSC			:16; /*Bit 0 to 15 */
	volatile uint32 Rsvd		:16; /*Bit 16 to 31 */
}MCU_Reg_Time2_5_PSC_Bitfield_Type;

typedef struct
{
	volatile uint32 ARR			:32; /*Bit 0 to 31 */
}MCU_Reg_Time2_5_ARR_Bitfield_Type;

typedef struct
{
	volatile uint32 CC1S		:2; /*Bit 0 to 1 */
	volatile uint32 OC1FE		:1; /*Bit 2 */
	volatile uint32 OC1PE		:1; /*Bit 3 */
	volatile uint32 OC1M		:3; /*Bit 4 to 6 */
	volatile uint32 OC1CE		:1; /*Bit 7 */
	volatile uint32 CC2S		:2; /*Bit 8 to 9 */
	volatile uint32 OC2FE		:1; /*Bit 10 */
	volatile uint32 OC2PE		:1; /*Bit 11 */
	volatile uint32 OC2M		:3; /*Bit 12 to 14 */
	volatile uint32 OC2CE		:1; /*Bit 15 */
	volatile uint32 Rsvd		:16; /*Bit 16 to 31 */
}MCU_Reg_Time2_5_CCMR1_OC_Bitfield_Type;


typedef struct
{
	volatile uint32 CC1S		:2; /*Bit 0 to 1 */
	volatile uint32 IC1PSC		:2; /*Bit 2-3 */
	volatile uint32 IC1F		:4; /*Bit 4 to 7 */
	volatile uint32 CC2S		:2; /*Bit 8 to 9 */
	volatile uint32 IC2PSC		:2; /*Bit 10-11 */
	volatile uint32 IC2F		:4; /*Bit 12-15 */
	volatile uint32 Rsvd		:16; /*Bit 16 to 31 */
}MCU_Reg_Time2_5_CCMR1_IC_Bitfield_Type;

typedef struct
{
	volatile uint32 CC3S		:2; /*Bit 0 to 1 */
	volatile uint32 OC3FE		:1; /*Bit 2 */
	volatile uint32 OC3PE		:1; /*Bit 3 */
	volatile uint32 OC3M		:3; /*Bit 4 to 6 */
	volatile uint32 OC3CE		:1; /*Bit 7 */
	volatile uint32 CC4S		:2; /*Bit 8 to 9 */
	volatile uint32 OC4FE		:1; /*Bit 10 */
	volatile uint32 OC4PE		:1; /*Bit 11 */
	volatile uint32 OC4M		:3; /*Bit 12 to 14 */
	volatile uint32 OC4CE		:1; /*Bit 15 */
	volatile uint32 Rsvd		:16; /*Bit 16 to 31 */
}MCU_Reg_Time2_5_CCMR2_OC_Bitfield_Type;


typedef struct
{
	volatile uint32 CC3S		:2; /*Bit 0 to 1 */
	volatile uint32 IC3PSC		:2; /*Bit 2-3 */
	volatile uint32 IC3F		:4; /*Bit 4 to 7 */
	volatile uint32 CC4S		:2; /*Bit 8 to 9 */
	volatile uint32 IC4PSC		:2; /*Bit 10-11 */
	volatile uint32 IC4F		:4; /*Bit 12-15 */
	volatile uint32 Rsvd		:16; /*Bit 16 to 31 */
}MCU_Reg_Time2_5_CCMR2_IC_Bitfield_Type;

typedef struct
{
	volatile uint32 CC1E		:1; /*Bit 0 */
	volatile uint32 CC1P		:1; /*Bit 1 */
	volatile uint32 Rsvd1		:1; /*Bit 2 */
	volatile uint32 CC1NP		:1; /*Bit 3 */
	volatile uint32 CC2E		:1; /*Bit 4 */
	volatile uint32 CC2P		:1; /*Bit 5 */
	volatile uint32 Rsvd2		:1; /*Bit 6 */
	volatile uint32 CC2NP		:1; /*Bit 7 */
	volatile uint32 CC3E		:1; /*Bit 8 */
	volatile uint32 CC3P		:1; /*Bit 9 */
	volatile uint32 Rsvd3		:1; /*Bit 10 */
	volatile uint32 CC3NP		:1; /*Bit 11 */
	volatile uint32 CC4E		:1; /*Bit 12 */
	volatile uint32 CC4P		:1; /*Bit 13 */
	volatile uint32 Rsvd4		:1; /*Bit 14 */
	volatile uint32 CC4NP		:1; /*Bit 15 */
	volatile uint32 Rsvd5		:16; /*Bit 16 to 31 */
}MCU_Reg_Time2_5_CCER_Bitfield_Type;

typedef struct
{
	volatile uint32 CCR1		:32; /*Bit 0 to 31 */
}MCU_Reg_Time2_5_CCR1_Bitfield_Type;

typedef struct
{
	volatile uint32 CCR2		:32; /*Bit 0 to 31 */
}MCU_Reg_Time2_5_CCR2_Bitfield_Type;

typedef struct
{
	volatile uint32 CCR3		:32; /*Bit 0 to 31 */
}MCU_Reg_Time2_5_CCR3_Bitfield_Type;

typedef struct
{
	volatile uint32 CCR4		:32; /*Bit 0 to 31 */
}MCU_Reg_Time2_5_CCR4_Bitfield_Type;

typedef struct
{
	volatile uint32 DBA			:5; /*Bit 0-4 */
	volatile uint32 Rsvd 		:3; /*Bit 5-7 */
	volatile uint32 DBL			:5; /*Bit 8-12*/
	volatile uint32 Rsvd2     	:19;/*Bit 13-31 */
}MCU_Reg_Time2_5_DCR_Bitfield_Type;

typedef struct
{
	volatile uint32 DMAB		:16;/*Bit 0-15 */
	volatile uint32 Rsvd		:16;/*Bit 16-31 */
}MCU_Reg_Time2_5_DMAR_Bitfield_Type;

typedef struct
{
	volatile uint32  Rsvd1		:10; /*Bit 0-9 */
	volatile uint32  ITR1_RMP	:2;  /*Bit 10-11 */
	volatile uint32  Rsvd2  	:20; /*Bit 12-31 */
}MCU_Reg_Time2_5_TIM2_OR_Bitfield_Type;

typedef struct
{
	volatile uint32  Rsvd1		:6; /*Bit 0-5 */
	volatile uint32  ITR4_RMP	:2;  /*Bit 6-7 */
	volatile uint32  Rsvd2  	:24; /*Bit 8-31 */
}MCU_Reg_Time2_5_TIM5_OR_Bitfield_Type;

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                      TIMER individual Registers' structure                                               |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/



/* Define register's whole type */

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time2_5_CR1_Bitfield_Type 	Bitfield;
}MCU_Reg_Time2_5_CR1;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time2_5_CR2_Bitfield_Type 	Bitfield;
}MCU_Reg_Time2_5_CR2;

typedef union
{
	uint32								Whole;
	MCU_Reg_Time2_5_SMCR_Bitfield_Type  Bitfield;
}MCU_Reg_Time2_5_SMCR;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time2_5_DIER_Bitfield_Type 	Bitfield;
}MCU_Reg_Time2_5_DIER;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time2_5_SR_Bitfield_Type 	Bitfield;
}MCU_Reg_Time2_5_SR;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time2_5_EGR_Bitfield_Type 	Bitfield;
}MCU_Reg_Time2_5_EGR;

typedef union
{
	uint32 									Whole;
	MCU_Reg_Time2_5_CCMR1_OC_Bitfield_Type 	Bitfield_OC;
	MCU_Reg_Time2_5_CCMR1_IC_Bitfield_Type 	Bitfield_IC;
}MCU_Reg_Time2_5_CCMR1;

typedef union
{
	uint32 									Whole;
	MCU_Reg_Time2_5_CCMR2_OC_Bitfield_Type 	Bitfield_OC;
	MCU_Reg_Time2_5_CCMR2_IC_Bitfield_Type 	Bitfield_IC;
}MCU_Reg_Time2_5_CCMR2;

typedef union
{
	uint32 									Whole;
	MCU_Reg_Time2_5_CCER_Bitfield_Type 		Bitfield;
}MCU_Reg_Time2_5_CCER;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time2_5_CNT_Bitfield_Type 	Bitfield;
}MCU_Reg_Time2_5_CNT;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time2_5_PSC_Bitfield_Type 	Bitfield;
}MCU_Reg_Time2_5_PSC;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time2_5_ARR_Bitfield_Type 	Bitfield;
}MCU_Reg_Time2_5_ARR;


typedef union
{
	uint32 								Whole;
	MCU_Reg_Time2_5_CCR1_Bitfield_Type 	Bitfield;
}MCU_Reg_Time2_5_CCR1;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time2_5_CCR2_Bitfield_Type 	Bitfield;
}MCU_Reg_Time2_5_CCR2;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time2_5_CCR3_Bitfield_Type 	Bitfield;
}MCU_Reg_Time2_5_CCR3;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time2_5_CCR4_Bitfield_Type 	Bitfield;
}MCU_Reg_Time2_5_CCR4;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time2_5_DCR_Bitfield_Type 	Bitfield;
}MCU_Reg_Time2_5_DCR;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time2_5_DMAR_Bitfield_Type 	Bitfield;
}MCU_Reg_Time2_5_DMAR;

typedef union
{
	uint32 									Whole;
	MCU_Reg_Time2_5_TIM2_OR_Bitfield_Type 	Bitfield;
}MCU_Reg_Time2_5_TIM2_OR;

typedef union
{
	uint32 									Whole;
	MCU_Reg_Time2_5_TIM5_OR_Bitfield_Type 	Bitfield;
}MCU_Reg_Time2_5_TIM5_OR;

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                     Timer Whole Control Structure                                                        |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*                                                                                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


typedef struct
{
	MCU_Reg_Time2_5_CR1			CR1				;
	MCU_Reg_Time2_5_CR2			CR2				;
	MCU_Reg_Time2_5_SMCR 		SMCR			;
	MCU_Reg_Time2_5_DIER		DIER			;
	MCU_Reg_Time2_5_SR			SR				;
	MCU_Reg_Time2_5_EGR			EGR				;
	MCU_Reg_Time2_5_CCMR1 		CCMR1			;
	MCU_Reg_Time2_5_CCMR2 		CCMR2			;
	MCU_Reg_Time2_5_CCER 		CCER			;
	MCU_Reg_Time2_5_CNT			CNT				;
	MCU_Reg_Time2_5_PSC			PSC				;
	MCU_Reg_Time2_5_ARR			ARR				;
	uint32 						RESERVED1:32	;
	MCU_Reg_Time2_5_CCR1		CCR1			;
	MCU_Reg_Time2_5_CCR2		CCR2			;
	MCU_Reg_Time2_5_CCR3		CCR3			;
	MCU_Reg_Time2_5_CCR4		CCR4			;
	uint32 						RESERVED2:32	;
	MCU_Reg_Time2_5_DCR			DCR				;
	MCU_Reg_Time2_5_DMAR		DMAR			;
	MCU_Reg_Time2_5_TIM2_OR		TIM2_OR			;
	MCU_Reg_Time2_5_TIM5_OR		TIM5_OR			;

}MCU_Reg_Timer2_5_RegSet;


#ifdef __cplusplus
	}
#endif
#endif /* PLATFORM_MCU_CORE_MCU_REG_MCU_REG_TIMER2_5_H_ */
