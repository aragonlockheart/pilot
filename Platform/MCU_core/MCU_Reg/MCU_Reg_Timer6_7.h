/*!\file MCU_Reg_Timer6_7.h
*  \date Dec 25, 2016
*  \author Nguyen Trong Viet
*  \brief This file defines the bitfield structure for timer 1 AND 6 with bit width of 16 bit
*/

#ifndef MCU_REG_TIMER6_7_H_
#define MCU_REG_TIMER6_7_H_



#ifdef __cplusplus
	extern "C" {
#endif






/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* _______________________________________________________________________________________________________________________________ */
/*/                                                                                                                               \*/
/*|                                                      BIT FIELD DEFINITION                                                     |*/
/*\_______________________________________________________________________________________________________________________________/*/
/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/




	/* Define the bit field types for timer's register */
typedef struct
{
	volatile uint32 CEN			:1;	 /* Bit 0 */
	volatile uint32 UDIS		:1;  /* Bit 1 */
	volatile uint32 URS			:1;  /* Bit 2 */
	volatile uint32 OPM			:1;  /* Bit 3 */
	volatile uint32 Rsvd1		:3;  /* Bit 4-6 */
	volatile uint32 ARPE		:1;  /* Bit 7 */
	volatile uint32 Rsvd2		:24; /* Bit 8-31 */

}MCU_Reg_Time6_7_CR1_Bitfield_Type;

typedef struct
{
	volatile uint32 Rsvd1		:4; /* Bit 0 to 3 */
	volatile uint32 MMS			:3; /* Bit 4 to 6 */
	volatile uint32 Rsvd2		:25;/* Bit 7 to 31 */
}MCU_Reg_Time6_7_CR2_Bitfield_Type;


typedef struct
{
	volatile uint32 UIE			:1; /*Bit 0 */
	volatile uint32 Rsvd1		:7; /*Bit 1 to 7 */
	volatile uint32 UDE			:1; /*Bit 8 */
	volatile uint32 Rsvd2		:23;/*Bit 9 to 31 */
}MCU_Reg_Time6_7_DIER_Bitfield_Type;

typedef struct
{
	volatile uint32 UIF			:1; /*Bit 0 */
	volatile uint32 Rsvd1		:31;/*Bit 1 to 31*/
}MCU_Reg_Time6_7_SR_Bitfield_Type;

typedef struct
{
	volatile uint32 UG			:1; /*Bit 0 */
	volatile uint32 Rsvd1		:31;/*Bit 1 to 31*/
}MCU_Reg_Time6_7_EGR_Bitfield_Type;

typedef struct
{
	volatile uint16 CNT;
	volatile uint16 Rsvd;
}MCU_Reg_Time6_7_CNT_Bitfield_Type;

typedef struct
{
	volatile uint16 PSC;
	volatile uint16 Rsvd;
}MCU_Reg_Time6_7_PSC_Bitfield_Type;

typedef struct
{
	volatile uint16 ARR;
	volatile uint16 Rsvd;
}MCU_Reg_Time6_7_ARR_Bitfield_Type;
/* end definition of bit fields */







/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                      TIMER individual Registers' structure                                               |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/



/* Define register's whole type */

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time6_7_CR1_Bitfield_Type 	Bitfield;
}MCU_Reg_Time6_7_CR1;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time6_7_CR2_Bitfield_Type 	Bitfield;
}MCU_Reg_Time6_7_CR2;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time6_7_DIER_Bitfield_Type 	Bitfield;
}MCU_Reg_Time6_7_DIER;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time6_7_SR_Bitfield_Type 	Bitfield;
}MCU_Reg_Time6_7_SR;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time6_7_EGR_Bitfield_Type 	Bitfield;
}MCU_Reg_Time6_7_EGR;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time6_7_CNT_Bitfield_Type 	Bitfield;
}MCU_Reg_Time6_7_CNT;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time6_7_PSC_Bitfield_Type 	Bitfield;
}MCU_Reg_Time6_7_PSC;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time6_7_ARR_Bitfield_Type 	Bitfield;
}MCU_Reg_Time6_7_ARR;






/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                     Timer Whole Control Structure                                                        |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*                                                                                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/


typedef struct
{
	MCU_Reg_Time6_7_CR1		CR1				;
	MCU_Reg_Time6_7_CR2		CR2				;
	uint32 	   				PRESERVED1:32	;
	MCU_Reg_Time6_7_DIER	DIER			;
	MCU_Reg_Time6_7_SR		SR				;
	MCU_Reg_Time6_7_EGR		EGR				;
	uint32 					PRESERVED2:32	;
	uint32 					PRESERVED3:32	;
	uint32 					PRESERVED4:32	;
	MCU_Reg_Time6_7_CNT		CNT				;
	MCU_Reg_Time6_7_PSC		PSC				;
	MCU_Reg_Time6_7_ARR		ARR				;

}MCU_Reg_Timer6_7_RegSet;




#ifdef __cplusplus
	}
#endif
#endif /* MCU_REG_TIMER6_7_H_ */
