/*!\file MCU_Reg_DMA.h
*  \date Oct 24, 2015
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef MCU_REG_DMA_H_
#define MCU_REG_DMA_H_

#include "stm32f4xx.h"
#include "stm32f4xx_Types.h"
#include "stm32f4xx_RegTypes.h"

typedef struct
{
	RegIO RegType FEIF0		: 	1; /*! Bit 0 */
	RegIO RegType BIT1		:	1; /*! Bit 1 */
	RegIO RegType DMEIF0	:	1; /*! Bit 2 */
	RegIO RegType TEIF0		:	1; /*! Bit 3 */
	RegIO RegType HTIF0		:	1; /*! Bit 4 */
	RegIO RegType TCIF0		:	1; /*! Bit 5 */
	RegIO RegType FEIF1		:	1; /*! Bit 6 */
	RegIO RegType BIT7		:	1; /*! Bit 7 */
	RegIO RegType DMEIF1	:	1; /*! Bit 8 */
	RegIO RegType TEIF1		:	1; /*! Bit 9 */
	RegIO RegType HTIF1		:	1; /*! Bit 10 */
	RegIO RegType TCIF1		:	1; /*! Bit 11 */
	RegIO RegType BIT12		:	4; /*! Bit 12-15 */
	RegIO RegType FEIF2		:	1; /*! Bit 16 */
	RegIO RegType BIT17		:	1; /*! Bit 17 */
	RegIO RegType DMEIF2	: 	1; /*! Bit 18 */
	RegIO RegType TEIF2		:	1; /*! Bit 19 */
	RegIO RegType HTIF2		:	1; /*! Bit 20 */
	RegIO RegType TCIF2		:	1; /*! Bit 21 */
	RegIO RegType FEIF3		:	1; /*! Bit 22 */
	RegIO RegType BIT23		:	1; /*! Bit 23 */
	RegIO RegType DMEIF3	:	1; /*! Bit 24 */
	RegIO RegType TEIF3		:	1; /*! Bit 25 */
	RegIO RegType HTIF3		:	1; /*! Bit 27 */
	RegIO RegType TCIF3		:	1; /*! Bit 28 */
	RegIO RegType BIT28		:	4; /*! Bit 28-31 */

}Reg_DMA_Flag_Type_LISR;


typedef struct
{
	RegIO RegType FEIF4		: 	1; /*! Bit 0 */
	RegIO RegType BIT1		:	1; /*! Bit 1 */
	RegIO RegType DMEIF4	:	1; /*! Bit 2 */
	RegIO RegType TEIF4		:	1; /*! Bit 3 */
	RegIO RegType HTIF4		:	1; /*! Bit 4 */
	RegIO RegType TCIF4		:	1; /*! Bit 5 */
	RegIO RegType FEIF5		:	1; /*! Bit 6 */
	RegIO RegType BIT7		:	1; /*! Bit 7 */
	RegIO RegType DMEIF5	:	1; /*! Bit 8 */
	RegIO RegType TEIF5		:	1; /*! Bit 9 */
	RegIO RegType HTIF5		:	1; /*! Bit 10 */
	RegIO RegType TCIF5		:	1; /*! Bit 11 */
	RegIO RegType BIT12 	:	4; /*! Bit 12-15 */
	RegIO RegType FEIF6		:	1; /*! Bit 16 */
	RegIO RegType BIT17 	:	1; /*! Bit 17 */
	RegIO RegType DMEIF6	: 	1; /*! Bit 18 */
	RegIO RegType TEIF6		:	1; /*! Bit 19 */
	RegIO RegType HTIF6		:	1; /*! Bit 20 */
	RegIO RegType TCIF6		:	1; /*! Bit 21 */
	RegIO RegType FEIF7		:	1; /*! Bit 22 */
	RegIO RegType BIT23 	:	1; /*! Bit 23 */
	RegIO RegType DMEIF7	:	1; /*! Bit 24 */
	RegIO RegType TEIF7		:	1; /*! Bit 25 */
	RegIO RegType HTIF7		:	1; /*! Bit 27 */
	RegIO RegType TCIF7		:	1; /*! Bit 28 */
	RegIO RegType BIT28 	:	4; /*! Bit 28-31 */

}Reg_DMA_Flag_Type_HISR;

typedef struct
{
	RegIO RegType CFEIF0		:1; /*! Bit 0 */
	RegIO RegType BIT1  		:1; /*! Bit 1 */
	RegIO RegType CDMEIF0		:1; /*! Bit 2 */
	RegIO RegType CTEIF0		:1; /*! Bit 3 */
	RegIO RegType CHTIF0		:1; /*! Bit 4 */
	RegIO RegType CTCIF0		:1; /*! Bit 5 */
	RegIO RegType CFEIF1		:1; /*! Bit 6 */
	RegIO RegType BIT7			:1; /*! Bit 7 */
	RegIO RegType CDMEIF1		:1; /*! Bit 8 */
	RegIO RegType CTEIF1		:1; /*! Bit 9 */
	RegIO RegType CHTIF1		:1; /*! Bit 10 */
	RegIO RegType CTCIF1		:1; /*! Bit 11 */
	RegIO RegType BIT12			:4; /*! Bit 12-15 */
	RegIO RegType CFEIF2		:1; /*! Bit 16 */
	RegIO RegType BIT17 		:1; /*! Bit 17 */
	RegIO RegType CDMEIF2		:1; /*! Bit 18 */
	RegIO RegType CTEIF2		:1; /*! Bit 19 */
	RegIO RegType CHTIF2		:1; /*! Bit 20 */
	RegIO RegType CTCIF2		:1; /*! Bit 21 */
	RegIO RegType CFEIF3		:1; /*! Bit 22 */
	RegIO RegType BIT23			:1; /*! Bit 23 */
	RegIO RegType CDMEIF3		:1; /*! Bit 24 */
	RegIO RegType CTEIF3		:1; /*! Bit 25 */
	RegIO RegType CHTIF3		:1; /*! Bit 26 */
	RegIO RegType CTCIF3		:1; /*! Bit 27 */
	RegIO RegType BIT28 		:4; /*! Bit 28-31 */


}Reg_DMA_Flag_Type_LIFCR;

typedef struct
{
	RegIO RegType CFEIF4		:1; /*! Bit 0 */
	RegIO RegType BIT1			:1; /*! Bit 1 */
	RegIO RegType CDMEIF4		:1; /*! Bit 2 */
	RegIO RegType CTEIF4		:1; /*! Bit 3 */
	RegIO RegType CHTIF4		:1; /*! Bit 4 */
	RegIO RegType CTCIF4		:1; /*! Bit 5 */
	RegIO RegType CFEIF5		:1; /*! Bit 6 */
	RegIO RegType BIT7			:1; /*! Bit 7 */
	RegIO RegType CDMEIF5		:1; /*! Bit 8 */
	RegIO RegType CTEIF5		:1; /*! Bit 9 */
	RegIO RegType CHTIF5		:1; /*! Bit 10 */
	RegIO RegType CTCIF5		:1; /*! Bit 11 */
	RegIO RegType BIT12 		:4; /*! Bit 12-15 */
	RegIO RegType CFEIF6		:1; /*! Bit 16 */
	RegIO RegType BIT17			:1; /*! Bit 17 */
	RegIO RegType CDMEIF6		:1; /*! Bit 18 */
	RegIO RegType CTEIF6		:1; /*! Bit 19 */
	RegIO RegType CHTIF6		:1; /*! Bit 20 */
	RegIO RegType CTCIF6		:1; /*! Bit 21 */
	RegIO RegType CFEIF7		:1; /*! Bit 22 */
	RegIO RegType BIT23			:1; /*! Bit 23 */
	RegIO RegType CDMEIF7		:1; /*! Bit 24 */
	RegIO RegType CTEIF7		:1; /*! Bit 25 */
	RegIO RegType CHTIF7		:1; /*! Bit 26 */
	RegIO RegType CTCIF7		:1; /*! Bit 27 */
	RegIO RegType BIT28			:4; /*! Bit 28-31 */


}Reg_DMA_Flag_Type_HIFCR;

typedef struct
{
	RegIO RegType EN		:1; /*! Bit 0 */
	RegIO RegType DMEIE		:1; /*! Bit 1 */
	RegIO RegType TEIE		:1; /*! Bit 2 */
	RegIO RegType HTIE		:1; /*! Bit 3 */
	RegIO RegType TCIE		:1; /*! Bit 4 */
	RegIO RegType PFCTRL	:1; /*! Bit 5 */
	RegIO RegType DIR		:2; /*! Bit 6-7 */
	RegIO RegType CIRC		:1; /*! Bit 8 */
	RegIO RegType PINC		:1; /*! Bit 9 */
	RegIO RegType MINC		:1; /*! Bit 10 */
	RegIO RegType PSIZE		:2; /*! Bit 11-12*/
	RegIO RegType MSIZE		:2; /*! Bit 13-14 */
	RegIO RegType PINCOS	:1; /*! Bit 15 */
	RegIO RegType PL		:2; /*! Bit 16-17 */
	RegIO RegType DBM		:1; /*! Bit 18 */
	RegIO RegType CT		:1; /*! Bit 19 */
	RegIO RegType BIT20		:1; /*! Bit 20 */
	RegIO RegType PBURST	:2; /*! Bit 21-22 */
	RegIO RegType MBURST	:2; /*! Bit 23-24 */
	RegIO RegType CHSEL		:3; /*! Bit 25-27 */
	RegIO RegType BIT28 	:4; /*! Bit 28-31*/
}Reg_DMA_Stream_Type_CR;

typedef struct
{
	RegIO RegType NDT		:16; /*! Bit 0-15 */
	RegIO RegType 			:16; /*! Bit 16-31 */

}Reg_DMA_Stream_Type_NDTR;

typedef struct
{
	RegIO RegType PA		:32; /*! Bit 0-31 */
}Reg_DMA_Stream_Type_PAR;

typedef struct
{
	RegIO RegType M0A		:32; /*Bit 0-31 */
}Reg_DMA_Stream_Type_M0AR;

typedef struct
{
	RegIO RegType M1A		:32; /*Bit 0-31 */
}Reg_DMA_Stream_Type_M1AR;

typedef struct
{
	RegIO RegType FTH		:2; 	/*! Bit 0-1 */
	RegIO RegType DMDIS		:1; 	/*! Bit 2 */
	RegIO RegType FS		:3; 	/*! Bit 3-5*/
	RegIO RegType BIT6		:1; 	/*! Bit 6 */
	RegIO RegType FEIE		:1; 	/*! Bit 7 */
	RegIO RegType BIT8		:24; 	/*! Bit 8-31 */
}Reg_DMA_Stream_Type_FCR;


typedef struct
{
	Reg_DMA_Flag_Type_LISR 		LISR;
	Reg_DMA_Flag_Type_HISR		HISR;
	Reg_DMA_Flag_Type_LIFCR		LIFCR;
	Reg_DMA_Flag_Type_HIFCR		HIFCR;

}Reg_DMA_Flag_Type_All;



typedef struct
{
	Reg_DMA_Stream_Type_CR 		CR;
	Reg_DMA_Stream_Type_NDTR 	NDTR;
	Reg_DMA_Stream_Type_PAR		PAR;
	Reg_DMA_Stream_Type_M0AR	M0AR;
	Reg_DMA_Stream_Type_M1AR	M1AR;
	Reg_DMA_Stream_Type_FCR		FCR;
}Reg_DMA_Stream_Type_All;

#define Reg_DMA1_LISR			((Reg_DMA_Flag_Type_LISR *) 	&(DMA1->LISR))
#define Reg_DMA1_HISR			((Reg_DMA_Flag_Type_HISR *) 	&(DMA1->HISR))
#define Reg_DMA1_LIFCR			((Reg_DMA_Flag_Type_LIFCR *) 	&(DMA1->LIFCR))
#define Reg_DMA1_HIFCR			((Reg_DMA_Flag_Type_HIFCR *) 	&(DMA1->HIFCR))

#define Reg_DMA1_Stream0_CR		((Reg_DMA_Stream_Type_CR *) 	&(DMA1_Stream0->CR))
#define Reg_DMA1_Stream0_NDTR	((Reg_DMA_Stream_Type_NDTR *) 	&(DMA1_Stream0->NDTR))
#define Reg_DMA1_Stream0_PAR	((Reg_DMA_Stream_Type_PAR *) 	&(DMA1_Stream0->PAR))
#define Reg_DMA1_Stream0_M0AR	((Reg_DMA_Stream_Type_M0AR *) 	&(DMA1_Stream0->M0AR))
#define Reg_DMA1_Stream0_M1AR	((Reg_DMA_Stream_Type_M1AR *) 	&(DMA1_Stream0->M1AR))
#define Reg_DMA1_Stream0_FCR	((Reg_DMA_Stream_Type_FCR *) 	&(DMA1_Stream0->FCR))

#define Reg_DMA1_Stream1_CR		((Reg_DMA_Stream_Type_CR *) 	&(DMA1_Stream1->CR))
#define Reg_DMA1_Stream1_NDTR	((Reg_DMA_Stream_Type_NDTR *) 	&(DMA1_Stream1->NDTR))
#define Reg_DMA1_Stream1_PAR	((Reg_DMA_Stream_Type_PAR *) 	&(DMA1_Stream1->PAR))
#define Reg_DMA1_Stream1_M0AR	((Reg_DMA_Stream_Type_M0AR *) 	&(DMA1_Stream1->M0AR))
#define Reg_DMA1_Stream1_M1AR	((Reg_DMA_Stream_Type_M1AR *) 	&(DMA1_Stream1->M1AR))
#define Reg_DMA1_Stream1_FCR	((Reg_DMA_Stream_Type_FCR *) 	&(DMA1_Stream1->FCR))

#define Reg_DMA1_Stream2_CR		((Reg_DMA_Stream_Type_CR *) 	&(DMA1_Stream2->CR))
#define Reg_DMA1_Stream2_NDTR	((Reg_DMA_Stream_Type_NDTR *) 	&(DMA1_Stream2->NDTR))
#define Reg_DMA1_Stream2_PAR	((Reg_DMA_Stream_Type_PAR *) 	&(DMA1_Stream2->PAR))
#define Reg_DMA1_Stream2_M0AR	((Reg_DMA_Stream_Type_M0AR *) 	&(DMA1_Stream2->M0AR))
#define Reg_DMA1_Stream2_M1AR	((Reg_DMA_Stream_Type_M1AR *) 	&(DMA1_Stream2->M1AR))
#define Reg_DMA1_Stream2_FCR	((Reg_DMA_Stream_Type_FCR *) 	&(DMA1_Stream2->FCR))

#define Reg_DMA1_Stream3_CR		((Reg_DMA_Stream_Type_CR *) 	&(DMA1_Stream3->CR))
#define Reg_DMA1_Stream3_NDTR	((Reg_DMA_Stream_Type_NDTR *) 	&(DMA1_Stream3->NDTR))
#define Reg_DMA1_Stream3_PAR	((Reg_DMA_Stream_Type_PAR *) 	&(DMA1_Stream3->PAR))
#define Reg_DMA1_Stream3_M0AR	((Reg_DMA_Stream_Type_M0AR *) 	&(DMA1_Stream3->M0AR))
#define Reg_DMA1_Stream3_M1AR	((Reg_DMA_Stream_Type_M1AR *) 	&(DMA1_Stream3->M1AR))
#define Reg_DMA1_Stream3_FCR	((Reg_DMA_Stream_Type_FCR *) 	&(DMA1_Stream3->FCR))

#define Reg_DMA1_Stream4_CR		((Reg_DMA_Stream_Type_CR *) 	&(DMA1_Stream4->CR))
#define Reg_DMA1_Stream4_NDTR	((Reg_DMA_Stream_Type_NDTR *) 	&(DMA1_Stream4->NDTR))
#define Reg_DMA1_Stream4_PAR	((Reg_DMA_Stream_Type_PAR *) 	&(DMA1_Stream4->PAR))
#define Reg_DMA1_Stream4_M0AR	((Reg_DMA_Stream_Type_M0AR *) 	&(DMA1_Stream4->M0AR))
#define Reg_DMA1_Stream4_M1AR	((Reg_DMA_Stream_Type_M1AR *) 	&(DMA1_Stream4->M1AR))
#define Reg_DMA1_Stream4_FCR	((Reg_DMA_Stream_Type_FCR *) 	&(DMA1_Stream4->FCR))

#define Reg_DMA1_Stream5_CR		((Reg_DMA_Stream_Type_CR *) 	&(DMA1_Stream5->CR))
#define Reg_DMA1_Stream5_NDTR	((Reg_DMA_Stream_Type_NDTR *) 	&(DMA1_Stream5->NDTR))
#define Reg_DMA1_Stream5_PAR	((Reg_DMA_Stream_Type_PAR *) 	&(DMA1_Stream5->PAR))
#define Reg_DMA1_Stream5_M0AR	((Reg_DMA_Stream_Type_M0AR *) 	&(DMA1_Stream5->M0AR))
#define Reg_DMA1_Stream5_M1AR	((Reg_DMA_Stream_Type_M1AR *) 	&(DMA1_Stream5->M1AR))
#define Reg_DMA1_Stream5_FCR	((Reg_DMA_Stream_Type_FCR *) 	&(DMA1_Stream5->FCR))

#define Reg_DMA1_Stream6_CR		((Reg_DMA_Stream_Type_CR *) 	&(DMA1_Stream6->CR))
#define Reg_DMA1_Stream6_NDTR	((Reg_DMA_Stream_Type_NDTR *) 	&(DMA1_Stream6->NDTR))
#define Reg_DMA1_Stream6_PAR	((Reg_DMA_Stream_Type_PAR *) 	&(DMA1_Stream6->PAR))
#define Reg_DMA1_Stream6_M0AR	((Reg_DMA_Stream_Type_M0AR *) 	&(DMA1_Stream6->M0AR))
#define Reg_DMA1_Stream6_M1AR	((Reg_DMA_Stream_Type_M1AR *) 	&(DMA1_Stream6->M1AR))
#define Reg_DMA1_Stream6_FCR	((Reg_DMA_Stream_Type_FCR *) 	&(DMA1_Stream6->FCR))

#define Reg_DMA1_Stream7_CR		((Reg_DMA_Stream_Type_CR *) 	&(DMA1_Stream7->CR))
#define Reg_DMA1_Stream7_NDTR	((Reg_DMA_Stream_Type_NDTR *) 	&(DMA1_Stream7->NDTR))
#define Reg_DMA1_Stream7_PAR	((Reg_DMA_Stream_Type_PAR *) 	&(DMA1_Stream7->PAR))
#define Reg_DMA1_Stream7_M0AR	((Reg_DMA_Stream_Type_M0AR *) 	&(DMA1_Stream7->M0AR))
#define Reg_DMA1_Stream7_M1AR	((Reg_DMA_Stream_Type_M1AR *) 	&(DMA1_Stream7->M1AR))
#define Reg_DMA1_Stream7_FCR	((Reg_DMA_Stream_Type_FCR *) 	&(DMA1_Stream7->FCR))

#define Reg_DMA2_LISR			((Reg_DMA_Flag_Type_LISR *) 	&(DMA2->LISR))
#define Reg_DMA2_HISR			((Reg_DMA_Flag_Type_HISR *) 	&(DMA2->HISR))
#define Reg_DMA2_LIFCR			((Reg_DMA_Flag_Type_LIFCR *) 	&(DMA2->LIFCR))
#define Reg_DMA2_HIFCR			((Reg_DMA_Flag_Type_HIFCR *) 	&(DMA2->HIFCR))

#define Reg_DMA2_Stream0_CR		((Reg_DMA_Stream_Type_CR *) 	&(DMA2_Stream0->CR))
#define Reg_DMA2_Stream0_NDTR	((Reg_DMA_Stream_Type_NDTR *) 	&(DMA2_Stream0->NDTR))
#define Reg_DMA2_Stream0_PAR	((Reg_DMA_Stream_Type_PAR *) 	&(DMA2_Stream0->PAR))
#define Reg_DMA2_Stream0_M0AR	((Reg_DMA_Stream_Type_M0AR *) 	&(DMA2_Stream0->M0AR))
#define Reg_DMA2_Stream0_M1AR	((Reg_DMA_Stream_Type_M1AR *) 	&(DMA2_Stream0->M1AR))
#define Reg_DMA2_Stream0_FCR	((Reg_DMA_Stream_Type_FCR *) 	&(DMA2_Stream0->FCR))

#define Reg_DMA2_Stream1_CR		((Reg_DMA_Stream_Type_CR *) 	&(DMA2_Stream1->CR))
#define Reg_DMA2_Stream1_NDTR	((Reg_DMA_Stream_Type_NDTR *) 	&(DMA2_Stream1->NDTR))
#define Reg_DMA2_Stream1_PAR	((Reg_DMA_Stream_Type_PAR *) 	&(DMA2_Stream1->PAR))
#define Reg_DMA2_Stream1_M0AR	((Reg_DMA_Stream_Type_M0AR *) 	&(DMA2_Stream1->M0AR))
#define Reg_DMA2_Stream1_M1AR	((Reg_DMA_Stream_Type_M1AR *) 	&(DMA2_Stream1->M1AR))
#define Reg_DMA2_Stream1_FCR	((Reg_DMA_Stream_Type_FCR *) 	&(DMA2_Stream1->FCR))

#define Reg_DMA2_Stream2_CR		((Reg_DMA_Stream_Type_CR *) 	&(DMA2_Stream2->CR))
#define Reg_DMA2_Stream2_NDTR	((Reg_DMA_Stream_Type_NDTR *) 	&(DMA2_Stream2->NDTR))
#define Reg_DMA2_Stream2_PAR	((Reg_DMA_Stream_Type_PAR *) 	&(DMA2_Stream2->PAR))
#define Reg_DMA2_Stream2_M0AR	((Reg_DMA_Stream_Type_M0AR *) 	&(DMA2_Stream2->M0AR))
#define Reg_DMA2_Stream2_M1AR	((Reg_DMA_Stream_Type_M1AR *) 	&(DMA2_Stream2->M1AR))
#define Reg_DMA2_Stream2_FCR	((Reg_DMA_Stream_Type_FCR *) 	&(DMA2_Stream2->FCR))

#define Reg_DMA2_Stream3_CR		((Reg_DMA_Stream_Type_CR *) 	&(DMA2_Stream3->CR))
#define Reg_DMA2_Stream3_NDTR	((Reg_DMA_Stream_Type_NDTR *) 	&(DMA2_Stream3->NDTR))
#define Reg_DMA2_Stream3_PAR	((Reg_DMA_Stream_Type_PAR *) 	&(DMA2_Stream3->PAR))
#define Reg_DMA2_Stream3_M0AR	((Reg_DMA_Stream_Type_M0AR *) 	&(DMA2_Stream3->M0AR))
#define Reg_DMA2_Stream3_M1AR	((Reg_DMA_Stream_Type_M1AR *) 	&(DMA2_Stream3->M1AR))
#define Reg_DMA2_Stream3_FCR	((Reg_DMA_Stream_Type_FCR *) 	&(DMA2_Stream3->FCR))

#define Reg_DMA2_Stream4_CR		((Reg_DMA_Stream_Type_CR *) 	&(DMA2_Stream4->CR))
#define Reg_DMA2_Stream4_NDTR	((Reg_DMA_Stream_Type_NDTR *) 	&(DMA2_Stream4->NDTR))
#define Reg_DMA2_Stream4_PAR	((Reg_DMA_Stream_Type_PAR *) 	&(DMA2_Stream4->PAR))
#define Reg_DMA2_Stream4_M0AR	((Reg_DMA_Stream_Type_M0AR *) 	&(DMA2_Stream4->M0AR))
#define Reg_DMA2_Stream4_M1AR	((Reg_DMA_Stream_Type_M1AR *) 	&(DMA2_Stream4->M1AR))
#define Reg_DMA2_Stream4_FCR	((Reg_DMA_Stream_Type_FCR *) 	&(DMA2_Stream4->FCR))

#define Reg_DMA2_Stream5_CR		((Reg_DMA_Stream_Type_CR *) 	&(DMA2_Stream5->CR))
#define Reg_DMA2_Stream5_NDTR	((Reg_DMA_Stream_Type_NDTR *) 	&(DMA2_Stream5->NDTR))
#define Reg_DMA2_Stream5_PAR	((Reg_DMA_Stream_Type_PAR *) 	&(DMA2_Stream5->PAR))
#define Reg_DMA2_Stream5_M0AR	((Reg_DMA_Stream_Type_M0AR *) 	&(DMA2_Stream5->M0AR))
#define Reg_DMA2_Stream5_M1AR	((Reg_DMA_Stream_Type_M1AR *) 	&(DMA2_Stream5->M1AR))
#define Reg_DMA2_Stream5_FCR	((Reg_DMA_Stream_Type_FCR *) 	&(DMA2_Stream5->FCR))

#define Reg_DMA2_Stream6_CR		((Reg_DMA_Stream_Type_CR *) 	&(DMA2_Stream6->CR))
#define Reg_DMA2_Stream6_NDTR	((Reg_DMA_Stream_Type_NDTR *) 	&(DMA2_Stream6->NDTR))
#define Reg_DMA2_Stream6_PAR	((Reg_DMA_Stream_Type_PAR *) 	&(DMA2_Stream6->PAR))
#define Reg_DMA2_Stream6_M0AR	((Reg_DMA_Stream_Type_M0AR *) 	&(DMA2_Stream6->M0AR))
#define Reg_DMA2_Stream6_M1AR	((Reg_DMA_Stream_Type_M1AR *) 	&(DMA2_Stream6->M1AR))
#define Reg_DMA2_Stream6_FCR	((Reg_DMA_Stream_Type_FCR *) 	&(DMA2_Stream6->FCR))

#define Reg_DMA2_Stream7_CR		((Reg_DMA_Stream_Type_CR *) 	&(DMA2_Stream7->CR))
#define Reg_DMA2_Stream7_NDTR	((Reg_DMA_Stream_Type_NDTR *) 	&(DMA2_Stream7->NDTR))
#define Reg_DMA2_Stream7_PAR	((Reg_DMA_Stream_Type_PAR *) 	&(DMA2_Stream7->PAR))
#define Reg_DMA2_Stream7_M0AR	((Reg_DMA_Stream_Type_M0AR *) 	&(DMA2_Stream7->M0AR))
#define Reg_DMA2_Stream7_M1AR	((Reg_DMA_Stream_Type_M1AR *) 	&(DMA2_Stream7->M1AR))
#define Reg_DMA2_Stream7_FCR	((Reg_DMA_Stream_Type_FCR *) 	&(DMA2_Stream7->FCR))


#ifdef __cplusplus
	extern "C" {
#endif


#ifdef __cplusplus
	}
#endif
#endif /* MCU_REG_DMA_H_ */
