/*!\file MCU_Reg_RCC.h
*  \date Oct 24, 2015
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef MCU_REG_RCC_H_
#define MCU_REG_RCC_H_


#ifdef __cplusplus
	extern "C" {
#endif
#include "stm32f4xx.h"
#include "stm32f4xx_Types.h"
#include "stm32f4xx_RegTypes.h"

typedef struct
{
	RegIO RegType GPIOAEN		:	1;/* bit 0 */
	RegIO RegType GPIOBEN		:	1;/* bit 1 */
	RegIO RegType GPIOCEN		:	1;/* bit 2 */
	RegIO RegType GPIODEN		:	1;/* bit 3 */
	RegIO RegType GPIOEEN		:	1;/* bit 4 */
	RegIO RegType GPIOFEN		:	1;/* bit 5 */
	RegIO RegType GPIOGEN		:	1;/* bit 6 */
	RegIO RegType GPIOHEN		:	1;/* bit 7 */
	RegIO RegType GPIOIEN		:	1;/* bit 8 */
	RegIO RegType GPIOJEN		:	1;/* bit 9 */
	RegIO RegType GPIOKEN		:	1;/* bit 10 */
	RegIO RegType Rsvd1			:	1;/* bit 11 */
	RegIO RegType CRCEN			:	1;/* bit 12 */
	RegIO RegType Rsvd2			:	5;/* bit 13-17 */
	RegIO RegType BKPSRAMEN		:	1;/* bit 18 */
	RegIO RegType Rsvd3			:	1;/* bit 19 */
	RegIO RegType CCMDATARAMEN	:	1;/* bit 20 */
	RegIO RegType DMA1EN		:	1;/* bit 21 */
	RegIO RegType DMA2EN		:	1;/* bit 22 */
	RegIO RegType DMA2DEN		:	1;/* bit 23 */
	RegIO RegType Rsvd4			:	1;/* bit 24 */
	RegIO RegType ETHMACEN		:	1;/* bit 25 */
	RegIO RegType ETHMACTXEN	:	1;/* bit 26 */
	RegIO RegType ETHMACRXEN	:	1;/* bit 27 */
	RegIO RegType ETHMACPTPEN	:	1;/* bit 28 */
	RegIO RegType OTGHSEN		:	1;/* bit 29 */
	RegIO RegType OTGHSULPIEN	:	1;/* bit 30 */
	RegIO RegType Rsvd5     	:	1;/* bit 31 */
} Reg_RCC_AHB1ENR_Bit_Type;

typedef struct
{
	RegIO RegType DCMIEN		:	1; /* bit 0 */
	RegIO RegType Rsvd1			: 	3; /* bit 1 to 3 */
	RegIO RegType CRYPEN		:	1; /* bit 4 */
	RegIO RegType HASHEN		: 	1; /* bit 5 */
	RegIO RegType RNGEN			:	1; /* bit 6 */
	RegIO RegType OTGFSEN		: 	1; /* bit 7 */
	RegIO RegType Rsvd2			:	24;/* bit 8 to 31 */
}Reg_RCC_AHB2ENR_Bit_Type;

typedef struct
{
	RegIO RegType FMCEN			:	1; /* bit 0 */
	RegIO RegType Rsvd1			:	31;/* bit 1 to 31 */
}Reg_RCC_AHB3ENR_Bit_Type;

typedef struct
{
	RegIO RegType TIM2EN		:	1; /* bit 0 */
	RegIO RegType TIM3EN		:	1; /* bit 1 */
	RegIO RegType TIM4EN		:	1; /* bit 2*/
	RegIO RegType TIM5EN		:	1; /* bit 3 */
	RegIO RegType TIM6EN		:	1; /* bit 4 */
	RegIO RegType TIM7EN		:	1; /* bit 5 */
	RegIO RegType TIM12EN		:	1; /* bit 6 */
	RegIO RegType TIM13EN		:	1; /* bit 7 */
	RegIO RegType TIM14EN		:	1; /* bit 8 */
	RegIO RegType Rsvd1			:	2; /* bit 9-10 */
	RegIO RegType WWDGEN		:	1; /* bit 11 */
	RegIO RegType Rsvd2			:	2; /* bit 12-13 */
	RegIO RegType SPI2EN		:	1; /* bit 14 */
	RegIO RegType SPI3EN		:	1; /* bit 15 */
	RegIO RegType Rsvd3			:	1; /* bit 16 */
	RegIO RegType USART2EN		:	1; /* bit 17 */
	RegIO RegType USART3EN		:	1; /* bit 18 */
	RegIO RegType UART4EN		:	1; /* bit 19 */
	RegIO RegType UART5EN		:	1; /* bit 20 */
	RegIO RegType I2C1EN		:	1; /* bit 21 */
	RegIO RegType I2C2EN		:	1; /* bit 22 */
	RegIO RegType I2C3EN		:	1; /* bit 23 */
	RegIO RegType Rsvd4			:	1; /* bit 24 */
	RegIO RegType CAN1EN		:	1; /* bit 25 */
	RegIO RegType CAN2EN		:	1; /* bit 26 */
	RegIO RegType Rsvd5			:	1; /* bit 27 */
	RegIO RegType PWREN			:	1; /* bit 28 */
	RegIO RegType DACEN			:	1; /* bit 29 */
	RegIO RegType UART7EN		:	1; /* bit 30 */
	RegIO RegType UART8EN		:	1; /* bit 31 */
}Reg_RCC_APB1ENR_Bit_Type;


typedef struct
{
	RegIO RegType TIM1EN		:	1; /* bit 0 */
	RegIO RegType TIM8EN		:	1; /* bit 1 */
	RegIO RegType Rsvd1			:	2; /* bit 2-3 */
	RegIO RegType USART1EN		:	1; /* bit 4 */
	RegIO RegType USART6EN		:	1; /* bit 5 */
	RegIO RegType Rsvd2			:	2; /* bit 6-7 */
	RegIO RegType ADC1EN		:	1; /* bit 8 */
	RegIO RegType ADC2EN		:	1; /* bit 9 */
	RegIO RegType ADC3EN		:	1; /* bit 10 */
	RegIO RegType SDIOEN		:	1; /* bit 11 */
	RegIO RegType SPI1EN		:	1; /* bit 12 */
	RegIO RegType SPI4EN		:	1; /* bit 13 */
	RegIO RegType SYSCFGEN		:	1; /* bit 14 */
	RegIO RegType Rsvd3			:	1; /* bit 15 */
	RegIO RegType TIM9EN		:	1; /* bit 16 */
	RegIO RegType TIM10EN		:	1; /* bit 17 */
	RegIO RegType TIM11EN		:	1; /* bit 18 */
	RegIO RegType Rsvd4			:	1; /* bit 19 */
	RegIO RegType SPI5EN		:	1; /* bit 20 */
	RegIO RegType SPI6EN		:	1; /* bit 21 */
	RegIO RegType SAI1EN		:	1; /* bit 22 */
	RegIO RegType Rsvd5			:	3; /* bit 23-25 */
	RegIO RegType LTDCEN		:	1; /* bit 26 */
	RegIO RegType Rsvd6			:	5; /* bit 27-31 */
}Reg_RCC_APB2ENR_Bit_Type;




typedef union
{
	uint32 						Whole;
	Reg_RCC_AHB1ENR_Bit_Type	BitField;
} Reg_RCC_AHB1ENR_Type;

typedef union
{
	uint32 						Whole;
	Reg_RCC_AHB2ENR_Bit_Type	BitField;
} Reg_RCC_AHB2ENR_Type;

typedef union
{
	uint32 						Whole;
	Reg_RCC_AHB3ENR_Bit_Type	BitField;
} Reg_RCC_AHB3ENR_Type;

typedef union
{
	uint32 						Whole;
	Reg_RCC_APB1ENR_Bit_Type	BitField;
} Reg_RCC_APB1ENR_Type;

typedef union
{
	uint32 						Whole;
	Reg_RCC_APB2ENR_Bit_Type	BitField;
} Reg_RCC_APB2ENR_Type;

#define Reg_RCC_AHB1ENR			((Reg_RCC_AHB1ENR_Type *) (&(RCC->AHB1ENR)))
#define Reg_RCC_AHB2ENR			((Reg_RCC_AHB2ENR_Type *) (&(RCC->AHB2ENR)))
#define Reg_RCC_AHB3ENR			((Reg_RCC_AHB3ENR_Type *) (&(RCC->AHB3ENR)))
#define Reg_RCC_APB1ENR			((Reg_RCC_APB1ENR_Type *) (&(RCC->APB1ENR)))
#define Reg_RCC_APB2ENR			((Reg_RCC_APB2ENR_Type *) (&(RCC->APB2ENR)))

#ifdef __cplusplus
	}
#endif
#endif /* MCU_REG_RCC_H_ */
