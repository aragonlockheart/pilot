/*!\file MCU_Reg_Timer1_6.h
*  \date Dec 25, 2016
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef MCU_REG_TIMER1_6_H_
#define MCU_REG_TIMER1_6_H_



#ifdef __cplusplus
	extern "C" {
#endif


/* Define the bit field types for timer's register */
typedef struct
{
	volatile uint32 CEN			:1;	 /* Bit 0 */
	volatile uint32 UDIS		:1;  /* Bit 1 */
	volatile uint32 URS			:1;  /* Bit 2 */
	volatile uint32 OPM			:1;  /* Bit 3 */
	volatile uint32 Rsvd1		:3;  /* Bit 4-6 */
	volatile uint32 ARPE		:1;  /* Bit 7 */
	volatile uint32 Rsvd2		:24; /* Bit 8-31 */

}MCU_Reg_Time1_6_CR1_Bitfield_Type;

typedef struct
{
	volatile uint32 Rsvd1		:4; /* Bit 0 to 3 */
	volatile uint32 MMS			:3; /* Bit 4 to 6 */
	volatile uint32 Rsvd2		:25;/* Bit 7 to 31 */
}MCU_Reg_Time1_6_CR2_Bitfield_Type;


typedef struct
{
	volatile uint32 UIE			:1; /*Bit 0 */
	volatile uint32 Rsvd1		:7; /*Bit 1 to 7 */
	volatile uint32 UDE			:1; /*Bit 8 */
	volatile uint32 Rsvd2		:23;/*Bit 9 to 31 */
}MCU_Reg_Time1_6_DIER_Bitfield_Type;

typedef struct
{
	volatile uint32 UIF			:1; /*Bit 0 */
	volatile uint32 Rsvd1		:31;/*Bit 1 to 31*/
}MCU_Reg_Time1_6_SR_Bitfield_Type;

typedef struct
{
	volatile uint32 UG			:1; /*Bit 0 */
	volatile uint32 Rsvd1		:31;/*Bit 1 to 31*/
}MCU_Reg_Time1_6_EGR_Bitfield_Type;

typedef struct
{
	volatile uint16 CNT;
	volatile uint16 Rsvd;
}MCU_Reg_Time1_6_CNT_Bitfield_Type;

typedef struct
{
	volatile uint16 PSC;
	volatile uint16 Rsvd;
}MCU_Reg_Time1_6_PSC_Bitfield_Type;

typedef struct
{
	volatile uint16 ARR;
	volatile uint16 Rsvd;
}MCU_Reg_Time1_6_ARR_Bitfield_Type;
/* end definition of bit fields */

/* Define register's whole type */

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time1_6_CR1_Bitfield_Type 	Bitfield;
}MCU_Reg_Time1_6_CR1;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time1_6_CR2_Bitfield_Type 	Bitfield;
}MCU_Reg_Time1_6_CR2;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time1_6_DIER_Bitfield_Type 	Bitfield;
}MCU_Reg_Time1_6_DIER;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time1_6_SR_Bitfield_Type 	Bitfield;
}MCU_Reg_Time1_6_SR;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time1_6_EGR_Bitfield_Type 	Bitfield;
}MCU_Reg_Time1_6_EGR;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time1_6_CNT_Bitfield_Type 	Bitfield;
}MCU_Reg_Time1_6_CNT;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time1_6_PSC_Bitfield_Type 	Bitfield;
}MCU_Reg_Time1_6_PSC;

typedef union
{
	uint32 								Whole;
	MCU_Reg_Time1_6_ARR_Bitfield_Type 	Bitfield;
}MCU_Reg_Time1_6_ARR;
#ifdef __cplusplus
	}
#endif
#endif /* MCU_REG_TIMER1_6_H_ */
