/*!\file MCU_Reg.h
*  \date Apr 9, 2016
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef MCU_REG_H_
#define MCU_REG_H_

#include "MCU_Reg_DMA.h"
#include "MCU_Reg_RCC.h"

#ifdef __cplusplus
	extern "C" {
#endif


#ifdef __cplusplus
	}
#endif
#endif /* MCU_REG_H_ */
