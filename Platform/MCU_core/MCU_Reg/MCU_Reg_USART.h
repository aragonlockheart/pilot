/*!\file MCU_Reg_USART.h
*  \date Sep 26, 2016
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef MCU_REG_USART_H_
#define MCU_REG_USART_H_

#include "stm32f4xx.h"
#include "stm32f4xx_Types.h"
#include "stm32f4xx_RegTypes.h"

#ifdef __cplusplus
	extern "C" {
#endif

typedef struct
{
	RegIO uint32 PE		: 		1; /* Bit 0 */
	RegIO uint32 FE		: 		1; /* Bit 1 */
	RegIO uint32 NF		: 		1; /* Bit 2 */
	RegIO uint32 ORE	: 		1; /* Bit 3 */
	RegIO uint32 IDLE	: 		1; /* Bit 4 */
	RegIO uint32 RXNE	: 		1; /* Bit 5 */
	RegIO uint32 TC		: 		1; /* Bit 6 */
	RegIO uint32 TXE	: 		1; /* Bit 7 */
	RegIO uint32 LBD	: 		1; /* Bit 8 */
	RegIO uint32 CTS	: 		1; /* Bit 9 */
	RegIO uint32 Reserved:	   22; /* Bit 10 to 31 */

}REG_SR_BIT;

typedef struct
{
	RegIO uint32 DR			: 		9; /* Bit 0 to 8 */
	RegIO uint32 Reserved	:	   23; /* Bit 9 to 31 */

}REG_DR_BIT;

typedef struct
{
	RegIO uint32 Fraction	: 		4; /* Bit 0 to 3 */
	RegIO uint32 Mantissa	: 	   12; /* Bit 4 to 15 */
	RegIO uint32 Reserved	:	   16; /* Bit 16 to 31 */

}REG_BRR_BIT;

typedef struct
{
	RegIO uint32 SBK		: 		1; /* Bit 0 */
	RegIO uint32 RWU		: 		1; /* Bit 1 */
	RegIO uint32 RE			: 	    1; /* Bit 2 */
	RegIO uint32 TE			: 	    1; /* Bit 3 */
	RegIO uint32 IDLEIE		: 	    1; /* Bit 4 */
	RegIO uint32 RXNEIE		: 	    1; /* Bit 5 */
	RegIO uint32 TCIE		: 	    1; /* Bit 6 */
	RegIO uint32 TXEIE		: 	    1; /* Bit 7 */
	RegIO uint32 PEIE		: 	    1; /* Bit 8 */
	RegIO uint32 PS			: 	    1; /* Bit 9 */
	RegIO uint32 PCE		: 	    1; /* Bit 10 */
	RegIO uint32 WAKE		: 	    1; /* Bit 11 */
	RegIO uint32 M			: 	    1; /* Bit 12 */
	RegIO uint32 UE			: 	    1; /* Bit 13 */
	RegIO uint32 Reserved1	: 	    1; /* Bit 14 */
	RegIO uint32 OVER8		: 	    1; /* Bit 15 */
	RegIO uint32 Reserved2	:	   16; /* Bit 16 to 31 */

}REG_CR1_BIT;

typedef struct
{
	RegIO uint32 ADD		: 		4; /* Bit 0 to 3 */
	RegIO uint32 Reserved1	: 		1; /* Bit 4 */
	RegIO uint32 LBDL		: 	    1; /* Bit 5 */
	RegIO uint32 LBDIE		: 	    1; /* Bit 6 */
	RegIO uint32 Reserved2	: 	    1; /* Bit 7 */
	RegIO uint32 LBCL		: 	    1; /* Bit 8 */
	RegIO uint32 CPHA		: 	    1; /* Bit 9 */
	RegIO uint32 CPOL		: 	    1; /* Bit 10 */
	RegIO uint32 CLKEN		: 	    1; /* Bit 11 */
	RegIO uint32 STOP		: 	    2; /* Bit 12 to 13 */
	RegIO uint32 LINEN		: 	    1; /* Bit 14 */
	RegIO uint32 Reserved3	: 	   17; /* Bit 15 to 31 */

}REG_CR2_BIT;

typedef struct
{
	RegIO uint32 EIE		: 		1; /* Bit 0 */
	RegIO uint32 IREN		: 		1; /* Bit 1 */
	RegIO uint32 IRLP		: 	    1; /* Bit 2 */
	RegIO uint32 HDSEL		: 	    1; /* Bit 3 */
	RegIO uint32 NACK		: 	    1; /* Bit 4 */
	RegIO uint32 SCEN		: 	    1; /* Bit 5 */
	RegIO uint32 DMAR		: 	    1; /* Bit 6 */
	RegIO uint32 DMAT		: 	    1; /* Bit 7 */
	RegIO uint32 RTSE		: 	    1; /* Bit 8 */
	RegIO uint32 CTSE		: 	    1; /* Bit 9 */
	RegIO uint32 CTSIE		: 	    1; /* Bit 10 */
	RegIO uint32 ONEBIT		: 	    1; /* Bit 11 */
	RegIO uint32 Reserved	: 	   20; /* Bit 12 to 31 */

}REG_CR3_BIT;

typedef struct
{
	RegIO uint32 PSC		: 		8; /* Bit 0 to 7 */
	RegIO uint32 GT		    : 		8; /* Bit 8 to 15 */
	RegIO uint32 Reserved	: 	   16; /* Bit 16 to 31 */

}REG_GTPR_BIT;

typedef union
{
	RegIO REG_SR_BIT	REG_SR_DETAIL;
	RegIO uint32 		REG_SR_WHOLE;
}USART_SR_Type;

typedef union
{
	RegIO REG_DR_BIT	REG_DR_DETAIL;
	RegIO uint32 		REG_DR_WHOLE;
}USART_DR_Type;

typedef union
{
	RegIO REG_BRR_BIT	REG_BRR_DETAIL;
	RegIO uint32 		REG_BRR_WHOLE;
}USART_BRR_Type;

typedef union
{
	RegIO REG_CR1_BIT	REG_CR1_DETAIL;
	RegIO uint32 		REG_CR1_WHOLE;
}USART_CR1_Type;

typedef union
{
	RegIO REG_CR2_BIT	REG_CR2_DETAIL;
	RegIO uint32 		REG_CR2_WHOLE;
}USART_CR2_Type;

typedef union
{
	RegIO REG_CR3_BIT	REG_CR3_DETAIL;
	RegIO uint32 		REG_CR3_WHOLE;
}USART_CR3_Type;

typedef union
{
	RegIO REG_GTPR_BIT	REG_GTPR_DETAIL;
	RegIO uint32 		REG_GTPR_WHOLE;
}USART_GTPR_Type;

typedef struct
{
	RegIO USART_SR_Type 	SR;
	RegIO USART_DR_Type 	DR;
	RegIO USART_BRR_Type 	BRR;
	RegIO USART_CR1_Type    CR1;
	RegIO USART_CR2_Type    CR2;
	RegIO USART_CR3_Type	CR3;
	RegIO USART_GTPR_Type   GTPR;

}USART_AllRegType;

#ifdef __cplusplus
	}
#endif
#endif /* MCU_REG_USART_H_ */
