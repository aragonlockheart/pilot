/*!\file MCU_Cfg.h
*  \date Apr 9, 2016
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef MCU_CFG_H_
#define MCU_CFG_H_

#include "stm32f4xx_conf.h"
#include "stm32f4xx.h"
#include "system_stm32f4xx.h"

#ifdef __cplusplus
	extern "C" {
#endif


#ifdef __cplusplus
	}
#endif
#endif /* MCU_CFG_H_ */
