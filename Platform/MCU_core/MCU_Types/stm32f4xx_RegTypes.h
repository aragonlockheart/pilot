/*!\file stm32f4xx_RegTypes.h
*  \date Oct 24, 2015
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef STM32F4XX_REGTYPES_H_
#define STM32F4XX_REGTYPES_H_

#include "stm32f4xx_Types.h"

#ifdef __cplusplus
	extern "C" {
#endif

typedef uint32	RegType;

#ifdef __cplusplus
	}
#endif
#endif /* STM32F4XX_REGTYPES_H_ */
