/*!\file MCU_Types.h
*  \date Apr 9, 2016
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef MCU_TYPES_H_
#define MCU_TYPES_H_

#include "stm32f4xx_RegTypes.h"
#include "stm32f4xx_Types.h"

#ifdef __cplusplus
	extern "C" {
#endif


#ifdef __cplusplus
	}
#endif
#endif /* MCU_TYPES_H_ */
