/*! \file stm32f4xx_Types.h
 *  \brief This file contains declaration of most basic system types
 *	\date 14-June-2015
 *	\author Nguyen Trong Viet
 */


#ifndef STM32F4XX_TYPES_H
#define STM32F4XX_TYPES_H
#include "STD_Types.h"

#ifdef __cplusplus
	extern "C" {
#endif



#define ZERO	0x00u

#define ZERO_8	((sint8) 0x00u)			//!< Zero in uint8 type
#define ZERO_16	((sint16) 0x0000u)		//!< Zero in uint16 type
#define ZERO_32 ((sint32) 0u)			//!< Zero in uint32 type
#define ZERO_64 ((sint64) 0u)			//!< Zero in UN64 type
#define ZERO_F	((float32) 0x00u)			//!< Zero in UN64 type
#define ZERO_D	((float64) 0x00u)		//!< Zero in UN64 type

#define ZERO_UNSIGNED_8	    ((uint8)   0x0u)		//!< Zero in uint8 type
#define ZERO_UNSIGNED_16	((uint16)  0x0u)		//!< Zero in uint16 type
#define ZERO_UNSIGNED_32    ((uint32)  0x0u)			//!< Zero in uint32 type
#define ZERO_UNSIGNED_64    ((uint64)  0x0u)			//!< Zero in UN64 type
#define ZERO_UNSIGNED_F	    ((float32) 0x0u)			//!< Zero in UN64 type
#define ZERO_UNSIGNED_D	    ((float64) 0x0u)		//!< Zero in UN64 type

#define ONE_8 	((sint8) 0x01u)
#define ONE_16	((sint16) 0x0001u)
#define ONE_32	((sint32) 1u)
#define ONE_64 	((sint64) 1u)
#define ONE_F	((float32) 0x01u)
#define ONE_D	((float64) 0x01u)



#define TWO_8		((sint8)  2u)
#define TWO_16		((sint16) 2u)
#define THREE_16 	((sint16) 3u)
#define FOUR_16		((sint16) 4u)
#define TWO_32 		((sint32) 2u)
#define TWO_64 		((sint64) 2u)



#define FIVE_8	((uint8)  5u)
#define FIVE_16	((uint16) 5u)
#define FIVE_32 ((uint32) 5u)
#define FIVE_64 ((uint64) 5u)

#define MAX_uint8	((uint8)	 0xFFu)
#define MAX_uint16	((uint16) 0xFFFFu)
#define MAX_uint32	((uint32) 0xFFFFFFFFu)
#define MAX_uint64	((uint64) 0xFFFFFFFFFFFFFFFFu)


#define ByteShift	0x08u
#define NibShift	0x04u

#define BitIsSet(Victim, Bit_Position)  ((((Victim) >> (Bit_Position)) & (ONE_64)) != ZERO_64)

#define BitIsCleaned(Victim, Bit_Position)  ((((Victim) >> (Bit_Position)) & (ONE_64)) == ZERO_64)

#define ClearBit(Victim, Bit_Position)  (Victim) = (((Victim) & (~(ONE_64 << (Bit_Position)))))

#define SetBit(Victim, Bit_Position) (Victim) = (((Victim) | (ONE_64 << (Bit_Position))))

#define ToggleBit(Victim, Bit_Position) ((Victim) = ((Victim) ^ ((ONE_64) << (Bit_Position))))

#define P_IsZero(Victim)	((!(Victim))&(0x01u))

#define P_IsNotZero(Victim) ((~(!(Victim)))&(0x01u))

#define DEVIDE_BY	/

#define REMAIN_BY	%

#define MULTIPLY 	*

#define EQUAL_CON		==

#define NOT_EQUAL_CON	!=

#define SMALLERTHAN_CON <

#define LARGERTHAN_CON  >

#define AND_CON			&&

#define OR_CON			||

#define STATIC			static

#define BW_AND			&

#define BW_OR			|

#define Shift_Left		<<

#define Shift_Right		>>

#define RegIO			__IO

#define VOID



struct LCD_Coord
{
	uint16 X_Add;	//!< Starting X address of the window
	uint16 Y_Add;	//!< Starting Y address of the window
	uint16 Height;	//!< Height of the window
	uint16 Width;	//!< Width of the window
};



struct LCD_Line
{
	uint16 Start_X; 	//!< Starting X coordination
	uint16 Start_Y; 	//!< Starting Y coordination
	uint16 End_X; 	//!< Ending X coordination
	uint16 End_Y; 	//!< Ending Y coordination
	uint16 Colour; 	//!< Colour of the line
};

typedef enum BOOL
{
	EM_DISABLE,	//!< 0:Disable case
	EM_ENABLE	//!< 1:Enable case
}BOOL;





#ifdef __cplusplus
	}
#endif

#endif
