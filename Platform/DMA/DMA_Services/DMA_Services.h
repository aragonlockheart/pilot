/*!\file DMA_Services.h
*  \date Nov 9, 2015
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef DMA_SERVICES_H_
#define DMA_SERVICES_H_

#include "DMA_DRV.h"
#include "DMA_Nm.h"

#ifdef __cplusplus
	extern "C" {
#endif

typedef enum
{
	DMA_SER_Succcessful, /*!0 */
	DMA_SER_Fail,		 /*!1 */
	DMA_SER_Busy		 /*!2 */
}DMA_SER_Rettype;


DMA_SER_Rettype DMA_SER_Init(void);

DMA_SER_Rettype DMA_SER_Start(DMA_UnitStruct * DMA_Unit);

BooleanLstType DMA_SER_WhetherComplete(DMA_Path_Lst DMA_Index);

#ifdef __cplusplus
	}
#endif
#endif /* DMA_SERVICES_H_ */
