/*
 * DMA_Services.c
 *
 *  Created on: Nov 10, 2015
 *      Author: Constantine
 */
#include "DMA_Services.h"



/* ==============================================================================================================================*
 *                                                 Local Functions declaration                                                   *
 * ==============================================================================================================================*/
STATIC BooleanLstType Local_IsIndexvalid(DMA_Path_Lst DMA_Index);








/* ==============================================================================================================================*
 *                                                       API Definitions                                                         *
 * ==============================================================================================================================*/
/*! Function to init all DMA channels
 *  This functions initialize DRIVER and NETWORK MANAGEMENT
 *  @Param  None
 *  @Return
 * */
DMA_SER_Rettype DMA_SER_Init(void)
{
	/* Initialize local variables */
	DMA_SER_Rettype ret_Value = DMA_SER_Fail;
	uint8 Expected_Progress_local = ZERO_8;
	uint8 Real_Progress_local = ZERO_8;

	/* Initialize the DMA Driver */
	Real_Progress_local 	+= (uint8)DMA_DRV_Init();
	Expected_Progress_local += (uint8)DMA_DRV_Success;

	/* Initialize the Network Management */
	Real_Progress_local 	+= (uint8)DMA_NM_Init();
	Expected_Progress_local += (uint8)DMA_DRV_Success;

	/* Check if all the initialization is finished */
	if(Real_Progress_local EQUAL_CON Expected_Progress_local)
	{
		ret_Value = DMA_SER_Succcessful;
	}
	else
	{
		ret_Value = DMA_SER_Fail;
	}


	return ret_Value;
}

/*! Function to start the specific DMA channel
 *  @param DMA_Unit: structure contains the wanted configuration for the DMA channel
 *  @return result of starting the channel: Successful or Fail
 *  --------------- Successful: The channel is started and running
 *  --------------- Fail: The channel cannot be started
 */
DMA_SER_Rettype DMA_SER_Start(DMA_UnitStruct * DMA_Unit)
{
	/* Init local variables */
	DMA_UnitStruct DMA_Unit_local = *DMA_Unit;
	DMA_SER_Rettype ret_val_local = DMA_SER_Fail;

	/* Check if the state of DMA channel is ready */
	if(DMA_NM_GetState(DMA_Unit) == DMA_DRV_State_Ready )
	{
		/* The channel is in READY state */
		DMA_DRV_SetDest(&DMA_Unit_local);
		DMA_DRV_SetSrc(&DMA_Unit_local);
		DMA_DRV_SetAmount(&DMA_Unit_local);
		DMA_NM_SetOccupied(&DMA_Unit_local);
		DMA_NM_SetTimeOut(&DMA_Unit_local);
		DMA_DRV_Start(&DMA_Unit_local);

		ret_val_local = DMA_SER_Succcessful;
	}
	else
	{
		/* The channel is not in READY state, cannot modify */
		ret_val_local = DMA_SER_Fail;
	}

	return ret_val_local;
}

/*! Function for polling check if the transaction on a channel is completed
 * @param  DMA_Index: the index of DMA channels that are being checked
 * @return BooleanLstType YES or NO
 * ------------------------ YES: the transaction finished
 * ------------------------- NO: transaction does not finished yet
 */
BooleanLstType DMA_SER_WhetherComplete(DMA_Path_Lst DMA_Index)
{

	/* Initialize return value to NO */
	BooleanLstType Ret_Value = Boolean_NO;

	/* Check the validity of index */
	if(Local_IsIndexvalid(DMA_Index) EQUAL_CON Boolean_YES)
	{
		/* Index is in range */

		/* Check the status */
		Ret_Value = DMA_NM_WhetherFinished(DMA_Index);
	}
	else
	{
		/* Index is invalid, keep the default value of NO */
	}

	/* End of function */
	return Ret_Value;
}

/*! Function for getting the amount of data that has been transferred
 *
 */
BooleanLstType DMA_SER_GetTransferredAmount(DMA_Path_Lst DMA_Index, uint16 * DMA_Num)
{
	/* Initialize return value with NO */
	BooleanLstType Ret_Result = Boolean_NO;

	/*Check the Index validity */
/* s*/
	if(Local_IsIndexvalid(DMA_Index) EQUAL_CON Boolean_YES)
	{
		/* Index is in range */

		/* Return the number of already transferred data */
		*DMA_Num = DMA_DRV_GetTransferredAmount(DMA_Index);

		/* Set return result to YES */
		Ret_Result = Boolean_YES;

	}
	else
	{
		/* Index is not in range, keep the default value of NO
		 * and set transferred amount to 0*/
		*DMA_Num = ZERO_16;
	}

	/* end of the function */
	return Ret_Result;
}


BooleanLstType DMA_SER_ChannelIsReady(DMA_Path_Lst DMA_Index)
{
	/* Initialize return value to NO */
	BooleanLstType Ret_value = Boolean_NO;
	DMA_UnitStruct Dummy;
	Dummy.PathId = DMA_Index;

	/* Check the state of DMA channel */
	if(DMA_NM_GetState(&Dummy) EQUAL_CON DMA_DRV_State_Ready)
	{
		/* DMA channel is in ready state, return YES */
		Ret_value = Boolean_YES;
	}
	else
	{
		/* DMA channel has not been ready yet */
		Ret_value = Boolean_NO;
	}

    return Ret_value;
}






/*======================================================================================================================*
 *                                                 LOCAL FUNCTION DEFINITIONS                                           *
 *======================================================================================================================*/




/*!
 * Function to check the validity of DMA channel index
 * @param DMA_Index: ID of the requested channel
 * @return BooleanLstType YES or NO
 * --------------------------YES: channel index is valid
 * ---------------------------NO: channel index is out of range
 *  */
STATIC BooleanLstType Local_IsIndexvalid(DMA_Path_Lst DMA_Index)
{
	BooleanLstType Ret_Value = Boolean_NO;

	if((DMA_Index < DMA_Path_01)
			OR_CON
	   (DMA_Index > DMA_Path_Max))
	{
		/* Do nothing since the provided index is out or range
		 * Keep the default value of NO							 */
	}
	else
	{
		/* Index is in range */
		Ret_Value = Boolean_YES;
	}

	return Ret_Value;
}
