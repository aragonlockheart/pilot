/*!
 * @file DMA_DRV.c
 *
 * @date Oct 18, 2015
 * @author Constantine
 */

#include "DMA_DRV.h"
#include "DMA_DRV_LCfg.h"
#include "DMA_DRV_PbCfg.h"


#define Mac_DMA_ChannelsEachFlagReg		((uint8) (4u))
#define Mac_DMA_ChannelFlagOffsetEach	((uint8) (6u))

uint32 DMA_DummyLocation;

uint16 DMA_NoOfTransfer_All[DMA_Path_Max];
uint16 DMA_ReMainTransfer_All[DMA_Path_Max];


/* Array of pointer to Flag registers of all DMA channels, written in Init stage */
Reg_DMA_Flag_Type_All 	* DMA_Flag_All[DMA_Path_Max];

/* Array of pointer to stream registers of all DMA channels, written in Init stage */
Reg_DMA_Stream_Type_All * DMA_Stream_All[DMA_Path_Max];


STRICT_INLINE STATIC Reg_DMA_Flag_Type_All * DMA_FlagGetPtr_local(DMA_Path_Lst DMA_Index);
STRICT_INLINE STATIC Reg_DMA_Stream_Type_All * DMA_StreamGetPtr_local(DMA_Path_Lst DMA_Index);
STRICT_INLINE STATIC BooleanLstType DMA_TurnOff_local(DMA_Path_Lst DMA_Index);
STRICT_INLINE STATIC BOOL DMA_TurnOn_local(Reg_DMA_Stream_Type_All * DMA_Specific);
STRICT_INLINE STATIC BOOL DMA_CleanALLFlags_local(DMA_Path_Lst DMA_Index);
STRICT_INLINE STATIC BOOL DMA_SetAmount_local(Reg_DMA_Stream_Type_All * DMA_Specific, uint16 NumberOfTransfer_arg);
STRICT_INLINE STATIC BOOL DMA_AnyFlagSet_local(DMA_Path_Lst DMA_Index);
STATIC BOOL DMA_StatusFlagMask_local(DMA_Path_Lst DMA_Index);

/*!
 *  Function to get the reference to hardware flag registers
 *  @param DMA_Unit specific structure, contains the unique ID for that DMA path
 *  @return A structure with type Reg_DMA_Flag_Type_All and contains all the flags
 */

STRICT_INLINE STATIC Reg_DMA_Flag_Type_All * DMA_FlagGetPtr_local(DMA_Path_Lst DMA_Index)
{
	/* Get the DMA controller of the requested channel */
	DMA_CtrlLst Local_DMACtrl = DMA_Cfg_Table[DMA_Index].Controller;

	/* Get the address of DMA base for requested channel */
	return (Reg_DMA_Flag_Type_All *) DMA_Base_Table[Local_DMACtrl];
}


/*!
 * 	Function to get the referencec to DMA stream registers
 * 	@param DMA_Index specific unique ID for the DMA path
 * 	@return a structure pointer that allows access to stream control registers.
 */
STRICT_INLINE STATIC Reg_DMA_Stream_Type_All * DMA_StreamGetPtr_local(DMA_Path_Lst DMA_Index)
{
	/* Declare local register */
	Reg_DMA_Stream_Type_All * ret_Ptr_local;
	RegType ResultAdd;
	RegType	StreamNum;
	RegType	Stream_Offset;
	DMA_CtrlLst Ctr_Index;


	/* Get the stream ID for the requested DMA channel */
	StreamNum = (RegType)(DMA_Cfg_Table[DMA_Index]).Stream;

	/* Get the controller index of DMA channel */
	Ctr_Index = DMA_Cfg_Table[DMA_Index].Controller;

	/* Get the offset to the control registers of the requested stream */
	Stream_Offset =	(DMA_Stream_Multiplier) MULTIPLY (StreamNum);

	/* Get the absolute address to the DM control register */
	ResultAdd =	((RegType)DMA_Base_Table[Ctr_Index]) + DMA_Stream_Base_Offset + Stream_Offset;

	ret_Ptr_local = (Reg_DMA_Stream_Type_All *) ResultAdd;

	/* return the address */
	return ret_Ptr_local;
}

STRICT_INLINE STATIC BooleanLstType DMA_TurnOff_local(DMA_Path_Lst DMA_Index)
{
	Reg_DMA_Stream_Type_All * DMA_Specific = DMA_Stream_All[DMA_Index];
	BooleanLstType ret_Result = Boolean_NO;

	if(DMA_Specific->CR.EN == DMA_Stream_CR_EN_ON)
	{
		DMA_Specific->CR.EN = DMA_Stream_CR_EN_OFF;
	}

	if(DMA_Specific->CR.EN == DMA_Stream_CR_EN_OFF)
	{
		ret_Result = Boolean_YES;
	}
	else
	{
	}

	return ret_Result;
}

STRICT_INLINE STATIC BOOL DMA_TurnOn_local(Reg_DMA_Stream_Type_All * DMA_Specific)
{
	BOOL ret_Result = EM_DISABLE;

	while(DMA_Specific->CR.EN == DMA_Stream_CR_EN_OFF)
	{
		DMA_Specific->CR.EN = DMA_Stream_CR_EN_ON;
	}

	if(DMA_Specific->CR.EN == DMA_Stream_CR_EN_ON)
	{
		ret_Result = EM_ENABLE;
	}
	else
	{
		ret_Result = EM_DISABLE;
	}

	return ret_Result;
}

STRICT_INLINE STATIC BOOL DMA_CleanALLFlags_local(DMA_Path_Lst DMA_Index)
{
	/* Declare local variables */
	BOOL ret_Result = EM_DISABLE;
	Reg_DMA_Flag_Type_All * DMA_Specific = DMA_Flag_All[DMA_Index];

	uint32 * DMA_HIFCR_ptr;
	uint32 * DMA_LIFCR_ptr;

	/* Get pointer to status registers */
	DMA_LIFCR_ptr = (uint32 * )(&(DMA_Specific->LIFCR));
	DMA_HIFCR_ptr = (uint32 * )(&(DMA_Specific->HIFCR));

	/* Reset all the flags */
	*(DMA_LIFCR_ptr) |= (DMA_ISR_Mask[DMA_Index].DMA_LISR);
	*(DMA_HIFCR_ptr) |= (DMA_ISR_Mask[DMA_Index].DMA_HISR);

	/* Check whether the flags were successfully reset or not */
	if(EM_ENABLE == DMA_AnyFlagSet_local(DMA_Index))
	{
		ret_Result = EM_DISABLE;
	}
	else
	{
		ret_Result = EM_ENABLE;
	}

	return ret_Result;
}

STRICT_INLINE STATIC BOOL DMA_SetAmount_local(Reg_DMA_Stream_Type_All * DMA_Specific, uint16 NumberOfTransfer_arg)
{
	BOOL ret_Result = EM_DISABLE;

	DMA_Specific->NDTR.NDT = NumberOfTransfer_arg;
	return ret_Result;
}

STRICT_INLINE STATIC BOOL DMA_AnyFlagSet_local(DMA_Path_Lst DMA_Index)
{
	/* initialize return value as DISABLE */
	BOOL ret_Status = EM_DISABLE;

	/* initialize local variable */
	RegType DMA_Drv_ORResult_local = ((RegType) ZERO_32);

	Reg_DMA_Flag_Type_All * DMA_Unit = DMA_Flag_All[DMA_Index];

	/* Get pointer to specific flag registers */
	RegType * Reg_HISR = (RegType *)(&(DMA_Unit->HISR));
	RegType * Reg_LISR = (RegType *)(&(DMA_Unit->LISR));

	/* Logical OR two registers to get any On flag */
	DMA_Drv_ORResult_local = ((*Reg_HISR)&(DMA_ISR_Mask[DMA_Index].DMA_HISR)) BW_OR ((*Reg_LISR)&(DMA_ISR_Mask[DMA_Index].DMA_LISR));

	/* Check flag condition */
	if(P_IsNotZero(DMA_Drv_ORResult_local))
	{
		/* Set return value as ENABLE since at least one flag is turned on */
		ret_Status = EM_ENABLE;
	}
	else
	{
		/* Set return value as DISABLE as all the flag is cleaned */
		ret_Status = EM_DISABLE;
	}

	return ret_Status;
}

STATIC BOOL DMA_StatusFlagMask_local(DMA_Path_Lst DMA_Index)
{
	BOOL ret_value = EM_DISABLE;
	uint32 Channel_Mask = (uint32)(0x3D);
	uint32 OffSet_02 = (((uint16)(DMA_Cfg_Table[DMA_Index].Stream % (0x04u)))/2u)*((uint16)(0x10u));

	uint32 Channel_Mask_Offset = ((uint16)(DMA_Cfg_Table[DMA_Index].Stream % 2u))*Mac_DMA_ChannelFlagOffsetEach + OffSet_02;

	Channel_Mask = Channel_Mask << Channel_Mask_Offset;

	if(DMA_Cfg_Table[DMA_Index].Stream < Mac_DMA_ChannelsEachFlagReg)
	{
		DMA_ISR_Mask[DMA_Index].DMA_HISR = ((uint32) ZERO);
		DMA_ISR_Mask[DMA_Index].DMA_LISR = Channel_Mask;
	}
	else
	{
		DMA_ISR_Mask[DMA_Index].DMA_LISR = ((uint32) ZERO);
		DMA_ISR_Mask[DMA_Index].DMA_HISR = Channel_Mask;
	}

	if(DMA_ISR_Mask[DMA_Index].DMA_HISR BW_OR DMA_ISR_Mask[DMA_Index].DMA_HISR)
	{
		ret_value = EM_ENABLE;
	}
	else {;}

	return ret_value;
}









DMA_Drv_Rettype DMA_DRV_Init(void)
{
	/* Initialize return value */
	DMA_Drv_Rettype 			ret_Result = DMA_DRV_Fail;
	/* Local DMA Unit struct to initialize registers */
	DMA_UnitStruct 				DMA_Unit;

	/* Temporary DMA stream to config parameters before transferring to real one*/
	Reg_DMA_Stream_Type_All		DMA_Stream_tmp;
	DMA_Path_Lst				DMA_Index = DMA_Path_01;
	RegType * Wanted_Value;
	RegType * Real_Value;



	/* Stop all the DMA streams */
	for(DMA_Index = DMA_Path_01; DMA_Index < DMA_Path_Max; DMA_Index++)
	{
		DMA_Unit.PathId = DMA_Index;

		/* Get pointer to flags registers */
		DMA_Flag_All[DMA_Index] = DMA_FlagGetPtr_local(DMA_Index);

		/* Get pointer to stream control registers */
		DMA_Stream_All[DMA_Index] = DMA_StreamGetPtr_local(DMA_Index);

		/* Get ISR Mask for the DMA channel */
		DMA_StatusFlagMask_local(DMA_Index);



		if  (
				(DMA_TurnOff_local(DMA_Index) == EM_ENABLE)	 	/* DMA channel is turned off successfully 			*/
			  AND_CON											 						/* AND 											*/
				(DMA_CleanALLFlags_local(DMA_Index)== EM_ENABLE) 	/* DMA channel's flags are cleaned successfully 	*/
			)
		{
			/* Initialize transferring address to dummy address */
			DMA_Stream_tmp.PAR.PA   = (RegType) &DMA_DummyLocation;
			DMA_Stream_tmp.M0AR.M0A = (RegType) &DMA_DummyLocation;

			/* Set data counter to zero */
			DMA_Stream_tmp.NDTR.NDT  = ZERO_32;
			DMA_Stream_tmp.FCR.DMDIS = ZERO_32;
			DMA_Stream_tmp.FCR.FEIE  = ZERO_32;
			DMA_Stream_tmp.FCR.FTH   = ZERO_32;

			/* Set configuration parameters on temporary variable*/
			DMA_Stream_tmp.CR.EN     = ZERO_32;
			DMA_Stream_tmp.CR.CHSEL  = (RegType)DMA_Cfg_Table[DMA_Index].Channel;
			DMA_Stream_tmp.CR.MBURST = ZERO_32;
			DMA_Stream_tmp.CR.PBURST = ZERO_32;
			DMA_Stream_tmp.CR.CT	 = ZERO_32;
			DMA_Stream_tmp.CR.DBM	 = ZERO_32;
			DMA_Stream_tmp.CR.PL	 = ZERO_32;
			DMA_Stream_tmp.CR.PINCOS = ZERO_32;
			DMA_Stream_tmp.CR.MSIZE  = ZERO_32;
			DMA_Stream_tmp.CR.PSIZE  = ZERO_32;
			DMA_Stream_tmp.CR.MINC   = (RegType)DMA_Cfg_Table[DMA_Index].MemIncr;
			DMA_Stream_tmp.CR.PINC   = (RegType)DMA_Cfg_Table[DMA_Index].PerIncr;
			DMA_Stream_tmp.CR.CIRC	 = ZERO_32;
			DMA_Stream_tmp.CR.DIR	 = (RegType)DMA_Cfg_Table[DMA_Index].Direction;
			DMA_Stream_tmp.CR.PFCTRL = ZERO_32;
			DMA_Stream_tmp.CR.TCIE	 = ZERO_32;
			DMA_Stream_tmp.CR.TEIE	 = ZERO_32;
			DMA_Stream_tmp.CR.HTIE	 = ZERO_32;
			DMA_Stream_tmp.CR.DMEIE  = ZERO_32;
			DMA_Stream_tmp.CR.BIT28  = ZERO_32;

			/* Assign value to real Hardware register */
			*DMA_Stream_All[DMA_Index] = DMA_Stream_tmp;

			Wanted_Value = (RegType *) &(DMA_Stream_tmp.CR);
			Real_Value	 = (RegType *) &(DMA_Stream_All[DMA_Index]->CR);

			/* Assining operation sucessful? */
			if(*Real_Value EQUAL_CON *Wanted_Value)
			{
				/* Set return value to success */
				ret_Result = DMA_DRV_Success;
			}
			else
			{
				/* Set return value to fail	*/
				ret_Result = DMA_DRV_Fail;
			}
		}
		else
		{
			/* Set return value to fail	*/
			ret_Result = DMA_DRV_Fail;
		}
	}

	return ret_Result;
}

DMA_Drv_Rettype DMA_DRV_Start(DMA_UnitStruct * DMA_Unit)
{
	/* local variable */
	Reg_DMA_Stream_Type_All * 	DMA_Stream;
	DMA_Drv_Rettype				ret_value;

	/* Get pointer address to dma stream */
	DMA_Stream = DMA_Stream_All[DMA_Unit->PathId];

	/* Enable the DMA stream */
	DMA_Stream->CR.EN = ONE_32;

	/* The DMA stream was turned on successfully? */
	if(DMA_Stream->CR.EN EQUAL_CON ONE_32)
	{
		ret_value = DMA_DRV_Success;
	}
	else
	{
		/* DMA stream is still OFF, return false */
		ret_value = DMA_DRV_Fail;
	}

	return ret_value;
}

DMA_Drv_Rettype DMA_DRV_Stop(DMA_UnitStruct * DMA_Unit)
{
	/* local variable */
	Reg_DMA_Stream_Type_All * 	DMA_Stream;
	DMA_Drv_Rettype				ret_value = DMA_DRV_Fail;

	/* Get pointer address to dma stream */
	DMA_Stream = DMA_Stream_All[DMA_Unit->PathId];

	/* Is turning off successful? */
	if (DMA_TurnOff_local(DMA_Unit->PathId) EQUAL_CON Boolean_YES)
	{
		ret_value = DMA_DRV_Success;
	}
	else
	{
		ret_value = DMA_DRV_Fail;
	}

	return ret_value;
}

DMA_Drv_Rettype DMA_DRV_SetAmount(DMA_UnitStruct * DMA_Unit)
{
	/* Default return value is Fail */
	DMA_Drv_Rettype ret_value_local = DMA_DRV_Fail;

	/* Set amount of transferred data */
	Reg_DMA_Stream_Type_All * Stream_ptr = DMA_Stream_All[DMA_Unit->PathId];
	Stream_ptr->NDTR.NDT = DMA_Unit->SE_DMA_NumOfTransfer;

	/* Check if the assigning succeeded or not */
	if(Stream_ptr->NDTR.NDT EQUAL_CON DMA_Unit->SE_DMA_NumOfTransfer)
	{
		DMA_NoOfTransfer_All[DMA_Unit->PathId] = DMA_Unit->SE_DMA_NumOfTransfer;
		ret_value_local = DMA_DRV_Success;
	}
	else
	{ /* assigning was not successful, return the default value of Fail */}

	return ret_value_local;
}

void DMA_DRV_SetTransferredAmount(DMA_Path_Lst DMA_Index)
{
	/* Get amount of remaining amount of transferred data */
	Reg_DMA_Stream_Type_All * Stream_ptr = DMA_Stream_All[DMA_Index];
	DMA_DRV_TransferredNoData[DMA_Index] = DMA_NoOfTransfer_All[DMA_Index] - (uint16)(Stream_ptr->NDTR.NDT);
}

uint16 DMA_DRV_GetTransferredAmount(DMA_Path_Lst DMA_Index)
{
	return DMA_DRV_TransferredNoData[DMA_Index];
}

DMA_Drv_Rettype DMA_DRV_SetReady(DMA_UnitStruct * DMA_Unit)
{
	DMA_Drv_Rettype ret_Value = DMA_DRV_Fail;
	if(DMA_TurnOff_local(DMA_Unit->PathId) EQUAL_CON Boolean_YES)
	{
		DMA_UnitStruct Dummy_DMA_Unit_local;

		Dummy_DMA_Unit_local.PathId = DMA_Unit->PathId;
		Dummy_DMA_Unit_local.SE_DMA_NumOfTransfer = (uint16)(ZERO_16);

		DMA_DRV_SetAmount(&Dummy_DMA_Unit_local);
		DMA_CleanALLFlags_local(Dummy_DMA_Unit_local.PathId);
		ret_Value = DMA_DRV_Success;
	}

	return ret_Value;

}

/* This function is used to get set the destination for DMA stream
 * The direction is pre-defined in the a table in platform
 */
DMA_Drv_Rettype DMA_DRV_SetDest(DMA_UnitStruct * DMA_Unit)
{
	/* Initialize return value as DMA_DRV_Success */
	DMA_Drv_Rettype ret_value_local = DMA_DRV_Success;

	/* Get information of the specific DMA path */
	DMA_DIRLst Direction = (DMA_DIRLst) DMA_Cfg_Table[DMA_Unit->PathId].Direction;
	Reg_DMA_Stream_Type_All * Stream_ptr = DMA_Stream_All[DMA_Unit->PathId];

	/* Base on the direction type, the corresponding registers are set */
	switch(Direction)
	{
	case DMA_DIR_Per2Mem:
		Stream_ptr->M0AR.M0A = DMA_Unit->Dest;
		break;
	case DMA_DIR_Mem2Mem:
		Stream_ptr->M0AR.M0A = DMA_Unit->Dest;
		break;
	case DMA_DIR_Mem2Per:
		Stream_ptr->PAR.PA = DMA_Unit->Dest;
		break;
	default:
		ret_value_local = DMA_DRV_Fail; /* The Unexpected happened, return fail since no register was set */
		break;
	}
	return ret_value_local;
}

/* This function is used to get set the source for DMA stream
 * The direction is predefined in the a table in platform
 */
DMA_Drv_Rettype DMA_DRV_SetSrc(DMA_UnitStruct * DMA_Unit)
{
	DMA_DIRLst Direction = (DMA_DIRLst) DMA_Cfg_Table[DMA_Unit->PathId].Direction;
	Reg_DMA_Stream_Type_All * Stream_ptr = DMA_Stream_All[DMA_Unit->PathId];

	switch(Direction)
	{
	case DMA_DIR_Per2Mem:
		Stream_ptr->PAR.PA = DMA_Unit->Src;
		break;
	case DMA_DIR_Mem2Mem:
		Stream_ptr->PAR.PA = DMA_Unit->Src;
		break;
	case DMA_DIR_Mem2Per:
		Stream_ptr->M0AR.M0A = DMA_Unit->Src;
		break;
	default:
		break;
	}
	return DMA_DRV_Success;
}

DMA_Drv_Statetype DMA_DRV_StateJugement(DMA_UnitStruct * DMA_Unit)
{
	/* Initialize retturn value */
	DMA_Drv_Statetype ret_State_local = DMA_DRV_State_Stop;

	/* Initialize conditions collecting register */
	volatile Union_uint8 DMA_DRV_Condition_local;
	DMA_DRV_Condition_local.WHOLE_08 = ZERO_8;

	/* Initialize local pointers */
	Reg_DMA_Stream_Type_All * stream_ptr_local = DMA_Stream_All[DMA_Unit->PathId];

	/* Check if the transmission counter is not zero */
	DMA_DRV_Condition_local.BIT_FIELD.BIT_0 = ((uint8) (P_IsNotZero(stream_ptr_local->NDTR.NDT)));
	/* Check if any current flag is switched on */
	DMA_DRV_Condition_local.BIT_FIELD.BIT_1 = ((uint8) DMA_AnyFlagSet_local(DMA_Unit->PathId));
	/* Check if the EN bit is set */
	DMA_DRV_Condition_local.BIT_FIELD.BIT_2 = ((uint8) stream_ptr_local->CR.EN);
	/*************************************************************************
	 *****				   Begin to check the state of DMA				  ****
	 *************************************************************************/

	/* Get the state from pre-defined truth table */
	ret_State_local = DMA_Cfg_State_Table[DMA_DRV_Condition_local.WHOLE_08];

	return ret_State_local;
}

RegType DMA_DRV_GetAmount(DMA_Path_Lst DMA_Index)
{
	return DMA_Stream_All[DMA_Index]->NDTR.NDT;
}
