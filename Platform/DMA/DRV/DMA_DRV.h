/*!\file DMA.h
*  \date Oct 16, 2015
*  \author Nguyen Trong Viet
*  \brief /*
*  \brief  * DMA.h
*  \brief  *
*  \brief  *  Created on: Oct 16, 2015
*  \brief  *      Author: Constantine
*  \brief  */


#ifndef DMA_H_
#define DMA_H_

#define DMA_DRV_STATE_MAXCASE  (8u)		/* Maximum cases can happen when assert conditions for state */

#ifdef __cplusplus
	extern "C" {
#endif

#include "stm32f4xx.h"
#include "stm32f4xx_Types.h"
#include "MCU_Reg_DMA.h"
#include "Platform_Types.h"
#include "Compiler.h"
typedef enum
{
	DMA_Path_01,	/*!0 */
	DMA_Path_02,	/*!1 */
	DMA_Path_Max	/*!2 */
}DMA_Path_Lst;

typedef enum
{
	DMA_DRV_Success,	/*!0 */
	DMA_DRV_Fail,		/*!1 */
	DMA_DRV_Busy,		/*!2 */
	DMA_DRV_Max			/*!3 */
} DMA_Drv_Rettype;

typedef enum
{
	DMA_DRV_State_Stop,			/*!0*/
	DMA_DRV_State_Ready,		/*!1*/
	DMA_DRV_State_InProgress,	/*!2*/
	DMA_DRV_State_Finished,		/*!3*/
	DMA_DRV_State_Undefined,	/*!4*/
	DMA_DRV_State_Max			/*!5*/
} DMA_Drv_Statetype;

typedef struct
{
	DMA_Path_Lst	PathId;
	RegType			Src;
	RegType			Dest;
	uint32			SE_DMA_Status;
	uint16			SE_DMA_NumOfTransfer;
	uint16			TimeOutThreshold;
} DMA_UnitStruct;


extern Reg_DMA_Flag_Type_All 	* DMA_Flag_All[DMA_Path_Max];
extern Reg_DMA_Stream_Type_All * DMA_Stream_All[DMA_Path_Max];
extern uint16 DMA_NoOfTransfer_All[DMA_Path_Max];
extern uint16 DMA_ReMainTransfer_All[DMA_Path_Max];

DMA_Drv_Rettype DMA_DRV_Init(void);

DMA_Drv_Rettype DMA_DRV_Start(DMA_UnitStruct * DMA_Unit);

DMA_Drv_Rettype DMA_DRV_Stop(DMA_UnitStruct * DMA_Unit);

void DMA_DRV_SetTransferredAmount(DMA_Path_Lst DMA_Index);

DMA_Drv_Rettype DMA_DRV_GetStat(DMA_UnitStruct * DMA_Unit);

DMA_Drv_Rettype DMA_DRV_IsTransCompl(DMA_UnitStruct * DMA_Unit);

DMA_Drv_Statetype DMA_DRV_StateJugement(DMA_UnitStruct * DMA_Unit);

DMA_Drv_Rettype DMA_DRV_SetDest(DMA_UnitStruct * DMA_Unit);

DMA_Drv_Rettype DMA_DRV_SetReady(DMA_UnitStruct * DMA_Unit);

DMA_Drv_Rettype DMA_DRV_SetSrc(DMA_UnitStruct * DMA_Unit);

RegType DMA_DRV_GetAmount(DMA_Path_Lst DMA_Index);

uint16 DMA_DRV_GetTransferredAmount(DMA_Path_Lst DMA_Index);

DMA_Drv_Rettype DMA_DRV_SetAmount(DMA_UnitStruct * DMA_Unit);

#ifdef __cplusplus
	}
#endif


#endif /* DMA_H_ */
