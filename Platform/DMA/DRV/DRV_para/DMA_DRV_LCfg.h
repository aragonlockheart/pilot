/*!\file DMA_DRV_LCfg.h
*  \date Jul 24, 2016
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef DMA_DRV_LCFG_H_
#define DMA_DRV_LCFG_H_

#include "DMA_DRV.h"

#define DMA_Path_Size_01			(uint16)(0x400)
#define DMA_Path_Size_02			(uint16)(0x200)

#define DMA_Stream_Base_Offset		(uint32)(0x10)
#define DMA_Stream_Multiplier		(uint32)(0x18)

#define DMA_Stream_CR_EN_OFF		(uint32)(0x00)
#define DMA_Stream_CR_EN_ON			(uint32)(0x01)

#define DMA_Flags_LISR_Clear		(uint32)(0x00)
#define DMA_Flags_HISR_Clear		(uint32)(0x00)
#define DMA_Flags_ClearALL			(uint32)(0xFFFFFFFF)

typedef enum
{
	DMA_Ctrl_01,		/*!0*/
	DMA_Ctrl_02,		/*!1*/
	DMA_Ctrl_Max		/*!2*/
} DMA_CtrlLst;

typedef enum
{
	DMA_Channel_00,		/*!0*/
	DMA_Channel_01,		/*!1*/
	DMA_Channel_02,		/*!2*/
	DMA_Channel_03,		/*!3*/
	DMA_Channel_04,		/*!4*/
	DMA_Channel_05,		/*!5*/
	DMA_Channel_06,		/*!6*/
	DMA_Channel_07,		/*!7*/
	DMA_Channel_Max		/*!8*/
} DMA_ChannelLst;

typedef enum
{
	DMA_Stream_00,		/*!0*/
	DMA_Stream_01,		/*!1*/
	DMA_Stream_02,		/*!2*/
	DMA_Stream_03,		/*!3*/
	DMA_Stream_04,		/*!4*/
	DMA_Stream_05,		/*!5*/
	DMA_Stream_06,		/*!6*/
	DMA_Stream_07,		/*!7*/
	DMA_Stream_Max		/*!8*/
} DMA_StreamLst;

typedef enum
{
	DMA_DIR_Per2Mem,		/*!0*/
	DMA_DIR_Mem2Per,		/*!1*/
	DMA_DIR_Mem2Mem,		/*!2*/
	DMA_DIR_Max				/*!3*/
} DMA_DIRLst;

typedef enum
{
	DMA_INC_NO,			/*!0*/
	DMA_INC_YES,		/*!1*/
	DMA_INC_MAX			/*!2*/
} DMA_INCLst;

typedef enum
{
	DMA_PathPar_Path,		/*!0 : DMA Path ID */
	DMA_PathPar_Ctrl,		/*!1 : DMA controller ID */
	DMA_PathPar_Channel,	/*!2 : DMA Channel */
	DMA_PathPar_Stream,		/*!3 : DMA Stream */
	DMA_PathPar_Dir,		/*!4 : DMA direction */
	DMA_PathPar_PerInc,		/*!5 : DMA Periph address increase */
	DMA_PathPar_MemInc,		/*!6 : DMA Memory address increase */
	DMA_PathPar_Max			/*!7 */
}DMA_PathParLst;

typedef struct
{
	DMA_Path_Lst 	PathID;
	DMA_CtrlLst  	Controller;
	DMA_ChannelLst	Channel;
	DMA_StreamLst	Stream;
	DMA_DIRLst		Direction;
	DMA_INCLst		PerIncr;
	DMA_INCLst		MemIncr;
}DMA_PbCfg_Struct;

typedef uint16	DMA_ExpireTimerType;

#ifdef __cplusplus
	extern "C" {
#endif

	/*Config table for all DMA channels*/
	const DMA_PbCfg_Struct DMA_Cfg_Table[DMA_Path_Max]=
	{
		[DMA_Path_01] = {DMA_Path_01	,	DMA_Ctrl_02	,	DMA_Channel_05	,	DMA_Stream_01	,	DMA_DIR_Per2Mem	,	DMA_INC_NO	,	DMA_INC_YES},
		[DMA_Path_02] = {DMA_Path_02	,	DMA_Ctrl_02	,	DMA_Channel_05	,	DMA_Stream_06	,	DMA_DIR_Mem2Per	,	DMA_INC_NO	,	DMA_INC_YES}
	};


	const RegType DMA_Base_Table[DMA_Ctrl_Max] =
	{
		[DMA_Ctrl_01] = (RegType) DMA1,
		[DMA_Ctrl_02] = (RegType) DMA2
	};

	const

	const DMA_Drv_Statetype DMA_Cfg_State_Table[DMA_DRV_STATE_MAXCASE] =
	{
		[0] = DMA_DRV_State_Ready,
		[1] = DMA_DRV_State_Stop,
		[2] = DMA_DRV_State_Finished,
		[3] = DMA_DRV_State_Stop,
		[4] = DMA_DRV_State_Undefined,
		[5] = DMA_DRV_State_InProgress,
		[6] = DMA_DRV_State_Undefined,
		[7] = DMA_DRV_State_InProgress
	};


#ifdef __cplusplus
	}
#endif
#endif /* DMA_DRV_LCFG_H_ */
