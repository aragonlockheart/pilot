/*!\file DMA_DRV_PbCfg.h
*  \date Jul 24, 2016
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef DMA_DRV_PBCFG_H_
#define DMA_DRV_PBCFG_H_



#ifdef __cplusplus
	extern "C" {
#endif

#include "DMA_DRV.h"

typedef struct
{
	RegIO RegType DMA_LISR;
	RegIO RegType DMA_HISR;
}DMA_ISR_Mask_Type;

uint16 DMA_DRV_TransferredNoData[DMA_Path_Max];
volatile DMA_ISR_Mask_Type DMA_ISR_Mask[DMA_Path_Max];


#ifdef __cplusplus
	}
#endif
#endif /* DMA_DRV_PBCFG_H_ */
