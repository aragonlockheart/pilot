/*!\file DMA_Nm_Lcfg.h
*  \date Jun 30, 2016
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef DMA_NM_LCFG_H_
#define DMA_NM_LCFG_H_

#include "DMA_Nm.h"

#ifdef __cplusplus
	extern "C" {
#endif

/* Configuration table for monitoring expire time
 * ---YES: the channel will be monitored for time out
 * ---NO : not monitored*/
const BooleanLstType DMA_ExpiTimCfg[DMA_Path_Max]=
{
	[DMA_Path_01] = Boolean_YES,
	[DMA_Path_02] = Boolean_NO
};

#ifdef __cplusplus
	}
#endif
#endif /* DMA_NM_LCFG_H_ */
