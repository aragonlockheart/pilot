/*!\file DMA_Nm_PbCfg.h
*  \date Jun 30, 2016
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef DMA_NM_PBCFG_H_
#define DMA_NM_PBCFG_H_

#include "DMA_Nm.h"

#ifdef __cplusplus
	extern "C" {
#endif

volatile uint16 		DMA_Nm_TimeOutBegin[DMA_Path_Max];

volatile BooleanLstType	DMA_Nm_FinishedFlag[DMA_Path_Max];

VAR(DMA_Drv_Statetype,STATIC) DMA_NM_State[DMA_Path_Max];

VAR(RegType,STATIC) DMA_NM_OldNoOfData[DMA_Path_Max];

volatile uint16 DMA_Nm_TimeOutThres[DMA_Path_Max];

VAR(uint8,STATIC) DMA_NM_CyclicIndex = (uint8) 0u;

volatile uint16 DMA_Nm_CommonTimer;

#ifdef __cplusplus
	}
#endif
#endif /* DMA_NM_PBCFG_H_ */
