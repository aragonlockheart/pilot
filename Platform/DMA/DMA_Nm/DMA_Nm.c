/*
 * DMA_Nm.c
 *
 *  Created on: Nov 7, 2015
 *      Author: Constantine
 */

#include "DMA_Nm.h"
#include "DMA_Nm_Lcfg.h"
#include "DMA_Nm_PbCfg.h"

#define DMA_NM_CYCLIC_PHASE1	((DMA_Path_Max >> (uint8) 1u) + ((uint8) 1u))
#define DMA_NM_CYCLIC_PHASE2	(DMA_Path_Max - DMA_NM_CYCLIC_PHASE1)

#define DMA_NM_CYCLIC_FIRST		0u
#define DMA_NM_CYCLIC_SECOND	1u


/*********************** Prototypes of local functions *******************************/
FUNC(void, STATIC) 				DMA_NM_StateSwitch_local(DMA_UnitStruct * DMA_Unit);

FUNC(BooleanLstType, STATIC) 	DMA_NM_TimeOutPolling_local(DMA_Path_Lst PathId);

FUNC(void, STATIC)				DMA_NM_DetectTransmission(DMA_Path_Lst DMA_Index);
/*************************************************************************************/


static BooleanLstType DMA_NM_TimeOutPolling_local(DMA_Path_Lst PathId)
{
	sint32 TempDiff_local;
	uint32 CommonTimerSnapshot_local;
	uint32 ChannelTimerSnapshot_local;
	BooleanLstType Ret_local;

	Ret_local = Boolean_NO;

	CommonTimerSnapshot_local  = (uint32)(DMA_Nm_CommonTimer);
	ChannelTimerSnapshot_local = (uint32)(DMA_Nm_TimeOutBegin[PathId]);

	TempDiff_local = (sint32)(CommonTimerSnapshot_local - ChannelTimerSnapshot_local);

	if(TempDiff_local < ZERO_32)
	{
		TempDiff_local = -(TempDiff_local);
	}
	else{};

	if(((uint32)TempDiff_local) > (uint32)DMA_Nm_TimeOutThres[PathId])
	{
		Ret_local = Boolean_YES;
	}
	else
	{
		Ret_local = Boolean_NO;
	}

	return Ret_local;
}




static void DMA_NM_StateSwitch_local(DMA_UnitStruct * DMA_Unit)
{
	DMA_Path_Lst DMA_PathID_local = DMA_Unit->PathId;
	DMA_Drv_Statetype DMA_State_local = DMA_NM_State[DMA_PathID_local];
	uint16 DMA_RemainAmount_local;
	switch(DMA_State_local)
	{
	case DMA_DRV_State_Stop:
		/* DMA channel is stopped mid-transmission */

		if(DMA_DRV_SetReady(DMA_Unit) EQUAL_CON DMA_DRV_Success)
		{
			DMA_State_local = DMA_DRV_State_Ready;
		}
		else
		{
			/* Keep the state since DMA channel cannot be put to ready state */
		}
		break;
	case DMA_DRV_State_Ready:
		/* Do nothing as the channel is ready */
		break;
	case DMA_DRV_State_InProgress:

		DMA_NM_DetectTransmission(DMA_PathID_local);

		if(Boolean_YES == DMA_ExpiTimCfg[DMA_PathID_local]
			AND_CON
		   Boolean_YES == DMA_NM_TimeOutPolling_local(DMA_PathID_local))
		{
			/* Stop the DMA channel since it is timed out */
			DMA_DRV_Stop(DMA_Unit);

			/* Record the number of transfer data */
			DMA_DRV_SetTransferredAmount(DMA_PathID_local);

			/* switch On the receiving flag */
			DMA_Nm_FinishedFlag[DMA_PathID_local] = Boolean_YES;

			/* Change state to STOP */
			DMA_State_local = DMA_DRV_State_Stop;
		}
		else { /* Do nothing as the channel is still running */}
		break;
	case DMA_DRV_State_Finished:

		/* Switch On the finished flag */
		DMA_Nm_FinishedFlag[DMA_PathID_local] = Boolean_YES;

		/* Get number of transferred data */
		DMA_DRV_SetTransferredAmount(DMA_PathID_local);

		/* The transfer is completed successfully, restart it to ready state */

		if(DMA_DRV_SetReady(DMA_Unit) EQUAL_CON DMA_DRV_Success)
		{
			DMA_State_local = DMA_DRV_State_Ready;
		}
		else
		{
			/* Keep the state since DMA channel cannot be restarted */
		}
		break;
	case DMA_DRV_State_Undefined:
		/* State of channel is unexpected, try to restart it */
		if(DMA_DRV_SetReady(DMA_Unit) EQUAL_CON DMA_DRV_Success)
		{
			DMA_State_local = DMA_DRV_State_Ready;
		}
		else
		{
			/* Keep the state since DMA channel cannot be restarted */
		}
		break;
	default:
		/* When the DMA state reach this point, there must be a problem,
		 * therefore set state to Undefined
		 */
		DMA_State_local = DMA_DRV_State_Undefined;
		break;
	}

	/* After switching state, assign it back to DMA NM */
	DMA_NM_State[DMA_PathID_local] = DMA_State_local;
}

FUNC(void, STATIC)	DMA_NM_DetectTransmission(DMA_Path_Lst DMA_Index)
{
	Reg_DMA_Stream_Type_All * Stream_ptr = DMA_Stream_All[DMA_Index];
	RegType Dma_CurrentNDTR = Stream_ptr->NDTR.NDT;

	/* Check if data transaction is going on or not */
	if(Dma_CurrentNDTR NOT_EQUAL_CON DMA_NM_OldNoOfData[DMA_Index])
	{/* Data transaction is going on */

		/* Reset the counter by getting new beginning point */
		DMA_Nm_TimeOutBegin[DMA_Index] = DMA_Nm_CommonTimer;

		/* Update the old image of current DMA counter */
		DMA_NM_OldNoOfData[DMA_Index] = Dma_CurrentNDTR;
	}
	else
	{/* Data transaction is not going on, do nothing */}
}

boolean DMA_NM_Init(void)
{
	BOOL ret_Result = EM_DISABLE;
	DMA_Path_Lst DMA_index;

	for(DMA_index = DMA_Path_01; DMA_index < DMA_Path_Max; DMA_index++ )
	{
		DMA_NM_State[DMA_index] = DMA_DRV_State_Undefined;
	}

	return ret_Result;
}


void DMA_NM_StateManager_Cyclic(void)
{
/* Initialize local variables */
DMA_Path_Lst DMA_index;
DMA_UnitStruct DMA_DummyStruct;

switch (DMA_NM_CyclicIndex)
{
	case DMA_NM_CYCLIC_FIRST:
		for(DMA_index = DMA_Path_01; DMA_index < DMA_NM_CYCLIC_PHASE1; DMA_index++)
		{
			DMA_DummyStruct.PathId = DMA_index;
			if(DMA_DRV_State_Stop != DMA_NM_State[DMA_index])
			{
				DMA_NM_State[DMA_index] = DMA_DRV_StateJugement(&DMA_DummyStruct);
			}
			else
			{
				/* Do nothing as the channel is being stopped */
			}
			DMA_NM_StateSwitch_local(&DMA_DummyStruct);
		}
		if(DMA_index SMALLERTHAN_CON DMA_Path_Max)
		{
			DMA_NM_CyclicIndex = DMA_NM_CYCLIC_SECOND;
		}
		else{}
		break;
	case DMA_NM_CYCLIC_SECOND:
		for(DMA_index = DMA_NM_CYCLIC_PHASE1; DMA_index < DMA_Path_Max; DMA_index++)
		{
			DMA_DummyStruct.PathId = DMA_index;
			if(DMA_DRV_State_Stop != DMA_NM_State[DMA_index])
			{
				DMA_DRV_StateJugement(&DMA_DummyStruct);
			}
			else
			{
				/* Do nothing as the channel is being stopped */
			}
			DMA_NM_StateSwitch_local(&DMA_DummyStruct);
		}
		DMA_NM_CyclicIndex = DMA_NM_CYCLIC_FIRST;
		break;
	default:
		break;
}

	DMA_NM_TimeOut_Cyclic();
}

DMA_Drv_Statetype DMA_NM_GetState(DMA_UnitStruct * DMA_Unit)
{
	DMA_Drv_Statetype DMA_State_local = DMA_NM_State[DMA_Unit->PathId];
	return DMA_State_local;
}

void DMA_NM_SetOccupied(DMA_UnitStruct * DMA_Unit)
{
	DMA_Path_Lst	Path_Id_local;
	Path_Id_local = DMA_Unit->PathId;
	/* Change current state of the DMA Path to occupied */
	DMA_NM_State[Path_Id_local] = DMA_DRV_State_InProgress;

	/* Clean the received flag */
	DMA_Nm_FinishedFlag[Path_Id_local] = Boolean_NO;
}

void DMA_NM_SetTimeOut(DMA_UnitStruct * DMA_Unit)
{
	DMA_Path_Lst DMA_Index = DMA_Unit->PathId;
	/*Reset the begin time */
	DMA_Nm_TimeOutBegin[DMA_Index] = DMA_Nm_CommonTimer;

	DMA_NM_OldNoOfData[DMA_Index] = DMA_DRV_GetAmount(DMA_Index);

	/* Set time out threshold for polling */
	DMA_Nm_TimeOutThres[DMA_Index] = DMA_Unit->TimeOutThreshold;

}

void DMA_NM_TimeOut_Cyclic(void)
{
	DMA_Nm_CommonTimer++;
}



BooleanLstType DMA_NM_WhetherFinished(DMA_Path_Lst DMA_Index)
{
	return DMA_Nm_FinishedFlag[DMA_Index];
}

