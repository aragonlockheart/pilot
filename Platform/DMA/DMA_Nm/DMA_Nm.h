/*!\file DMA_Nm.h
*  \date Nov 7, 2015
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef DMA_NM_H_
#define DMA_NM_H_

#include "DMA_DRV.h"



#ifdef __cplusplus
	extern "C" {
#endif


boolean DMA_NM_Init(void);

void DMA_NM_StateManager_Cyclic(void);

void DMA_NM_SetTimeOut(DMA_UnitStruct * DMA_Unit);
void DMA_NM_SetOccupied(DMA_UnitStruct * DMA_Unit);

DMA_Drv_Statetype DMA_NM_GetState(DMA_UnitStruct * DMA_Unit);

void DMA_NM_SetOccupied(DMA_UnitStruct * DMA_Unit);

void DMA_NM_SetTimeOut(DMA_UnitStruct * DMA_Unit);

void DMA_NM_TimeOut_Cyclic(void);

BooleanLstType DMA_NM_WhetherFinished(DMA_Path_Lst DMA_Index);

#ifdef __cplusplus
	}
#endif
#endif /* DMA_NM_H_ */
