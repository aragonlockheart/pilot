//Choen
#ifndef	PORT_H_
#define PORT_H_

#include	"stm32f4xx.h"
#include  	"Std_types.h"
#include    "Port_Cfg.h"
// ----- Port_ConfigType->PinNumber definitions -----
//#define PIN_0                 0
//#define PIN_1                 1
//#define PIN_2                 2
//#define PIN_3                 3
//#define PIN_4                 4
//#define PIN_5                 5
//#define PIN_6                 6
//#define PIN_7                 7
//#define PIN_8                 8
//#define PIN_9                 9
//#define PIN_10                10
//#define PIN_11                11
//#define PIN_12                12
//#define PIN_13                13
//#define PIN_14                14
//#define PIN_15                15
//----------------------------------------------------

// ----- Port_ConfigType->PortName definitions -----
//#define PORT_A								0
//#define PORT_B								1
//#define PORT_C								2
//#define PORT_D								3
//#define PORT_E								4
//#define PORT_F								5
//#define PORT_G								6
//#define PORT_H								7
//#define PORT_I								8
//--------------------------------------------------



#define	PORT_GETPORT(x)									((x & 0xF0) >> 4)
#define	PORT_GETPIN(x)									(x & 0x0F)

typedef enum {
	PORT_PIN_MODE_DIO, 
	PORT_PIN_MODE_SYSTEM, 												//	AF0	
	PORT_PIN_MODE_TIM_1_2,												//  AF1
	PORT_PIN_MODE_TIM_3_5,												//  AF2
	PORT_PIN_MODE_TIM_8_11,												//	AF3
	PORT_PIN_MODE_I2C_1_3,												//  AF4
	PORT_PIN_MODE_SPI_1_2,												//	AF5
	PORT_PIN_MODE_SPI_3,												//	AF6
	PORT_PIN_MODE_USART_1_3,											//	AF7
	PORT_PIN_MODE_USART_4_6,											//	AF8
	PORT_PIN_MODE_CAN_TIM_12_14,										//	AF9
	PORT_PIN_MODE_USB_OTG,												//	AF10
	PORT_PIN_MODE_ETH,													//	AF11
	PORT_PIN_MODE_FSMC_SDIO_OTG_HS,										//	AF12
	PORT_PIN_MODE_DCMI,													//	AF13
	PORT_PIN_MODE_EVENTOUT,												//	AF15
} Port_PinModeType;

typedef enum {
	PORT_PIN_IN,
	PORT_PIN_OUT,
	PORT_PIN_BIDIR
} Port_PinDirectionType;

typedef enum {
	PORT_PIN_LEVEL_LOW,
	PORT_PIN_LEVEL_HIGH
} Port_PinLevelValueType;


typedef struct {
	// specific
	uint8									PortPinName;
	uint8									PinHWType;
	uint8									PinHWPuPd;
	// standard
	Port_PinModeType						PinMode;
	Port_PinDirectionType					PinDirection;
	Port_PinLevelValueType					PinInitValue;
	uint8									PinRTDirChangeable;
	uint8									PinRTModChangeable;
} Port_ConfigType;

/* DIO ACCESS*/

typedef struct{
	uint16 						mask;
	uint8 						offset;
	uint8 						port;
}Port_PinGroupType;

#define PortConfig				User_PortConfig

extern const Port_ConfigType		User_PortConfig[];

/*----------------------------------------------------------------------------------------*/
/*function declaration*/
void Port_Init(const Port_ConfigType* ConfigPtr);
//void Port_SetPinDirection(Port_PinType Pin, Port_PinDirectionType Direction);
//void Port_RefreshPortDirection(void);
//void Port_SetPinMode(Port_PinType Pin, Port_PinModeType Mode);

Port_PinModeType Port_GetPinMode(const Port_ConfigType* ConfigPtr, uint8 PinName);
Port_PinDirectionType Port_GetPinDirection(const Port_ConfigType* ConfigPtr, uint8 PinName);

/* DIO ACCESS*/
void Port_WriteChannelGroup(const Port_PinGroupType* PinGroupIdPtr, uint16 Level);
void Port_WritePort(uint8 PortId, uint16 Level);
void Port_WritePin(uint8 PinID, uint8 Level);
uint16 Port_ReadPinGroup(const Port_PinGroupType* PinGroupIdPtr);
uint16 Port_ReadPort(uint8 PortId);
uint8 Port_ReadPin(uint8 PinID);

#endif	//PORT_H
