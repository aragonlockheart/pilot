//Choen
#ifndef	PORT_CFG_H
#define PORT_CFG_H

// ----- Port_ConfigType->PinHWType definitions -----
#define	PORT_PIN_HW_PUSHPULL 					0x00
#define	PORT_PIN_HW_OPENDRAIN  					0x01
//---------------------------------------------------

// ----- Port_ConfigType->PinHWPuPd definitions -----
#define	PORT_PIN_HW_PULL_UP  					0x01
#define	PORT_PIN_HW_PULL_DOWN  					0x02
#define	PORT_PIN_HW_NO_PUPD  					0x00
//---------------------------------------------------



	//-Port A definitions---------
#define	PORT_A_PIN_0										0x00
#define	PORT_A_PIN_1										0x01
#define	PORT_A_PIN_2										0x02
#define	PORT_A_PIN_3										0x03
#define	PORT_A_PIN_4										0x04
	// miss 5
#define	PORT_A_PIN_6										0x06
#define	PORT_A_PIN_7										0x07
#define	PORT_A_PIN_8										0x08
#define	PORT_A_PIN_9										0x09
#define	PORT_A_PIN_10										0x0A
	// miss 11, 12
#define	PORT_A_PIN_13										0x0D
#define	PORT_A_PIN_14										0x0E
#define	PORT_A_PIN_15										0x0F
	//----------------------------
	//-Port B definitions---------
#define	PORT_B_PIN_0										0x10
#define	PORT_B_PIN_1										0x11
#define	PORT_B_PIN_2										0x12
#define	PORT_B_PIN_3										0x13
#define	PORT_B_PIN_4										0x14
#define	PORT_B_PIN_5										0x15
#define	PORT_B_PIN_6										0x16
#define	PORT_B_PIN_7										0x17
#define	PORT_B_PIN_8										0x18
#define	PORT_B_PIN_9										0x19
#define	PORT_B_PIN_10										0x1A
#define	PORT_B_PIN_11										0x1B
#define	PORT_B_PIN_12										0x1C
#define	PORT_B_PIN_13										0x1D
#define	PORT_B_PIN_14										0x1E
#define	PORT_B_PIN_15										0x1F
	//----------------------------
	//-Port C definitions---------
#define	PORT_C_PIN_0										0x20
#define	PORT_C_PIN_1										0x21
#define	PORT_C_PIN_2										0x22
#define	PORT_C_PIN_3										0x23
#define	PORT_C_PIN_4										0x24
#define	PORT_C_PIN_5										0x25
#define	PORT_C_PIN_6										0x26
#define	PORT_C_PIN_7										0x27
#define	PORT_C_PIN_8										0x28
#define	PORT_C_PIN_9										0x29
#define	PORT_C_PIN_10										0x2A
#define	PORT_C_PIN_11										0x2B
#define	PORT_C_PIN_12										0x2C
#define	PORT_C_PIN_13										0x2D
#define	PORT_C_PIN_14										0x2E
#define	PORT_C_PIN_15										0x2F
	//----------------------------
	//-Port D definitions---------
#define	PORT_D_PIN_0										0x30
#define	PORT_D_PIN_1										0x31
#define	PORT_D_PIN_2										0x32
#define	PORT_D_PIN_3										0x33
#define	PORT_D_PIN_4										0x34
#define	PORT_D_PIN_5										0x35
#define	PORT_D_PIN_6										0x36
#define	PORT_D_PIN_7										0x37
#define	PORT_D_PIN_8										0x38
#define	PORT_D_PIN_9										0x39
#define	PORT_D_PIN_10										0x3A
#define	PORT_D_PIN_11										0x3B
#define	PORT_D_PIN_12										0x3C
#define	PORT_D_PIN_13										0x3D
#define	PORT_D_PIN_14										0x3E
#define	PORT_D_PIN_15										0x3F
	//----------------------------
	//-Port E definitions---------
#define	PORT_E_PIN_0										0x40
#define	PORT_E_PIN_1										0x41
#define	PORT_E_PIN_2										0x42
#define	PORT_E_PIN_3										0x43
#define	PORT_E_PIN_4										0x44
#define	PORT_E_PIN_5										0x45
#define	PORT_E_PIN_6										0x46
#define	PORT_E_PIN_7										0x47
#define	PORT_E_PIN_8										0x48
#define	PORT_E_PIN_9										0x49
#define	PORT_E_PIN_10										0x4A
#define	PORT_E_PIN_11										0x4B
#define	PORT_E_PIN_12										0x4C
#define	PORT_E_PIN_13										0x4D
#define	PORT_E_PIN_14										0x4E
#define	PORT_E_PIN_15										0x4F
	//----------------------------
	//-Port F definitions---------							0x5x
	//----------------------------
	//-Port G definitions---------							0x6x
	//----------------------------
	//-Port H definitions---------
#define	PORT_H_PIN_0										0x70
#define	PORT_H_PIN_1										0x71
	//----------------------------
#define	PORT_ENDFLAG										0xFF
/*--NUMBER OF PINS USED --*/


/*------------------------*/


#endif	//PORT_CFG_H
