//Choen

#include "Port.h"
#include "Port_Cfg.h"


/* local function declaration*/
void Port_SubInit( const Port_ConfigType* ConfigPtr);
GPIO_TypeDef * Port_BaseGetter(uint8 PortID);

	
// standard api [AUTOSAR compliance]------------------------------------------------------------

void Port_Init(const Port_ConfigType* ConfigPtr)
{
	uint8	l_index = 0;
	while (PORT_ENDFLAG != ConfigPtr[l_index].PortPinName)
	{
		Port_SubInit(&ConfigPtr[l_index]);
		l_index++;
	}
}
	
//void Port_SetPinDirection(Port_PinType Pin, Port_PinDirectionType Direction)
//{
//	// will be updated in future ...
//}
//
//void Port_RefreshPortDirection(void)
//{
//	// will be updated in future ...
//}
//
//void Port_SetPinMode(Port_PinType Pin, Port_PinModeType Mode)
//{
//	// will be updated in future ...
//}
//--------------------------------------------------------------------------	
// not standard functions [developing...]-----------------------------------
Port_PinModeType Port_GetPinMode(const Port_ConfigType* ConfigPtr, uint8 PinName)
{
	uint8	l_index = 0;
	while (PORT_ENDFLAG != ConfigPtr[l_index].PortPinName)
	{
		if (ConfigPtr[l_index].PortPinName == PinName)
		{
			return ConfigPtr[l_index].PinMode;
		}
		l_index++;
	}

	return 0xFF;								// no result
}

Port_PinDirectionType Port_GetPinDirection(const Port_ConfigType* ConfigPtr, uint8 PinName)
{
	uint8	l_index = 0;
	while (PORT_ENDFLAG != ConfigPtr[l_index].PortPinName)
	{
		if (ConfigPtr[l_index].PortPinName == PinName)
		{
			return ConfigPtr[l_index].PinDirection;
		}
		l_index++;
	}

	return 0xFF;								// no result
}
//--------------------------------------------------------------------------

/*
 * FOR DIO COMPONENT ACCESS
 *
 *
 *
 */
uint8 Port_ReadPin(uint8 PinID)
{
	uint8 l_PinNum = PORT_GETPIN(PinID);

	GPIO_TypeDef *l_Port_Handler = Port_BaseGetter(PORT_GETPORT(PinID));

	uint16 l_PinValue = 0xFFFF;		//initialized with error code.

	l_PinValue = (uint16) l_Port_Handler->IDR;

	if ( l_PinValue & ((uint16)(0x0001 << l_PinNum)))
	{
		return STD_HIGH;
	}
	else
	{
		return STD_LOW;
	}
}

/*	Note: ignored pin mode and direction checking due to optimize execute time.
 *
 *
 *
 */
uint16 Port_ReadPort(uint8 PortId)
{
	uint16 l_PortValue = 0xFFFF;

	GPIO_TypeDef *l_Port_Handler = Port_BaseGetter((PortId & 0x0F));

	l_PortValue = (uint16) l_Port_Handler->IDR;

	return l_PortValue;

}

/*	Note: ignored pin mode and direction checking due to optimize execute time.
 *  Offset will shift everything to right.
 *  currently, the requirement is not clear.
 *  pending to next discussion.
 */
uint16 Port_ReadPinGroup(const Port_PinGroupType* PinGroupIdPtr)
{
	uint16 l_PinGroupValue = 0xFFFF;

	uint16 l_mask = PinGroupIdPtr->mask;

	uint8 l_offset = PinGroupIdPtr->offset;

	GPIO_TypeDef *l_Port_Handler = Port_BaseGetter((PinGroupIdPtr->port & 0x0F));

	l_PinGroupValue = (uint16) l_Port_Handler->IDR;

	l_mask = l_mask << l_offset;

	l_PinGroupValue &= l_mask;

	l_PinGroupValue = l_PinGroupValue >> l_offset;
	
	return l_PinGroupValue;
}

/*	Note: defect may comes because we define Dio_LevelType has only 2 values 0 and 1, other values will be belong
 * to STD_HIGH
 * Consider to use BSRR register
 * updated: currently use BSRR instead.
 *
 */
void Port_WritePin(uint8 PinID, uint8 Level)
{
	uint8 l_Level = Level;

	uint8 l_PinNum = PORT_GETPIN(PinID);

	GPIO_TypeDef *l_Port_Handler = Port_BaseGetter(PORT_GETPORT(PinID));

	if (STD_LOW == l_Level)
	{
//		l_Port_Handler->ODR &= ~((uint16)(0x0001 << l_PinNum));
		l_Port_Handler->BSRRH |= (uint16)(0x0001 << l_PinNum);
	}
	else
	{
//		l_Port_Handler->ODR |= (uint16)(0x0001 << l_PinNum);
		l_Port_Handler->BSRRL |= (uint16)(0x0001 << l_PinNum);
	}

}

/* Note:
 *
 *
 *
 */
void Port_WritePort(uint8 PortId, uint16 Level)
{
	uint16 l_Level = Level;


	GPIO_TypeDef *l_Port_Handler = Port_BaseGetter((PortId & 0x0F));

	l_Port_Handler->ODR = (uint16)l_Level;
}

/* Note:
 * currently, the requirement is not clear.
 * pending to next discussion.
 *
 */
void Port_WriteChannelGroup(const Port_PinGroupType* PinGroupIdPtr, uint16 Level)
{
	uint16 l_PortValue = 0xFFFF;

	uint16 l_PinGroupValue = Level;

	uint16 l_mask = PinGroupIdPtr->mask;

	uint8 l_offset = PinGroupIdPtr->offset;

	GPIO_TypeDef *l_Port_Handler = Port_BaseGetter((PinGroupIdPtr->port & 0x0F));

	l_PortValue = l_Port_Handler->ODR;

	l_mask = l_mask << l_offset;

	l_PinGroupValue = l_PinGroupValue << l_offset;

	l_PinGroupValue &= l_mask;

	l_PortValue &= (~l_mask);

	l_PortValue |= l_PinGroupValue;

	l_Port_Handler->ODR = l_PortValue;

}


// private function for local using-----------------------------------------
GPIO_TypeDef * Port_BaseGetter(uint8 PortID)
	{
		GPIO_TypeDef * l_Port_Base;
		switch (PortID)
		{
			case 0:	
				// Port A matched.
				l_Port_Base = (GPIO_TypeDef*) GPIOA_BASE;
				break;
			case 1:
				// Port B matched.
				l_Port_Base = (GPIO_TypeDef*) GPIOB_BASE;
				break;
			case 2:
				// Port C matched.
				l_Port_Base = (GPIO_TypeDef*) GPIOC_BASE;
				break;
			case 3:
				// Port D matched.
				l_Port_Base = (GPIO_TypeDef*) GPIOD_BASE;
				break;
			case 4:
				// Port E matched.
				l_Port_Base = (GPIO_TypeDef*) GPIOE_BASE;
				break;
			default:
				break;
		}
		return l_Port_Base;
	}
	
void Port_SubInit( const Port_ConfigType* ConfigPtr)
{
	uint8 l_PinNum = PORT_GETPIN(ConfigPtr->PortPinName);
	GPIO_TypeDef *l_Port_Handler = Port_BaseGetter(PORT_GETPORT(ConfigPtr->PortPinName));
	
	// Reset all 
	l_Port_Handler->MODER &= ~((0x00000003) << (l_PinNum*2));
	l_Port_Handler->OTYPER &= ~((0x0001) << l_PinNum);
	l_Port_Handler->PUPDR &= ~((0x00000003) << (l_PinNum*2));
	l_Port_Handler->OSPEEDR &= ~((0x00000003) << (l_PinNum*2));
	
	switch (ConfigPtr->PinMode)
	{
		/*--------PORT_PIN_MODE_DIO INITIALIZATION-----------------------------*/
		case PORT_PIN_MODE_DIO:
			if (PORT_PIN_OUT == ConfigPtr->PinDirection)
			{
				l_Port_Handler->MODER |= ((0x00000001) << (l_PinNum*2));
				l_Port_Handler->OTYPER |= ((ConfigPtr->PinHWType) << l_PinNum);
				l_Port_Handler->OSPEEDR |= ((0x00000003) << (l_PinNum*2));
				
				/* Set init level for output type DIO*/
				if (PORT_PIN_LEVEL_LOW == ConfigPtr->PinInitValue)
				{
					l_Port_Handler->BSRRH |= ((0x0001) << l_PinNum);
				}
				else
				{
					l_Port_Handler->BSRRL |= ((0x0001) << l_PinNum);
				}
			}
			else
			{
				// reset state, no configuration needed.
			}
			break;
		/*--------PORT_PIN_MODE_USART_1_3 INITIALIZATION-----------------------------*/
		/*--------NOT TESTED YET-----------------------------------------------------*/
		case PORT_PIN_MODE_USART_1_3:
			if (PORT_PIN_OUT == ConfigPtr->PinDirection)
			{
				l_Port_Handler->MODER |= ((0x00000002) << (l_PinNum*2));
				l_Port_Handler->OTYPER |= ((ConfigPtr->PinHWType) << l_PinNum);
				l_Port_Handler->OSPEEDR |= ((0x00000003) << (l_PinNum*2));
				
				l_Port_Handler->AFR[l_PinNum >> 0x03] &= ~(0x0000000F << ((l_PinNum & 0x07)*4));
				l_Port_Handler->AFR[l_PinNum >> 0x03] |= (0x00000007	<< ((l_PinNum & 0x07)*4));
			}
			else
			{
				l_Port_Handler->MODER |= ((0x00000002) << (l_PinNum*2));
				
				l_Port_Handler->AFR[l_PinNum >> 0x03] &= ~(0x0000000F << ((l_PinNum & 0x07)*4));
				l_Port_Handler->AFR[l_PinNum >> 0x03] |= (0x00000007	<< ((l_PinNum & 0x07)*4));
			}
			break;
		/*--------PORT_PIN_MODE_SPI_1_2 INITIALIZATION-------------------------------*/
		/*--------NOT TESTED YET-----------------------------------------------------*/
		case PORT_PIN_MODE_SPI_1_2:										
			if (PORT_PIN_OUT == ConfigPtr->PinDirection)
			{
				l_Port_Handler->MODER |= ((0x00000002) << (l_PinNum*2));
				l_Port_Handler->OTYPER |= ((ConfigPtr->PinHWType) << l_PinNum);
				l_Port_Handler->OSPEEDR |= ((0x00000003) << (l_PinNum*2));
				
				l_Port_Handler->AFR[l_PinNum >> 0x03] &= ~(0x0000000F << ((l_PinNum & 0x07)*4));
				l_Port_Handler->AFR[l_PinNum >> 0x03] |= (0x00000005	<< ((l_PinNum & 0x07)*4));
			}
			else
			{
				l_Port_Handler->MODER |= ((0x00000002) << (l_PinNum*2));
				
				l_Port_Handler->AFR[l_PinNum >> 0x03] &= ~(0x0000000F << ((l_PinNum & 0x07)*4));
				l_Port_Handler->AFR[l_PinNum >> 0x03] |= (0x00000005	<< ((l_PinNum & 0x07)*4));
			}
			break;
		/*--------PORT_PIN_MODE_SPI_3 INITIALIZATION---------------------------------*/
		/*--------NOT TESTED YET-----------------------------------------------------*/
		case PORT_PIN_MODE_SPI_3:
			if (PORT_PIN_OUT == ConfigPtr->PinDirection)
			{
				l_Port_Handler->MODER |= ((0x00000002) << (l_PinNum*2));
				l_Port_Handler->OTYPER |= ((ConfigPtr->PinHWType) << l_PinNum);
				l_Port_Handler->OSPEEDR |= ((0x00000003) << (l_PinNum*2));
				
				l_Port_Handler->AFR[l_PinNum >> 0x03] &= ~(0x0000000F << ((l_PinNum & 0x07)*4));
				l_Port_Handler->AFR[l_PinNum >> 0x03] |= (0x00000006	<< ((l_PinNum & 0x07)*4));
			}
			else
			{
				l_Port_Handler->MODER |= ((0x00000002) << (l_PinNum*2));
				
				l_Port_Handler->AFR[l_PinNum >> 0x03] &= ~(0x0000000F << ((l_PinNum & 0x07)*4));
				l_Port_Handler->AFR[l_PinNum >> 0x03] |= (0x00000006	<< ((l_PinNum & 0x07)*4));
			}
			break;
		case PORT_PIN_MODE_I2C_1_3:
		{
			l_Port_Handler->MODER |= ((0x00000002) << (l_PinNum*2));
			l_Port_Handler->OTYPER |= ((ConfigPtr->PinHWType) << l_PinNum);
			l_Port_Handler->OSPEEDR |= ((0x00000003) << (l_PinNum*2));

			l_Port_Handler->AFR[l_PinNum >> 0x03] &= ~(0x0000000F << ((l_PinNum & 0x07)*4));
			l_Port_Handler->AFR[l_PinNum >> 0x03] |= (0x00000004	<< ((l_PinNum & 0x07)*4));
		}
		break;
		// will be updated in future ...
		default:
			break;	
	}
	
	// Configure PUPDR : base on PinHWPuPd
	l_Port_Handler->PUPDR |= ((ConfigPtr->PinHWPuPd) << (l_PinNum*2));
}
