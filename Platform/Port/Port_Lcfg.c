//Choen

#include "Port.h"
#include "I2C_Cfg.h"
/* reference*/
//	uint8									PortPinName;
//	uint8									PinHWType;
//	uint8									PinHWPuPd;
//	// standard
//	Port_PinModeType						PinMode;
//	Port_PinDirectionType					PinDirection;
//	Port_PinLevelValueType					PinInitValue;
//	uint8									PinRTDirChangeable;
//	uint8

const Port_ConfigType		User_PortConfig[] =
{
/*------THE BEGIN OF USER EDIT SECTION---------*/
	{
		PORT_D_PIN_12,								// LED GREEN
		PORT_PIN_HW_PUSHPULL,
		PORT_PIN_HW_NO_PUPD,
		
		PORT_PIN_MODE_DIO,
		PORT_PIN_OUT,
		PORT_PIN_LEVEL_LOW,
		FALSE,
		FALSE		
	},
	
	{
		PORT_D_PIN_13,								// LED ORANGE
		PORT_PIN_HW_PUSHPULL,
		PORT_PIN_HW_NO_PUPD,

		PORT_PIN_MODE_DIO,
		PORT_PIN_OUT,
		PORT_PIN_LEVEL_HIGH,
		FALSE,
		FALSE
	},

	{
		PORT_D_PIN_14,								// LED LED
		PORT_PIN_HW_PUSHPULL,
		PORT_PIN_HW_NO_PUPD,

		PORT_PIN_MODE_DIO,
		PORT_PIN_OUT,
		PORT_PIN_LEVEL_LOW,
		FALSE,
		FALSE
	},

	{
		PORT_D_PIN_15,								// LED BLUE
		PORT_PIN_HW_PUSHPULL,
		PORT_PIN_HW_NO_PUPD,
		
		PORT_PIN_MODE_DIO,
		PORT_PIN_OUT,
		PORT_PIN_LEVEL_LOW,
		FALSE,
		FALSE		
	},



#if (FS_I2C_CHANNEL_1_ENABLED == I2C_FS_ENABLED)
	/* I2C channel 1*/
	{
		I2C_SCL_PIN_CHNL_1,								// I2C
		PORT_PIN_HW_OPENDRAIN,
		PORT_PIN_HW_PULL_UP,

		PORT_PIN_MODE_I2C_1_3,
		PORT_PIN_BIDIR,
		PORT_PIN_LEVEL_HIGH,
		FALSE,
		FALSE
	},

	{
		I2C_SDA_PIN_CHNL_1,								// I2C
		PORT_PIN_HW_OPENDRAIN,
		PORT_PIN_HW_PULL_UP,

		PORT_PIN_MODE_I2C_1_3,
		PORT_PIN_BIDIR,
		PORT_PIN_LEVEL_HIGH,
		FALSE,
		FALSE
	},
#endif

#if (FS_I2C_CHANNEL_2_ENABLED == I2C_FS_ENABLED)
	/* I2C channel 2*/
	{
		I2C_SCL_PIN_CHNL_2,								// I2C
		PORT_PIN_HW_OPENDRAIN,
		PORT_PIN_HW_PULL_UP,

		PORT_PIN_MODE_I2C_1_3,
		PORT_PIN_BIDIR,
		PORT_PIN_LEVEL_HIGH,
		FALSE,
		FALSE
	},

	{
		I2C_SDA_PIN_CHNL_2,								// I2C
		PORT_PIN_HW_OPENDRAIN,
		PORT_PIN_HW_PULL_UP,

		PORT_PIN_MODE_I2C_1_3,
		PORT_PIN_BIDIR,
		PORT_PIN_LEVEL_HIGH,
		FALSE,
		FALSE
	},
#endif
#if (FS_I2C_CHANNEL_3_ENABLED == I2C_FS_ENABLED)
	/* I2C channel 3*/
	{
		I2C_SCL_PIN_CHNL_3,								// I2C
		PORT_PIN_HW_OPENDRAIN,
		PORT_PIN_HW_PULL_UP,

		PORT_PIN_MODE_I2C_1_3,
		PORT_PIN_BIDIR,
		PORT_PIN_LEVEL_HIGH,
		FALSE,
		FALSE
	},

	{
		I2C_SDA_PIN_CHNL_3,								// I2C
		PORT_PIN_HW_OPENDRAIN,
		PORT_PIN_HW_PULL_UP,

		PORT_PIN_MODE_I2C_1_3,
		PORT_PIN_BIDIR,
		PORT_PIN_LEVEL_HIGH,
		FALSE,
		FALSE
	},
#endif
/*------THE END OF USER EDIT SECTION---------*/
	{
		PORT_ENDFLAG,
		0xFF,
		0xFF,
		
		0xFF,
		0xFF,
		0xFF,
		0xFF,
		0xFF		
	}
};


