/*
 * Tm.c
 *
 *  Created on: May 18, 2017
 *      Author: Tom
 */

#include "Tm.h"

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                     Private variable			                                                          |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*                                                                                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/






/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                     Local functions declaration                                                          |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*                                                                                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

















/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                     Global API definition                                                                |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*                                                                                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/



/*+-------------------------------------[   Tm_ResetTimer1us16bit    ]---------------------------------------+
  |                                                                                                          |
  | brief 	    Reset time reference to the current value of HW counter.                                     |
  |             It does NOT reset the HW counter                                                             |
  |                                                                                                          |
  | parameter 	TimerPtr : variable that hold the time reference		                                     |
  | return       None										                                                 |
  |                                                                                                        	 |
  +----------------------------------------------------------------------------------------------------------+*/
Std_ReturnType Tm_ResetTimer1us16bit(Tm_PredefTimer1us16bitType* TimerPtr)
{
	/* Call to the lower level */
	return Gpt_GetPredefTimerValue(GPT_PREDEF_TIMER_1US_16BIT, &(TimerPtr->Timepreference));
}

/*+-------------------------------------[   Tm_GetTimeSpan1us16bit    ]--------------------------------------+
  |                                                                                                          |
  | brief 	    get the time span bettween current time and the last time reference                          |
  |                                                                                                          |
  |                                                                                                          |
  | parameter 	TimerType_arg : time reference	                        	                                 |
  | parameter	TimeValuePtr_arg: place to hold time span                                                    |
  | return       None										                                                 |
  |                                                                                                          |
  | @notice     the limit of this function is 650000 us or 65ms                                              |                              	 |
  +----------------------------------------------------------------------------------------------------------+*/
Std_ReturnType Tm_GetTimeSpan1us16bit(const Tm_PredefTimer1us16bitType* TimerPtr,uint16* TimeSpanPtr)
{
	/* Init local variable */
	uint32 TimerValue_local;

	/* Get the timer value */
	Gpt_GetPredefTimerValue(GPT_PREDEF_TIMER_1US_16BIT,&TimerValue_local);

	/* Get the hardware time from the lower layer */
	*TimeSpanPtr = MAX_uint16 - ( (TimerPtr->Timepreference) - TimerValue_local);
	return E_OK;
}


/*+-------------------------------------[   Tm_ResetTimer1us32bit    ]---------------------------------------+
  |                                                                                                          |
  | brief 	    Reset time reference to the current value of HW counter.                                     |
  |             It does NOT reset the HW counter                                                             |
  |                                                                                                          |
  | parameter 	TimerPtr : variable that hold the time reference		                                     |
  | return       None										                                                 |
  |                                                                                                        	 |
  +----------------------------------------------------------------------------------------------------------+*/
Std_ReturnType Tm_ResetTimer1us32bit(Tm_PredefTimer1us32bitType* TimerPtr)
{
	/* Call to the lower level */
	return Gpt_GetPredefTimerValue(GPT_PREDEF_TIMER_1US_32BIT, &(TimerPtr->Timepreference));
}


/*+-------------------------------------[   Tm_GetTimeSpan1us16bit    ]--------------------------------------+
  |                                                                                                          |
  | brief 	    get the time span bettween current time and the last time reference                          |
  |                                                                                                          |
  |                                                                                                          |
  | parameter 	TimerType_arg : time reference	                        	                                 |
  | parameter	TimeValuePtr_arg: place to hold time span                                                    |
  | return       None										                                                 |
  |                                                                                                        	 |
  | @notice     the limit of this function is 1 hour                                                         |
  +----------------------------------------------------------------------------------------------------------+*/
Std_ReturnType Tm_GetTimeSpan1us32bit(const Tm_PredefTimer1us32bitType* TimerPtr,uint32* TimeSpanPtr)
{
	/* Init local variable */
	uint32 TimerValue_local;

	/* Get the timer value */
	Gpt_GetPredefTimerValue(GPT_PREDEF_TIMER_1US_32BIT,&TimerValue_local);

	/* Get the hardware time from the lower layer */
	*TimeSpanPtr = MAX_uint32 - ( (TimerPtr->Timepreference) - TimerValue_local);
	return E_OK;
}












/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                     Local functions definition                                                           |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*                                                                                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
