/*
 * Tm_Cfg.h
 *
 *  Created on: May 18, 2017
 *      Author: Tom
 */

#ifndef PLATFORM_TIMER_TM_TM_CFG_H_
#define PLATFORM_TIMER_TM_TM_CFG_H_

/* AUTOSAR SWS_Tm_00032*/
typedef struct
{
	uint32 Timepreference;
}Tm_PredefTimer1us16bitType;


/* AUTOSAR SWS_Tm_00034*/
typedef struct
{
	uint32 Timepreference;
}Tm_PredefTimer1us32bitType;



#endif /* PLATFORM_TIMER_TM_TM_CFG_H_ */
