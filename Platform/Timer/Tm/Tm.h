/*
 * Tm.h
 *
 *  Created on: May 18, 2017
 *      Author: Tom
 */

#ifndef PLATFORM_TIMER_TM_TM_H_
#define PLATFORM_TIMER_TM_TM_H_

#include "Std_Types.h"
#include "Gpt.h"
#include "Tm_Cfg.h"

#ifdef __cplusplus
	extern "C" {
#endif

Std_ReturnType Tm_ResetTimer1us16bit(Tm_PredefTimer1us16bitType* TimerPtr);
Std_ReturnType Tm_GetTimeSpan1us16bit(const Tm_PredefTimer1us16bitType* TimerPtr,uint16* TimeSpanPtr);

Std_ReturnType Tm_ResetTimer1us32bit(Tm_PredefTimer1us32bitType* TimerPtr);
Std_ReturnType Tm_GetTimeSpan1us32bit(const Tm_PredefTimer1us32bitType* TimerPtr,uint32* TimeSpanPtr);
#ifdef __cplusplus
	}
#endif




#endif /* PLATFORM_TIMER_TM_TM_H_ */
