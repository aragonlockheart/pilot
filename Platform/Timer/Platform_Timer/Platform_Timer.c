/*
 * Platform_Timer.c
 *
 *  Created on: May 1, 2015
 *      Author: Viet
 */
#include "Platform_timer.h"
#include "Platform_Timer_Cfg.h"
#include "Std_Types.h"




/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                     Private variable			                                                          |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*                                                                                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/* module's private variable to control the usage  of hardware timer, avoid the overlap in using timer */
uint32 TimerUsedCheckedLst = (uint32)(0u);
















/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                     Local functions declaration                                                          |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*                                                                                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
BooleanLstType Platform_Timer_local_IsOccupied(TIMER_USAGE_ENUM UsageType);
BooleanLstType Platform_Timer_local_IsInRange(TIMER_USAGE_ENUM UsageType);






/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                     Global API definition                                                                |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*                                                                                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/



/*==============================|              Platform_timer_GetPtr          |============================|
 *                                                                                                         |
 * brief 	    Get pointer to the corresponding timer address                                             |
 * parameter 	UsageType the reason of using the hardware timer                                           |
 * ReqPtr       The location where timer address will be put 											   |
 * 							             ---------------- ZERO: fail, cannot locate address                |
 * 										 ---------------- Diff from zero: the timer's address              |
 * return       BooleanLstType                                                                             |
 * 				    ---------- YES: timer adddress is located                                              |
 * 				    ---------- NO: fail to get timer address                                               |
 *                                                                                                         |
 ==========================================================================================================*/
BooleanLstType Platform_timer_GetPtr(uint32 * ReqPtr, TIMER_USAGE_ENUM UsageType)
{
	/* Init return value to NO */
	BooleanLstType Ret_val = Boolean_NO;

	TIM_TypeDef * Debug_pointer = TIM4;

	/* Check if the parameter in range */
	if(Platform_Timer_local_IsInRange(UsageType) EQUAL_CON Boolean_YES)
	{
		/* Get the address of timer */
		(*ReqPtr) = (uint32) TIMER_CFG_TABLE[UsageType];

		/* Set return to YES */
		Ret_val = Boolean_YES;
	}
	else
	{
		if(Debug_pointer != 0x00u)
		{
		/* The request is invalid, return no */
		Ret_val = Boolean_NO;
		}
	}

	return Ret_val;


}





/*==============================|            Platform_Timer_Register          |============================|
 *                                                                                                         |
 * brief 	    Register that the timer is in use                                                          |
 * parameter 	UsageType the reason of using the hardware timer                                           |
 * return       BooleanLstType                                                                             |
 * 				    ---------- YES: timer slot is registered                                               |
 * 				    ---------- NO: fail to register the timer                                              |
 *                                                                                                         |
 ==========================================================================================================*/
BooleanLstType Platform_Timer_Register(TIMER_USAGE_ENUM UsageType)
{
	/* Initialize return value to NO */
	BooleanLstType Ret_Val = Boolean_NO;

	/* Check if the parameter in range and HW timer is free */
	if((Platform_Timer_local_IsInRange(UsageType) EQUAL_CON Boolean_YES)
		    AND_CON
	   (Platform_Timer_local_IsOccupied(UsageType) EQUAL_CON Boolean_NO)
	  )
	{

		/* Set the bit */
		SetBit(TimerUsedCheckedLst, UsageType);

		/* Set result to YES */
		Ret_Val = Boolean_YES;
	}
	else
	{
		/* The request is invalid, return no */
		Ret_Val = Boolean_NO;
	}
	return Ret_Val;
}













/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                     Local functions definition                                                           |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*                                                                                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/




/*==============================|    Platform_Timer_local_IsOccupied          |============================|
 *                                                                                                         |
 * brief 	    Check if HW timer is currently occupied or not                                             |
 * parameter 	UsageType the reason of using the hardware timer                                           |
 * return       Boolean Type:                                                                              |
 *                  -----YES: The HW is being occupied, cannot use                                         |
 *                  ------NO: The HW is free, can be used now                                              |
 *                                                                                                         |
 ==========================================================================================================*/
BooleanLstType Platform_Timer_local_IsOccupied(TIMER_USAGE_ENUM UsageType)
{
	/* Initialize return value to YES */
	BooleanLstType Ret_Val = Boolean_YES;


	/* Check if the timer was used or not */
	if(BitIsSet(TimerUsedCheckedLst,UsageType))
	{
		/* HW Timer is being used by someone else, return YES */
		Ret_Val = Boolean_YES;
	}
	else
	{
		/* HW Timer is free */
		Ret_Val = Boolean_NO;
	}

	return Ret_Val;
}





/*==============================|    Platform_Timer_local_IsInRange           |============================|
 *                                                                                                         |
 * brief 	    Check if the reason for using HW timer is in range or not                                  |
 * parameter 	UsageType the reason of using the hardware timer                                           |
 * return       Boolean Type:                                                                              |
 *                  -----YES: The reason in in range			                                           |
 *                  ------NO: not in range					                                               |
 *                                                                                                         |
 ==========================================================================================================*/
BooleanLstType Platform_Timer_local_IsInRange(TIMER_USAGE_ENUM UsageType)
{
	/* Initialize return value to NO */
	BooleanLstType Ret_Val = Boolean_NO;

	/* Check if usage reason is in range */
	if((UsageType < 0u)
			OR_CON
	   (UsageType >= TIMER_USAGE_MAX))
	{
		/* Timer usage reason is out of range */
		Ret_Val = Boolean_NO;
	}
	else
	{
		/* Timer usage reason is in range */
		Ret_Val = Boolean_YES;
	}

	return Ret_Val;

}
