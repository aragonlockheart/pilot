/*________________________________________________________________________________________________________________________________________________
**************************************************************************************************************************************************
 ** Platform_timer.h
 **
 **  Created on: May 1, 2015
 **      Author: Viet
 **
 **  brief: This file is the adapter between AUTOSAR driver and MCU hardware.
 **  		It help to control the using of HW, make sure that they will not overlap
 **
 ************************************************************************************************************************************************
/************************************************************************************************************************************************/
#ifndef PLATFORM_TIMER_H_
#define PLATFORM_TIMER_H_

#ifdef __cplusplus
	extended "C" {
#endif


#include "Platform_Types.h"
#include "stm32f4xx_Types.h"
#include "MCU_Reg_Timer3_4.h"
#include "MCU_Reg_Timer2_5.h"
#include "MCU_Reg_Timer6_7.h"




typedef enum
{
	TIMER_1US_16BIT, /*0 used for timer service */
	TIMER_1US_32BIT, /*1 used for timer service */
	TIMER_USAGE_MAX  /*2 Maximum usages */
}TIMER_USAGE_ENUM;


/* Define type for 16 bit timer, used by upper drivers */
typedef MCU_Reg_Timer3_4_RegSet Timer_16Bit_Type;

/* Define type for 32 bit timer, used by upper drivers */
typedef MCU_Reg_Timer2_5_RegSet Timer_32Bit_Type;





BooleanLstType Platform_timer_GetPtr(uint32 * ReqPtr, TIMER_USAGE_ENUM UsageType);
BooleanLstType Platform_Timer_Register(TIMER_USAGE_ENUM UsageType);


#ifdef __cplusplus
	}
#endif

#endif /* PLATFORM_TIMER_H_ */
