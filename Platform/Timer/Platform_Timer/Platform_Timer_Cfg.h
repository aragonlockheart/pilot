/*
 * Platform_Timer_Cfg.h
 *
 *  Created on: May 1, 2017
 *      Author: USER
 */

#ifndef PLATFORM_TIMER_PLATFORM_TIMER_PLATFORM_TIMER_CFG_H_
#define PLATFORM_TIMER_PLATFORM_TIMER_PLATFORM_TIMER_CFG_H_

#include "stm32f4xx.h"
#include "Platform_timer.h"

/* Table to return the pointer for specific purposes */
const uint32 TIMER_CFG_TABLE[TIMER_USAGE_MAX] =
{
	[TIMER_1US_16BIT] = ((uint32) TIM4_BASE),
	[TIMER_1US_32BIT] = ((uint32) TIM5_BASE)
};


#endif /* PLATFORM_TIMER_PLATFORM_TIMER_PLATFORM_TIMER_CFG_H_ */
