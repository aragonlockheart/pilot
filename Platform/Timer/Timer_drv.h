/**
  ******************************************************************************
  * @file    Timer_drv.h
  * @author  Viet
  * @brief
  ******************************************************************************
  * @attention
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef TIMER_DRV_H
#define TIMER_DRV_H

/* header including */
#include "stm32f4xx.h"
#include "stm32f4xx_Types.h"
#ifdef __cplusplus
	extern "C" {
#endif

void Timer_Init(void);

#ifdef __cplusplus
	}
#endif





#endif
