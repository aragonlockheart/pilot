/*
 * Gpt.c
 *
 *  Created on: Dec 19, 2016
 *      Author: Constantine
 */
#include "Gpt.h"
#include "stm32f4xx_Types.h"
#include "Platform_timer.h"





/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                     Private variable			                                                          |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*                                                                                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/* Pointer to 16bit timer */
Timer_16Bit_Type * Timer_16Bit_Ptr;

/* Pointer to 32bit timer */
Timer_32Bit_Type * Timer_32Bit_Ptr;



/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
/* __________________________________________________________________________________________________________________________________________ */
/*/                                                                                                                                          \*/
/*|                                                     Local functions declaration                                                          |*/
/*\__________________________________________________________________________________________________________________________________________/*/
/*                                                                                                                                            */
/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

BooleanLstType Init_Tim16Bit_local(void);
BooleanLstType Init_Tim32Bit_local(void);














/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 __________________________________________________________________________________________________________________________________________
/                                                                                                                                          \
|                                                     Global API definition                                                                |
\__________________________________________________________________________________________________________________________________________/

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

/*    +---------------------------------------[       Gpt_Init		   ]----------------------------------------+
      |                                                                                                         |
      | brief 	    Init the timers								                                                |
      | parameter 	None											                                            |
      | return       None										                                                |
      |                                                                                                         |
      +---------------------------------------------------------------------------------------------------------+   */
void Gpt_Init(void)
{
	Init_Tim16Bit_local();
	Init_Tim32Bit_local();
	return;
}



/*    +-------------------------------------[   Gpt_GetPredefTimerValue  ]---------------------------------------+
	  |                                                                                                          |
	  | brief 	    function to get HW timer value								                                 |
	  | parameter 	TimerType_arg : which timer you want to get value			                                 |
	  | parameter	TimeValuePtr_arg: place to hold timer's value                                                |
	  | return       None										                                                 |
	  |                                                                                                        	 |
	  +----------------------------------------------------------------------------------------------------------+     */
Std_ReturnType Gpt_GetPredefTimerValue(Gpt_PredefTimerType TimerType_arg, uint32* TimeValuePtr_arg)
{
	/* Initialize return value to no ok */
	Std_ReturnType Ret_Val = E_NOT_OK;

	/* Begin checking main part */
	switch (TimerType_arg)
	{
		case GPT_PREDEF_TIMER_1US_16BIT:
			*TimeValuePtr_arg = Timer_16Bit_Ptr->CNT.Whole;
			Ret_Val = E_OK;
			break;
		case GPT_PREDEF_TIMER_1US_32BIT:
			*TimeValuePtr_arg = Timer_32Bit_Ptr->CNT.Whole;
			break;
		default:
			/* No match was found */
			Ret_Val = E_NOT_OK;
			break;
	}

	return Ret_Val;
}




















/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  __________________________________________________________________________________________________________________________________________
 /                                                                                                                                          \
                                                       Local functions definition
 \__________________________________________________________________________________________________________________________________________/

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/




/*+--------------------------------------|   Init_Tim16Bit_local   |---------------------------------------+
  |                                                                                                        |
  | brief 	    Init all the HW timer register to 0			                                               |
  | parameter 	None											                                           |
  | return       Boolean Type:                                                                             |
  |                 -----YES: The HW is being occupied, cannot use                                         |
  |                 ------NO: The HW is free, can be used now                                              |
  |                                                                                                        |
  +--------------------------------------------------------------------------------------------------------+*/
BooleanLstType Init_Tim16Bit_local(void)
{
	/****************** Local variables ****************/

	/* Init return value to NO */
	BooleanLstType Ret_Val = Boolean_NO;

	BooleanLstType Continue = Boolean_NO;


	/* Temporary pointer */
	uint32 Temp_Ptr;



	/************************* Main body ****************/

	/* Try to register the timer HW */
	if(Platform_Timer_Register(TIMER_1US_16BIT) EQUAL_CON Boolean_YES)
	{
		/* Registration is successful, continue */
		Continue = Boolean_YES;
	}
	else
	{
		/* Registration is not good, stop */
		Continue = Boolean_NO;
	}

	/* Try to get pointer to HW timer */
	if( (Continue EQUAL_CON Boolean_YES)
			AND_CON
		(Platform_timer_GetPtr(&Temp_Ptr,TIMER_1US_16BIT) EQUAL_CON Boolean_YES))
	{
		/* Successfully obtain the pointer, pass it to the module wise pointer */
		Timer_16Bit_Ptr = (Timer_16Bit_Type *) Temp_Ptr;

		/* Continue Processing */
		Continue = Boolean_YES;
	}
	else
	{
		/* Failed to get pointer, skip next steps */
		Continue = Boolean_NO;
	}


	/* Initialize register's value */
	if(Continue EQUAL_CON Boolean_YES)
	{
		/* Initialize Dummy struct value */
		Timer_16Bit_Ptr->ARR.Whole 		= 0xFFFFu;
		Timer_16Bit_Ptr->CCER.Whole 	= 0u;
		Timer_16Bit_Ptr->CCMR1.Whole 	= 0u;
		Timer_16Bit_Ptr->CCMR2.Whole 	= 0u;
		Timer_16Bit_Ptr->CCR1.Whole 	= 0u;
		Timer_16Bit_Ptr->CCR2.Whole 	= 0u;
		Timer_16Bit_Ptr->CCR3.Whole 	= 0u;
		Timer_16Bit_Ptr->CCR4.Whole 	= 0u;
		Timer_16Bit_Ptr->CNT.Whole 		= 0u;
		Timer_16Bit_Ptr->DCR.Whole 		= 0u;
		Timer_16Bit_Ptr->DIER.Whole 	= 0u;
		Timer_16Bit_Ptr->DMAR.Whole 	= 0u;
		Timer_16Bit_Ptr->EGR.Whole 		= 0u;
		Timer_16Bit_Ptr->PSC.Whole 		= 0x53u;  /* let the timer running at 1MHz */
		Timer_16Bit_Ptr->RESERVED1 		= 0u;
		Timer_16Bit_Ptr->RESERVED2 		= 0u;
		Timer_16Bit_Ptr->SMCR.Whole 	= 0u;
		Timer_16Bit_Ptr->SR.Whole		= 0u;
		Timer_16Bit_Ptr->TIM2_OR.Whole 	= 0u;
		Timer_16Bit_Ptr->TIM5_OR.Whole 	= 0u;
		Timer_16Bit_Ptr->CR1.Whole 		= 1u; /* Enable timer */
		Timer_16Bit_Ptr->CR2.Whole 		= 0u;

		/* Set return value to YES */
		Ret_Val = Boolean_YES;

		/* This is the final step, stop */
		Continue = Boolean_NO;
	}
	else { /* Do Nothing since previous steps failed */}

	return Ret_Val;
}


/*+--------------------------------------|   Init_Tim32Bit_local   |---------------------------------------+
  |                                                                                                        |
  | brief 	    Init all the HW timer register to 0			                                               |
  | parameter 	None											                                           |
  | return       Boolean Type:                                                                             |
  |                 -----YES: The HW is being occupied, cannot use                                         |
  |                 ------NO: The HW is free, can be used now                                              |
  |                                                                                                        |
  +--------------------------------------------------------------------------------------------------------+*/
BooleanLstType Init_Tim32Bit_local(void)
{
	/****************** Local variables ****************/

	/* Init return value to NO */
	BooleanLstType Ret_Val = Boolean_NO;

	BooleanLstType Continue = Boolean_NO;


	/* Temporary pointer */
	uint32 Temp_Ptr;



	/************************* Main body ****************/

	/* Try to register the timer HW */
	if(Platform_Timer_Register(TIMER_1US_32BIT) EQUAL_CON Boolean_YES)
	{
		/* Registration is successful, continue */
		Continue = Boolean_YES;
	}
	else
	{
		/* Registration is not good, stop */
		Continue = Boolean_NO;
	}

	/* Try to get pointer to HW timer */
	if( (Continue EQUAL_CON Boolean_YES)
			AND_CON
		(Platform_timer_GetPtr(&Temp_Ptr,TIMER_1US_32BIT) EQUAL_CON Boolean_YES))
	{
		/* Successfully obtain the pointer, pass it to the module wise pointer */
		Timer_32Bit_Ptr = (Timer_32Bit_Type *) Temp_Ptr;

		/* Continue Processing */
		Continue = Boolean_YES;
	}
	else
	{
		/* Failed to get pointer, skip next steps */
		Continue = Boolean_NO;
	}


	/* Initialize register's value */
	if(Continue EQUAL_CON Boolean_YES)
	{
		/* Initialize Dummy struct value */
		Timer_32Bit_Ptr->ARR.Whole 		= 0xFFFFFFFFu;
		Timer_32Bit_Ptr->CCER.Whole 	= 0u;
		Timer_32Bit_Ptr->CCMR1.Whole 	= 0u;
		Timer_32Bit_Ptr->CCMR2.Whole 	= 0u;
		Timer_32Bit_Ptr->CCR1.Whole 	= 0u;
		Timer_32Bit_Ptr->CCR2.Whole 	= 0u;
		Timer_32Bit_Ptr->CCR3.Whole 	= 0u;
		Timer_32Bit_Ptr->CCR4.Whole 	= 0u;
		Timer_32Bit_Ptr->CNT.Whole 		= 0xFFFFFFFEu;
		Timer_32Bit_Ptr->DCR.Whole 		= 0u;
		Timer_32Bit_Ptr->DIER.Whole 	= 0u;
		Timer_32Bit_Ptr->DMAR.Whole 	= 0u;
		Timer_32Bit_Ptr->EGR.Whole 		= 0u;
		Timer_32Bit_Ptr->PSC.Whole 		= 0x53u;  /* let the timer running at 1MHz */
		Timer_32Bit_Ptr->RESERVED1 		= 0u;
		Timer_32Bit_Ptr->RESERVED2 		= 0u;
		Timer_32Bit_Ptr->SMCR.Whole 	= 0u;
		Timer_32Bit_Ptr->SR.Whole		= 0u;
		Timer_32Bit_Ptr->TIM2_OR.Whole 	= 0u;
		Timer_32Bit_Ptr->TIM5_OR.Whole 	= 0u;
		Timer_32Bit_Ptr->CR2.Whole 		= 0u;
		Timer_32Bit_Ptr->CR1.Whole 		= 1u; /* Enable timer */

		/* Set return value to YES */
		Ret_Val = Boolean_YES;

		/* This is the final step, stop */
		Continue = Boolean_NO;
	}
	else { /* Do Nothing since previous steps failed */}

	return Ret_Val;
}


