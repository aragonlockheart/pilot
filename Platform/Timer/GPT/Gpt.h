/*!\file Gpt.h
*  \date Dec 19, 2016
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef GPT_H_
#define GPT_H_

#include "Gpt_Cfg.h"
#include "Platform_timer.h"
#include "Platform_Types.h"
#include "Std_Types.h"


#ifdef __cplusplus
	extern "C" {
#endif




void Gpt_Init(void);


#ifdef __cplusplus
	}
#endif
#endif /* GPT_H_ */
