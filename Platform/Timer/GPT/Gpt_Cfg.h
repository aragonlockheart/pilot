/*
 * Gpt_Cfg.h
 *
 *  Created on: May 1, 2017
 *      Author: Viet  Nguyen
 */

#ifndef PLATFORM_TIMER_GPT_GPT_CFG_H_
#define PLATFORM_TIMER_GPT_GPT_CFG_H_

#include "Platform_Types.h"
#include "MCU_Reg_Timer3_4.h"


#define TIM_16BIT_PTR		MCU_Reg_Timer3_4_RegSet *

/* Type of supported timer */
typedef enum
{
	GPT_PREDEF_TIMER_1US_16BIT,
	GPT_PREDEF_TIMER_1US_32BIT,
	GPT_PREDEF_MAX
}Gpt_PredefTimerType;



#endif /* PLATFORM_TIMER_GPT_GPT_CFG_H_ */
