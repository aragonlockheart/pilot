#include "Timer_drv.h"
#include "Timer_para.h"

/**
 * @brief This function initialize the registers for Timer1
 *
 * @param None
 *
 * @reval None
 */
inline static void Timer1_Config(void)
{

	TIM1->CNT = Tim1CNT;
	TIM1->PSC = Tim1PSC;
	TIM1->ARR = Tim1ARR;
	TIM1->CCR1= Tim1CCR_1;
	TIM1->CCR2= Tim1CCR_2;
	TIM1->CCR3= Tim1CCR_3;
	TIM1->CCR4= Tim1CCR_4;

	TIM1->SMCR = Tim1SMCR;
	TIM1->DIER = Tim1DIER;
	TIM1->EGR = Tim1EGR;
	TIM1->CCMR1 = Tim1CCMR1;
	TIM1->CCMR2 = Tim1CCMR2;
	TIM1->CCER = Tim1CCER;

	TIM1->CR2 = Tim1CR2;
	TIM1->CR1 = Tim1CR1;


	return;
}


/**
 * @brief This function initialize the registers for Timer8
 *
 * @param None
 *
 * @reval None
 */
inline static void Timer8_Config(void)
{

	TIM8->CNT = Tim8CNT;
	TIM8->PSC = Tim8PSC;
	TIM8->ARR = Tim8ARR;
	TIM8->CCR1= Tim8CCR_1;
	TIM8->CCR2= Tim8CCR_2;
	TIM8->CCR3= Tim8CCR_3;
	TIM8->CCR4= Tim8CCR_4;

	TIM8->SMCR = Tim8SMCR;
	TIM8->DIER = Tim8DIER;
	TIM8->EGR = Tim8EGR;
	TIM8->CCMR1 = Tim8CCMR1;
	TIM8->CCMR2 = Tim8CCMR2;
	TIM8->CCER = Tim8CCER;

	TIM8->CR2 = Tim8CR2;
	TIM8->CR1 = Tim8CR1;


	return;
}

/**
 * @brief This function initialize the registers for Timer2
 *
 * @param None
 *
 * @reval None
 */
inline static void Timer2_Config(void)
{

	TIM2->CNT = Tim2CNT;
	TIM2->PSC = Tim2PSC;
	TIM2->ARR = Tim2ARR;
	TIM2->CCR1= Tim2CCR_1;
	TIM2->CCR2= Tim2CCR_2;
	TIM2->CCR3= Tim2CCR_3;
	TIM2->CCR4= Tim2CCR_4;

	TIM2->SMCR = Tim2SMCR;
	TIM2->DIER = Tim2DIER;
	TIM2->EGR = Tim2EGR;
	TIM2->CCMR1 = Tim2CCMR1;
	TIM2->CCMR2 = Tim2CCMR2;
	TIM2->CCER = Tim2CCER;

	TIM2->CR2 = Tim2CR2;
	TIM2->CR1 = Tim2CR1;


	return;
}

inline static void Timer5_Config(void)
{

	TIM5->CNT = Tim5CNT;
	TIM5->PSC = Tim5PSC;
	TIM5->ARR = Tim5ARR;
	TIM5->CCR1= Tim5CCR_1;
	TIM5->CCR2= Tim5CCR_2;
	TIM5->CCR3= Tim5CCR_3;
	TIM5->CCR4= Tim5CCR_4;

	TIM5->SMCR = Tim5SMCR;
	TIM5->DIER = Tim5DIER;
	TIM5->EGR = Tim5EGR;
	TIM5->CCMR1 = Tim5CCMR1;
	TIM5->CCMR2 = Tim5CCMR2;
	TIM5->CCER = Tim5CCER;

	TIM5->CR2 = Tim5CR2;
	TIM5->CR1 = Tim5CR1;

	return;
}

inline static void Timer3_Config(void)
{

	TIM3->CNT = Tim3CNT;
	TIM3->PSC = Tim3PSC;
	TIM3->ARR = Tim3ARR;
	TIM3->CCR1= Tim3CCR_1;
	TIM3->CCR2= Tim3CCR_2;
	TIM3->CCR3= Tim3CCR_3;
	TIM3->CCR4= Tim3CCR_4;

	TIM3->SMCR = Tim3SMCR;
	TIM3->DIER = Tim3DIER;
	TIM3->EGR = Tim3EGR;
	TIM3->CCMR1 = Tim3CCMR1;
	TIM3->CCMR2 = Tim3CCMR2;
	TIM3->CCER = Tim3CCER;

	TIM3->CR2 = Tim3CR2;
	TIM3->CR1 = Tim3CR1;


	return;
}

inline static void Timer4_Config(void)
{

	TIM4->CNT = Tim4CNT;
	TIM4->PSC = Tim4PSC;
	TIM4->ARR = Tim4ARR;
	TIM4->CCR1= Tim4CCR_1;
	TIM4->CCR2= Tim4CCR_2;
	TIM4->CCR3= Tim4CCR_3;
	TIM4->CCR4= Tim4CCR_4;

	TIM4->SMCR = Tim4SMCR;
	TIM4->DIER = Tim4DIER;
	TIM4->EGR = Tim4EGR;
	TIM4->CCMR1 = Tim4CCMR1;
	TIM4->CCMR2 = Tim4CCMR2;
	TIM4->CCER = Tim4CCER;

	TIM4->CR2 = Tim4CR2;
	TIM4->CR1 = Tim4CR1;


	return;
}

inline static void Timer6_Config(void)
{

	TIM6->CNT 	= Tim6CNT;
	TIM6->PSC 	= Tim6PSC;
	TIM6->ARR 	= Tim6ARR;
	TIM6->DIER 	= Tim6DIER;
	TIM6->EGR 	= Tim6EGR;
	TIM6->CR2 	= Tim6CR2;
	TIM6->CR1 	= Tim6CR1;

	return;
}

void Timer_Init(void)
{
	return;
}
