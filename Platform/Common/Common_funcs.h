#ifndef COMMON_fUNCS_H
#define COMMON_fUNCS_H


#include "stm32f4xx_Types.h"
#include "Global_Var.h"

#ifdef __cplusplus
	extern "C" {
#endif

/**
*@addtogroup USER_FUNCS
* @brief the common functions which are used frequently
* @{

*/

/**
 * @defgroup COMMON_FUNCS
 * @{
 **/

/**
 * @{
 */

/**
  * @brief  Swap the values of 2 variables using their address
  * @param  uint16 * a: Address of the first variable
  * @param  uint16 * b: Address of the second variable
  * @retval None
  */
void Swap16(uint16 * a, uint16 * b);
void SplitDecDigits16(uint16 Victim, uint16 * Digits);
/**
 * @}
 */
/**
 * @}
 */
/**
 * @}
 */


#ifdef __cplusplus
	}
#endif
#endif

