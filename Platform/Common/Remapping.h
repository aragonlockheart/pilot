#ifndef REMAPPING_H_
#define REMAPPING_H_

/* Quick LED control macros for debug purposes */
/* Require LED initialization is done by Port_init()*/

#define LED_GREEN_ON()					GPIOD->BSRRL |= 0x00001000
#define LED_GREEN_OFF()					GPIOD->BSRRH |= 0x00001000
#define LED_GREEN_BLINK()				GPIOD->ODR ^= 0x00001000

#define LED_ORANGE_ON()					GPIOD->BSRRL |= 0x00002000
#define LED_ORANGE_OFF()				GPIOD->BSRRH |= 0x00002000
#define LED_ORANGE_BLINK()				GPIOD->ODR ^= 0x00002000

#define LED_RED_ON()					GPIOD->BSRRL |= 0x00004000
#define LED_RED_OFF()					GPIOD->BSRRH |= 0x00004000
#define LED_RED_BLINK()					GPIOD->ODR ^= 0x00004000

#define LED_BLUE_ON()					GPIOD->BSRRL |= 0x00008000
#define LED_BLUE_OFF()					GPIOD->BSRRH |= 0x00008000
#define LED_BLUE_BLINK()				GPIOD->ODR ^= 0x00008000

#endif
