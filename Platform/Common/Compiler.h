/*!\file Compiler.h
*  \date May 21, 2016
*  \author Nguyen Trong Viet
*  \brief This file is made accordingly to AUTOSAR 4.2.2 "Specification of Compiler Abstraction"
*
*/

#ifndef COMPILER_H
#define COMPILER_H

#ifdef __cplusplus
	extern "C" {
#endif

/* Define name of compiler and chip architecture */
#define _GNU_C_ARM_								/* SWS_COMPILER_00010 */

#define AUTOMATIC								/* SWS_COMPILER_00046 */
#define TYPEDEF									/* SWS_COMPILER_00059 */
#define NULL_PTR	void pointer ((void *)0) 	/* SWS_COMPILER_00051 */
#define INLINE									/* SWS_COMPILER_00057 */
#define LOCAL_INLINE							/* SWS_COMPILER_00060 */

#define FUNC(rettype, memclass) 				   memclass rettype  							/* SWS_COMPILER_00001 */
#define FUNC_P2CONST(rettype, ptrclass, memclass)  const ptrclass rettype * memclass		/* SWS_COMPILER_00061 */
#define FUNC_P2VAR(rettype, ptrclass, memclass)    ptrclass rettype * memclass       		/* SWS_COMPILER_00063 */
#define P2VAR(ptrtype, memclass, ptrclass)         ptrclass ptrtype * memclass				/* SWS_COMPILER_00006 */
#define P2CONST(ptrtype, memclass, ptrclass)       const ptrtype ptrclass * memclass 		/* SWS_COMPILER_00013 */
#define CONSTP2VAR (ptrtype, memclass, ptrclass)   ptrclass ptrtype * const memclass 		/* SWS_COMPILER_00031 */
#define CONSTP2CONST (ptrtype, memclass, ptrclass) const memclass ptrtype * const ptrclass	/* SWS_COMPILER_00032 */
#define CONSTP2FUNC(rettype, ptrclass, fctname)    rettype (* const ptrclass fctname)		/* SWS_COMPILER_00065 */


#define CONST(type, memclass) 					   memclass const type 						/* SWS_COMPILER_00023 */
#define VAR(type, memclass) 					   memclass type							/* SWS_COMPILER_00026 */


#define STRICT_INLINE							__attribute__((always_inline))				/* GCC force inline function */
#define GLOBAL									volatile									/* GCC volatile for all global var */
#define STATIC									static										/* GCC static */


#ifdef __cplusplus
	}
#endif
#endif /* COMPILER_H */
