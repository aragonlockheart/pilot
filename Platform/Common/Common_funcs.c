#include "Common_funcs.h"

/**
  * @brief  Swap the values of 2 variables using their address
  * @param  uint16 * a: Address of the first variable
  * @param  uint16 * b: Address of the second variable
  * @retval None
  */

void Swap16(uint16 * a, uint16 * b)
{
	uint16 temp = ZERO_16;
	temp = *a;
	*a = *b;
	*b = temp;
	return;
}

void SplitDecDigits16(uint16 Victim, uint16 * Digits)
{
	uint16 i = ZERO_16;
	uint16 Stored_Var;

	if(Victim != ZERO_16)
	{
		while(Victim > ZERO_16)
		{
			Stored_Var = Victim REMAIN_BY 10;

			Victim = Victim DEVIDE_BY 10;

			*(Digits + i) = Stored_Var;

			i++;
		}

	}
	else
	{
		*Digits = ZERO_16;
		*(Digits + ONE_16) = ZERO_16;
		*(Digits + TWO_16) = ZERO_16;
		*(Digits + THREE_16) = ZERO_16;
		*(Digits + FOUR_16) = ZERO_16;

	}

	return;
}
