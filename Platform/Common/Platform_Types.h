/*!\file Platform_Types.h
*  \date May 21, 2016
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef PLATFORM_TYPES_H
#define PLATFORM_TYPES_H

#include "stm32f4xx.h"


#ifdef __cplusplus
	extern "C" {
#endif

/* Define the register width of the CPU used */
#define CPU_TYPE	CPU_TYPE_32					/* SWS_Platform_00045 */

/* Define Endianess */
#define CPU_BIT_ORDER	LSB_FIRST		/* SWS_Platform_00049 */
#define CPU_BYTE_ORDER	LOW_BYTE_FIRST 	/* SWS_Platform_00051 */

/* Define True and False */

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE !(FALSE)
#endif

#ifndef NULL
#define NULL ( (void *) 0)
#endif

/* Define data type */
typedef unsigned 		   char 	boolean;  	/* SWS_Platform_00026 */
typedef unsigned 		   char 	uint8;		/* SWS_Platform_00013 */
typedef unsigned short 	   int 		uint16;		/* SWS_Platform_00014 */
typedef unsigned 		   int 		uint32;		/* SWS_Platform_00015 */
typedef unsigned long long int 		uint64;		/* SWS_Platform_00066 */
typedef signed 	 		   char		sint8;		/* SWS_Platform_00016 */
typedef signed 	 short 	   int		sint16;		/* SWS_Platform_00017 */
typedef signed 	 		   int      sint32;		/* SWS_Platform_00018 */
typedef signed   long long int      sint64;     /* SWS_Platform_00067 */

typedef unsigned 		   char 	uint8_least;		/* SWS_Platform_00020 */
typedef unsigned short 	   int 		uint16_least;		/* SWS_Platform_00021 */
typedef unsigned 		   int 		uint32_least;		/* SWS_Platform_00022 */
typedef signed 	 		   char		sint8_least;		/* SWS_Platform_00023 */
typedef signed 	 short 	   int		sint16_least;		/* SWS_Platform_00024 */
typedef signed 	 		   int      sint32_least;		/* SWS_Platform_00025 */

typedef float 						float32;	/* SWS_Platform_00041 */
typedef 		 long  	   double	float64;	/* SWS_Platform_00041 */

/* Define common bitfields */
typedef struct
{
	uint8	BIT_0:	1; /* bit 0 */
	uint8   BIT_1:  1; /* bit 1 */
	uint8	BIT_2:	1; /* bit 2 */
	uint8   BIT_3:  1; /* bit 3 */
	uint8	BIT_4:  1; /* bit 4 */
	uint8   BIT_5:  1; /* bit 5 */
	uint8   BIT_6:  1; /* bit 6 */
	uint8   BIT_7:  1; /* bit 7 */
}Bitfield_uint8;

typedef union
{
	Bitfield_uint8 BIT_FIELD;
	uint8		   WHOLE_08;
}Union_uint8;

#ifdef __cplusplus
	}
#endif
#endif /* PLATFORM_TYPES_H */

