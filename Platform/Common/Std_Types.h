/*!\file Std_Types.h
*  \date May 21, 2016
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef STD_TYPES_H
#define STD_TYPES_H



#ifdef __cplusplus
	extern "C" {
#endif

#include "Compiler.h"
#include "Platform_Types.h"

/* E_OK and E_NOT_OK */
#ifndef STATUSTYPEDEFINED
#define STATUSTYPEDEFINED
#define E_OK 0x00u
typedef unsigned char StatusType; /* OSEK compliance */
#endif
#define E_NOT_OK 0x01u			  /* SWS_Std_00006 */

/* STD High and Low */
#define STD_HIGH 0x01u
#define STD_LOW  0x00u			  /* SWS_Std_00007 */

/* STD active and idle */
#define STD_ACTIVE 0x01u 		  /* Logical state active */
#define STD_IDLE   0x00u 		  /* Logical state idle */       /* SWS_Std_00013 */

/* STD ON and OFF */
#define STD_ON 0x01u			  /* SWS_Std_00010 */
#define STD_OFF 0x00u

/* Data type definition */
typedef uint8 Std_ReturnType;	  /* SWS_Platform_00056 */

/* Define for VersionInfoType, This type shall be used to request the version
 * of a BSW module using the <Module name>_GetVersionInfo() function.
 */
typedef struct
{
	uint16	vendorID;
	uint16	moduleID;
	uint8	sw_major_version;
	uint8	sw_minor_version;
	uint8	sw_patch_version;
}Std_VersionInfoType;			/* SWS_Std_00015 */

typedef enum
{
	Boolean_NO,
	Boolean_YES
}BooleanLstType;

#ifdef __cplusplus
	}
#endif
#endif /* STD_TYPES_H */
