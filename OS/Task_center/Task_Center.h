#ifndef TASK_CENTER_H
#define TASK_CENTER_H

#include "Std_Types.h"
#include "DMA_Nm.h"
#include "Demo_App.h"
#include "Tm.h"
#include "stm32f4xx_rcc.h"

#include "I2C_core.h"
#include "I2C_Parameter.h"
#include "I2C_Cfg.h"

#ifdef __cplusplus
	extern "C" {
#endif

void T_0005_ms(void);
void T_0010_ms(void);
void T_0050_ms(void);
void T_0100_ms(void);


#ifdef __cplusplus
	}
#endif

#endif
