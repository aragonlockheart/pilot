#include "Task_Center.h"
#include "Gpt.h"
volatile uint32 Debug_Tim;


void T_0005_ms(void)
{
}

void T_0010_ms(void)
{
	/* DMA network management */
	DMA_NM_StateManager_Cyclic();

}

void T_0050_ms(void)
{

/*********** I2C SECTION **********************/
#if (FS_I2C_CHANNEL_1_ENABLED == I2C_FS_ENABLED)
	I2C_TransmitProcess(&I2C_Handler_1);
	I2C_ReceiveProcess(&I2C_Handler_1);
#endif

#if (FS_I2C_CHANNEL_2_ENABLED == I2C_FS_ENABLED)
	I2C_TransmitProcess(&I2C_Handler_2);
	I2C_ReceiveProcess(&I2C_Handler_2);
#endif

#if (FS_I2C_CHANNEL_3_ENABLED == I2C_FS_ENABLED)
	I2C_TransmitProcess(&I2C_Handler_3);
	I2C_ReceiveProcess(&I2C_Handler_3);
#endif
/*********** I2C SECTION **********************/

	DS3231_UpdateData();
}

void T_0100_ms(void)
{
	DEMO_APPUART();
	systick_counter++;
	if (systick_counter >= 10)
	{
		systick_counter = 0;
		if (initialized)
		{
		//	I2C_UserReceive(&I2C_Handler_3, (DS33231_ADD << 1), 0x0000, receive_data, 7);
			I2C_UserReceive_MemRequest_Chnl1((DS33231_ADD << 1), 0x0000, receive_data, 7);
			I2C_UserReceive_MemRequest_Chnl2((DS33231_ADD << 1), 0x0000, receive_data, 7);
			I2C_UserReceive_MemRequest_Chnl3((DS33231_ADD << 1), 0x0000, receive_data, 7);
		}
	}

}

