/* ###*B*###
 * ERIKA Enterprise - a tiny RTOS for small microcontrollers
 *
 * Copyright (C) 2002-2011  Evidence Srl
 *
 * This file is part of ERIKA Enterprise.
 *
 * ERIKA Enterprise is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation, 
 * (with a special exception described below).
 *
 * Linking this code statically or dynamically with other modules is
 * making a combined work based on this code.  Thus, the terms and
 * conditions of the GNU General Public License cover the whole
 * combination.
 *
 * As a special exception, the copyright holders of this library give you
 * permission to link this code with independent modules to produce an
 * executable, regardless of the license terms of these independent
 * modules, and to copy and distribute the resulting executable under
 * terms of your choice, provided that you also meet, for each linked
 * independent module, the terms and conditions of the license of that
 * module.  An independent module is a module which is not derived from
 * or based on this library.  If you modify this code, you may extend
 * this exception to your version of the code, but you are not
 * obligated to do so.  If you do not wish to do so, delete this
 * exception statement from your version.
 *
 * ERIKA Enterprise is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License version 2 for more details.
 *
 * You should have received a copy of the GNU General Public License
 * version 2 along with ERIKA Enterprise; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301 USA.
 * ###*E*### */

/*
 * Author: 2013 Gianluca Franchino
 * 
 */

CPU mySystem {

	OS myOs {
		EE_OPT = "DEBUG";
		//EE_OPT = "__NO_APP__";
		
		
		CPU_DATA = CORTEX_MX {
			MODEL = M4;
			COMPILER_TYPE = GNU;
			MULTI_STACK = FALSE;
			APP_SRC = "code.c";
		};
		EE_OPT = "__USE_SYSTICK__";

		MCU_DATA = STM32 {
			MODEL = STM32F4xx;
		};
		
		STATUS = STANDARD;
		STARTUPHOOK = FALSE;
		ERRORHOOK = FALSE;
		SHUTDOWNHOOK = FALSE;
		PRETASKHOOK = FALSE;
		POSTTASKHOOK = FALSE;
		USEGETSERVICEID = FALSE;
		USEPARAMETERACCESS = FALSE;
		USERESSCHEDULER = FALSE;

		KERNEL_TYPE = FP;

	};
	
	COUNTER OSMainCounter
	{
		MINCYCLE = 1; /* LEAVE THIS VALUE TO 1 */
		MAXALLOWEDVALUE = 65535 ; /* Change this to choose the counter resolution. */
		TICKSPERBASE = 1; /* LEAVE THIS VALUE TO 1 */	
	};

	
	ALARM ALARM_5ms 
	{
		COUNTER = OSMainCounter;
		ACTION = ACTIVATETASK { TASK = Task_5ms; };
	};
	
	ALARM ALARM_10ms 
	{
		COUNTER = OSMainCounter;
		ACTION = ACTIVATETASK { TASK = Task_10ms; };
	};
	
	ALARM ALARM_50ms 
	{
		COUNTER = OSMainCounter;
		ACTION = ACTIVATETASK { TASK = Task_50ms; };
	};
	
	ALARM ALARM_100ms 
	{
		COUNTER = OSMainCounter;
		ACTION = ACTIVATETASK { TASK = Task_100ms; };
	};
	
	
	TASK Task_5ms 
	{
		PRIORITY = 0xFF;   /* Highest priority */
		AUTOSTART = FALSE;
		STACK = SHARED;
		ACTIVATION = 1;    /* only one pending activation */
		SCHEDULE = FULL;
	};
	
	TASK Task_10ms 
	{
		PRIORITY = 0xFE;   /* Second place priority */
		AUTOSTART = FALSE;
		STACK = SHARED;
		ACTIVATION = 1;    /* only one pending activation */
		SCHEDULE = FULL;
	};
	
	TASK Task_50ms 
	{
		PRIORITY = 0xFD;   /* Third Place priority */
		AUTOSTART = FALSE;
		STACK = SHARED;
		ACTIVATION = 1;    /* only one pending activation */
		SCHEDULE = FULL;
	};
	
	
	TASK Task_100ms 
	{
		PRIORITY = 0xFC;   /* Lowest priority */
		AUTOSTART = FALSE;
		STACK = SHARED;
		ACTIVATION = 1;    /* only one pending activation */
		SCHEDULE = FULL;
	};
		
	ISR systick_handler 
	{
		CATEGORY = 2;
		ENTRY = "SYSTICK";
		PRIORITY = 1;
	};

};
