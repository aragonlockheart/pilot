/*
 * OS_Service.c
 *
 *  Created on: Jun 2, 2016
 *      Author: Constantine
 */
#include "OS_Service.h"


uint32 TaskCenter_RunTime[EE_MAX_TASK];
uint32 TaskCenter_PeriodTime[EE_MAX_TASK];
Tm_PredefTimer1us32bitType TaskCenter_PeriodBase[EE_MAX_TASK];
uint32 Debug_Counter;


void OSEK_START(void)
{
	/*Initialize Erika related stuffs*/


		/*Initialize systick */
		EE_systick_set_period(MILLISECONDS_TO_TICKS(1, SystemCoreClock));
		EE_systick_enable_int();

		SetRelAlarm(ALARM_5ms, 10, 5);
		SetRelAlarm(ALARM_10ms, 10, 10);
		SetRelAlarm(ALARM_50ms, 10, 50);
		SetRelAlarm(ALARM_100ms, 10, 100);


		EE_systick_start();


}


TASK(Task_5ms)
{
	Tm_PredefTimer1us32bitType local_start_time;
	Tm_GetTimeSpan1us32bit(&TaskCenter_PeriodBase[Task_5ms].Timepreference,&TaskCenter_PeriodTime[Task_5ms]);
	Tm_ResetTimer1us16bit(&local_start_time.Timepreference);

	T_0005_ms();

	Tm_GetTimeSpan1us32bit(&local_start_time.Timepreference,&TaskCenter_RunTime[1]);
	Tm_ResetTimer1us32bit(&TaskCenter_PeriodBase[Task_5ms].Timepreference);
}

TASK(Task_10ms)
{
	Tm_PredefTimer1us32bitType local_start_time;
	Tm_GetTimeSpan1us32bit(&TaskCenter_PeriodBase[Task_10ms].Timepreference,&TaskCenter_PeriodTime[Task_10ms]);
	Tm_ResetTimer1us32bit(&local_start_time);

	T_0010_ms();

	Tm_GetTimeSpan1us32bit(&local_start_time,&TaskCenter_RunTime[2]);
	Tm_ResetTimer1us32bit(&TaskCenter_PeriodBase[Task_10ms].Timepreference);
}

TASK(Task_50ms)
{
	Tm_GetTimeSpan1us32bit(&TaskCenter_PeriodBase[Task_50ms].Timepreference,&TaskCenter_PeriodTime[Task_50ms]);
	Tm_PredefTimer1us32bitType local_start_time;
	Tm_ResetTimer1us32bit(&local_start_time);

	T_0050_ms();
	Tm_GetTimeSpan1us32bit(&local_start_time,&TaskCenter_RunTime[0]);
	Tm_ResetTimer1us32bit(&TaskCenter_PeriodBase[Task_50ms].Timepreference);
	Debug_Counter++;

}

TASK(Task_100ms)
{
	Tm_PredefTimer1us32bitType local_start_time;
	Tm_GetTimeSpan1us32bit(&TaskCenter_PeriodBase[Task_100ms],&TaskCenter_PeriodTime[Task_100ms]);
	Tm_ResetTimer1us32bit(&local_start_time);

	T_0100_ms();

	Tm_GetTimeSpan1us32bit(&local_start_time,&TaskCenter_RunTime[3]);
	Tm_ResetTimer1us32bit(&TaskCenter_PeriodBase[Task_100ms].Timepreference);
}

/*
 * SysTick ISR2
 */
ISR2(systick_handler)
{
	/* count the interrupts, waking up expired alarms */
	CounterTick(OSMainCounter);

}
