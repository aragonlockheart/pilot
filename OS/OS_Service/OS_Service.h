/*!\file OS_Service.h
*  \date Jun 2, 2016
*  \author Nguyen Trong Viet
*  \brief 
*/

#ifndef OS_SERVICE_H
#define OS_SERVICE_H

#include "Task_center.h"
#include "ee.h"
#include "ee_api.h"


#ifdef __cplusplus
	extern "C" {
#endif

void OSEK_START(void);
TASK(Task_2ms);
TASK(Task_5ms);
TASK(Task_10ms);
TASK(Task_100ms);

#ifdef __cplusplus
	}
#endif
#endif /* OS_SERVICE_H_ */
