 use strict;
 use warnings;
 use File::Find qw(finddepth);
 use Data::Dump qw(dump);
 use File::Basename;
 use List::MoreUtils qw(uniq);
 use File::Find::Rule;
 use File::Spec;
 use Cwd;
 my @files;
 my @pure_paths;
 my @c_files_name;

my $Projectpath ='.\\';
my $Filename = 'all_head.mk';
my $Debug_path = $Projectpath.'Debug\\';
my $FullName = $Debug_path.$Filename;
my $cwd = getcwd();

 finddepth(sub {
      return if($_ eq '.' || $_ eq '..');
      push @files, $File::Find::name;
 }, $Projectpath);
 

if( -e $FullName)
{
	unlink $FullName;
}

my @Cfiles = File::Find::Rule->file() ->name( '*.c' ) ->in($Projectpath );
foreach my $c_element(@Cfiles)
{
	$c_element =~ s#\\#/#g;
	 my($temp_c_filename, $temp_c_dirs, $temP_suffix) = fileparse($c_element);
	 
	 if($temp_c_dirs ne "Debug/")
	 {
		push@c_files_name, $c_element;
	 }
}



foreach my $directory(@files)
{
	$directory =~ s#\\#/#g;
	 my($temp_filename, $temp_dirs, $temP_suffix) = fileparse($directory);
	 push@pure_paths, $temp_dirs;
}

@pure_paths= uniq(@pure_paths);


open(my $TargetFile, '>', $FullName) or die "Could not open file $Filename";


foreach my $dir_path(@pure_paths)
{
	$dir_path = substr($dir_path,1);
	my $find = "/";
	my $replace = "\\\\";
	$dir_path =~s/$find/$replace/g;
	$cwd =~s/$find/$replace/g;
	
	print $TargetFile "CFLAGS += ".'"-I'.$cwd.$dir_path.'"'."\n";
}

foreach my $c_name(@c_files_name)
{
	print $TargetFile "APP_SRCS += ".$c_name."\n";
}

print $TargetFile "CFLAGS += "."-Os "."\n";
print $TargetFile "CFLAGS += "."-std=c11 "."\n";
print $TargetFile "CFLAGS += "."-fdata-sections -ffunction-sections "."\n";
print $TargetFile "LFLAGS += "."-Wl,--gc-sections"."\n";

close $TargetFile;

