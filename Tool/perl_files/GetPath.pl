#!/usr/bin/perl

#include files 
use Cwd;
use Getopt::Long;
use strict;
use warnings;
use File::Find::Rule;
use File::Path;
use File::Path qw(make_path remove_tree);
use File::Basename;
use File::Copy;
use File::Spec;


# Declare global variable
my $ProjectDir='';
my $Action 	=	'';
my $ToolDir = 	"Tool";
my $MakeDir = 	"MakeDir";
my $ObjDir 	= 	"Obj";
my $ProgBin = 	"Output";
my @ContentList;

# Get parameters from command line 
GetOptions ("ProjectDirectory=s" => \$ProjectDir,
			"Action=s"			 => \$Action);

$ProjectDir = GetUnixPath($ProjectDir);
$Action = GetUnixPath($Action);




# Check if the parameter is valid or not 
if( $ProjectDir eq '' )
{
	die ("please enter the project path");
}
else
{
	# Continue processing
}

# Declare file name
my $MakeFolder 	= $ProjectDir."/".$ToolDir."/".$MakeDir;
my $ObjFolder 	= $ProjectDir."/".$ToolDir."/".$ObjDir;
my $BinFolder	= $ProjectDir."/".$ToolDir."/".$ProgBin;
my $FolderList 	= $MakeFolder."/"."FolderList.mk"; 
my $SrcList 	= $MakeFolder."/"."SrcList.mk";
my $HdrList 	= $MakeFolder."/"."HdrList.mk";
my $ObjList		= $MakeFolder."/"."ObjList.mk";
my $MakePathList= $MakeFolder."/"."MakePathList.mk";
my $MkPattList	= $MakeFolder."/"."MkPattList.mk";
my $HexFile		= "/"."Prog.hex";
my $MapFile		= "/"."Prog.map";
my $ElfFile		= "/"."Prog.elf";

CheckEnv();

if($Action eq "build")
{
	MakeProcess();
}
elsif ($Action eq "clean")
{
	CleanProcess();	
}
else { die ("Unsupported action");}













sub MakeProcess
{
	
	CleanFiles();
	# Check if the required folder exists
	if(-d $MakeFolder)
	{
		# Continue processing
	}
	else
	{
		# Folder is not there, create the folder
		make_path($MakeFolder) or die ("cannot make folder for make file");
	}
	
	# Check if the required folder exists
	if(-d $ObjFolder)
	{
		# Continue processing
	}
	else
	{
		# Folder is not there, create the folder
		make_path($ObjFolder) or die ("cannot make folder for obj file");
	}
	
	# Check if the required folder exists
	if(-d $BinFolder)
	{
		# Continue processing
	}
	else
	{
		# Folder is not there, create the folder
		make_path($BinFolder) or die ("cannot make folder for binaries");
	}


	# Export the list of all the sub-folder of compiler for libraries
	@ContentList = GetFolderPath($ENV{'ARM_GCC'});
	ExportFile($FolderList,"LDIncludePath",\@ContentList,"\'-L","\'");	
		
	# Export the list of all the sub-folders of Erika source
	@ContentList=();
	push @ContentList, $ENV{'ERIKA_PATH'};
	ExportFile($FolderList,"IncludePath",\@ContentList,"\'-I","/pkg/\'");
	ExportFile($MakePathList,"VPATH",\@ContentList,"","");
	
	# Export the list of all the sub-folders of project
	@ContentList = GetFolderPath($ProjectDir);
	ExportFile($FolderList,"IncludePath",\@ContentList,"\'-I","\'");
	ExportFile($MakePathList,"VPATH",\@ContentList,"","");
	
	
	# Export the list of all the header files
	@ContentList = GetFilePath($ProjectDir, "*.h");
	ExportFile($HdrList,"CHEAD",\@ContentList,"-I","");
	
	# Export the list of all the C source files
	@ContentList = GetFilePath($ProjectDir, "*.c");
	ExportFile($SrcList,"CSRC",\@ContentList,"","");
	
	# Export the list of object files that need to build from C source
	@ContentList = GetFileName($ProjectDir, "*.c");
	ExportFile($ObjList, "OBJECTC", \@ContentList, GetRelPath($ObjFolder,$MakeFolder)."/",".o");
	
	# Export the list of all the Assembly source files
	@ContentList = GetFilePath($ProjectDir, "*.S");
	ExportFile($SrcList,"ASSRC",\@ContentList,"","");
	
	# Export the list of object files that need to build from AS source
	@ContentList = GetFileName($ProjectDir, "*.S");
	ExportFile($ObjList, "OBJECTAS", \@ContentList, GetRelPath($ObjFolder,$MakeFolder)."/",".o");
	
	# Export the list of pattern
	@ContentList = ();
	push @ContentList, GetRelPath($ObjFolder,$MakeFolder)."/%.o";
	ExportFile($MkPattList, "OBJPAT", \@ContentList, "","");
	
	@ContentList = ();
	push @ContentList, GetRelPath($BinFolder,$MakeFolder);
	ExportFile($MkPattList, "HEXPAT", \@ContentList, "","$HexFile");
	ExportFile($MkPattList, "MAPPAT", \@ContentList, "","$MapFile");
	ExportFile($MkPattList, "ELFPAT", \@ContentList, "","$ElfFile");
		
	# Call makefile
	my $MakeQuery = PrepareMakeQuery();
	system("cd /d $MakeFolder && $MakeQuery");
}


sub CleanProcess
{
	CleanFiles();
	CleanDirs();
	
	# Call makefile
	my $MakeQuery = PrepareMakeQuery();
	system("cd /d $MakeFolder && $MakeQuery");
}



























sub GetFolderPath
{
	# Get in the first parameter, in this case it is parent folder's path
	my $local_path = $_[0]; 
	
	my @Return_Arr;
	
	# Get the list of subdirectory
	my @DirList = File::Find::Rule->directory
								  ->in($local_path);
	
	# Get
	foreach my $a (@DirList)
	{
		my $temp_path = File::Spec->abs2rel($a, $MakeFolder);
		$temp_path =~ s/\\/\//g;
		push @Return_Arr, $temp_path;
	}
	
	return @Return_Arr;
}

sub GetFilePath
{
	# Get in the first parameter, in this case it is parent folder's path
	my $local_path = $_[0]; 
	
	# Get in the second parameter, in this case it is the file's extension
	my $local_extension = $_[1];
	
	# Initialize return value
	my @Return_Arr;
	
	# Get the list of file
	my @FileList = File::Find::Rule->file
								   ->name("$local_extension")
								   ->in($local_path);
	# Get
	foreach $a (@FileList)
	{
		my $temp_path =  File::Spec->abs2rel($a, $MakeFolder);
		$temp_path =~ s/\\/\//g;
		push @Return_Arr, $temp_path;
	}			
	
	return @Return_Arr;					
}

sub GetFileName
{
	# Get in the first parameter, in this case it is parent folder's path
	my $local_path = $_[0]; 
	
	# Get in the second parameter, in this case it is the file's extension
	my $local_extension = $_[1];
	
	# Get the list of file
	my @FileList = File::Find::Rule->file
								   ->name("$local_extension")
								   ->in($local_path);
    my @FileName;		
    my $temp_name;						   
								   
	# Get the list of the file name
	# Get
	foreach my $FilePath (@FileList)
	{
		$temp_name = fileparse($FilePath);
		$temp_name = (split /\./,$temp_name)[0];
		push @FileName, $temp_name;
	}			
	
	return @FileName;					
}

sub ExportFile
{
	# Get in the first parameter, which is the absolute path and file name
	my $FileName = $_[0];
	
	# Get in the makefile flag
	my $MakeFlag = $_[1];
	
	# Get in the array of content
	my @Make_Content = @{$_[2]};
	
	# Get in the prefix of the content
	my $MakePrefix = $_[3];
	
	# Get in the suffix of the content
	my $MakeSuffix = $_[4];
	
	# Open the file
	open(my $fh,">>","$FileName") or die ("cannot open $FileName");
	
	# Write content of the file
	foreach my $content (@Make_Content)
	{
		#Acual content will be written to file
		my $ActualContent = $MakeFlag."+=".$MakePrefix.$content.$MakeSuffix;
		print $fh "$ActualContent \n" or die ("cannot write to file $FileName");
	}
	
	close($fh);
}

sub PrepareMakeQuery
{
	my $Query;
	my $RelPath = GetRelPath($ObjFolder,$MakeFolder);
	if($Action eq "build")
	{
		$Query = "make 2>$BinFolder"."/error_log OBJDIR=$RelPath all ";
	}
	elsif ($Action eq "clean")
	{
		$Query = "make OBJDIR=$RelPath clean";
		
	}
	else{die("there is no action")}
	
	print "\n";
	return $Query;
}

sub GetRelPath
{
	my $target = $_[0];
	my $base   = $_[1];
	
	my $temp_path =  File::Spec->abs2rel($target, $base);
	$temp_path =~ s/\\/\//g;
	
	return $temp_path;	
}

sub CleanFiles
{
	if(-e $FolderList) 		{unlink($FolderList) 	or die ("cannot delete $FolderList");	}
	if(-e $SrcList) 		{unlink($SrcList) 		or die ("cannot delete $SrcList");		}
	if(-e $HdrList) 		{unlink($HdrList) 		or die ("cannot delete $HdrList");		}
	if(-e $ObjList) 		{unlink($ObjList) 		or die ("cannot delete $ObjList");		}
	if(-e $MakePathList)	{unlink($MakePathList) 	or die ("cannot delete $MakePathList");	}
	if(-e $MkPattList)		{unlink($MkPattList) 	or die ("cannot delete $MkPattList");	}
	if(-e $HexFile)			{unlink($HexFile)	 	or die ("cannot delete $HexFile");		}
	if(-e $MapFile)			{unlink($MapFile)	 	or die ("cannot delete $MapFile");		}
	if(-e $ElfFile)			{unlink($ElfFile)	 	or die ("cannot delete $ElfFile");		}


}

sub CleanDirs
{
	if(-e $ObjFolder)		{rmtree($ObjFolder);											}
	if(-e $BinFolder)		{rmtree($BinFolder);											}
}


sub GetUnixPath
{
	my $target = $_[0];
	
	$target =~ s/\\/\//g;
	
	return $target;
	
}

sub CheckEnv
{
	if(defined $ENV{'ARM_GCC'})
	{
		#The Environment is defined, continue 
	} 
	else
	{
		die ("The ARM_GCC variables are no defined ");
	}

	if(defined $ENV{'ERIKA_PATH'})
	{
		#The variable is defined, continue	
	}
	else
	{
		die ("The ERIKA_PATH variables are no defined ");
	}


}
