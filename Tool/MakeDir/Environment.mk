CSRC 	=
ASSRC	=
CFLAG	=
LDFLAG  = 
OBJECTC	=
OBJECTAS=
OBJDIR	=
OBJPAT	=
LIB		= libee.a
LDFILE	= stm32f40x_gnu.ld
CC		= $(ARM_GCC)/bin/arm-none-eabi-gcc
AS		= $(ARM_GCC)/bin/arm-none-eabi-gcc
LK		= $(ARM_GCC)/bin/arm-none-eabi-ld
NM		= $(ARM_GCC)/bin/arm-none-eabi-nm
OBJCOPY = $(ARM_GCC)/bin/arm-none-eabi-objcopy

-include FolderList.mk
-include HdrList.mk
-include SrcList.mk
-include ObjList.mk
-include MakePathList.mk 
-include MkPattList.mk
-include CompilerFlags.mk
-include LinkerFlags.mk

