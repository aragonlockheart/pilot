CFLAG+= -mcpu=cortex-m4 
CFLAG+= -mthumb 
CFLAG+= -mthumb-interwork
CFLAG+= -c
CFLAG+= -ggdb
CLFAG+= -O0
CFLAG+= -std=c11
CFLAG+= -Wall
CFLAG+= 
CFLAG+= --specs=rdimon.specs
CFLAG+= -fdata-sections -ffunction-sections
